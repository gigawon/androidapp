package com.giga.shopping;


import com.giga.library.base.BaseApplication;
import com.google.gson.Gson;


public class MyApplication extends BaseApplication {

    public interface Config {
        boolean DEBUG = BuildConfig.DEBUG;
        String APP_NAME = "GigaShopping";
        String ENCRYPT = AppConstants.ENCRYPT_TYPE.NOCRYPT;
        String FILEPROVIDER = "com.giga.shopping.fileprovider";

    }

    @Override
    public void onCreate() {
        super.onCreate();
        gson = new Gson();
        getConfig(this)
                .setAppName(Config.APP_NAME)
                .setFileProvider(Config.FILEPROVIDER)
                .init();
    }

}
