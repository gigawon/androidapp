package com.giga.shopping.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.library.util.EmptyUtils;
import com.giga.shopping.R;
import com.giga.shopping.bean.Classify;

import java.util.ArrayList;
import java.util.List;

public class ClassifyRecyclerAdapter extends RecyclerView.Adapter<ClassifyRecyclerAdapter.ViewHolder> {

    private Context context;
    List<Classify.ListBeanX.ListBean> list = new ArrayList<>();
    private ItemListener listener;
    private int selectedPosition;

    public ClassifyRecyclerAdapter(Context context, List<Classify.ListBeanX.ListBean> list) {
        this.context = context;
        this.list = list;
    }

    public void getSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public Classify.ListBeanX.ListBean getItem(int position) {
        return list.get(position);
    }

    /**
     * 获取listener,将listener传入ViewHolder中
     *
     * @param listener
     */
    public void setItemClickListener(ItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view, listener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ClassifyRecyclerAdapter.ViewHolder viewHolder, int i) {
        viewHolder.setIsRecyclable(false);
        Classify.ListBeanX.ListBean listBean = list.get(i);
        viewHolder.setData(listBean);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView img;
        public ViewHolder(@NonNull View itemView, final ItemListener listener) {
            super(itemView);
            name = itemView.findViewById(R.id.text_3);
            img = itemView.findViewById(R.id.image_3);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }

        public void setData(Classify.ListBeanX.ListBean listBean) {
            name.setText(listBean.getLevel_name()+"");
            if (EmptyUtils.isNotEmpty(listBean.getImage_url())){
                Glide.with(context).asBitmap().load(listBean.getImage_url()).into(img);
            }else {
                Glide.with(context).asBitmap().load(R.mipmap.img_bitmap).into(img);
            }
        }
    }

    /**
     * RecyclerView没有内置监听器，自定义item点击事件
     */
    public interface ItemListener {

        void onItemClick(int position);
    }
}
