
package com.giga.shopping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.giga.shopping.R;

public class confirmOrderGoodAdapter extends BaseAdapter {


    private Context context;

    private ImageView imageView;

    private TextView goodName, goodPrice, goodNum;

    private TextView refund_status;

    public confirmOrderGoodAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {

        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
//		return listdata.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        try {

            if (convertView == null) {

                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_order_item, parent,
                        false);

                imageView = (ImageView) convertView.findViewById(R.id.img_confirm_order);

                goodName = (TextView) convertView.findViewById(R.id.good_name);

                goodPrice = (TextView) convertView.findViewById(R.id.good_price);

                goodNum = (TextView) convertView.findViewById(R.id.good_num);

                refund_status = (TextView) convertView.findViewById(R.id.refund_status);

            }


            goodName.setText("苏泊尔（SUPOR）电饭煲4L 电磁炉");
            goodPrice.setText("1000");
            goodNum.setText("x" + "1");


        } catch (Exception e) {

        }
        return convertView;
    }

}
