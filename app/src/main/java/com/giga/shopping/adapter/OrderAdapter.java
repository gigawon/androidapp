package com.giga.shopping.adapter;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.shopping.R;
import com.giga.shopping.activity.AllOrderActivity;
import com.giga.shopping.activity.SearchGoodsActivity;
import com.giga.shopping.bean.ProductDetail;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private AllOrderActivity context;
    private List<ProductDetail> productDetail;
    private SearchListener listener;


    public OrderAdapter(AllOrderActivity context, List<ProductDetail> productDetail) {
        this.context = context;
        this.productDetail = productDetail;
    }

    /**
     * 获取listener,将listener传入ViewHolder中
     *
     * @param listener
     */
    public void setItemClickListener(SearchListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
//        if (context.getType() == 1) {
//            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_item_layout, viewGroup, false);
//        } else {
//            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_grid_layout, viewGroup, false);
//        }
        return null;
        //return new RecyclerViewHolder(view, i);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        RecyclerViewHolder recyclerViewHolder = (RecyclerViewHolder) viewHolder;
        recyclerViewHolder.tvItem.setText(productDetail.get(i).getItem_name());
        recyclerViewHolder.priceLeft.setText(productDetail.get(i).getMarket_p());
        recyclerViewHolder.priceRight.setText(productDetail.get(i).getItem_p());
        Glide.with(context).load(productDetail.get(i).getItem_img_url()).into(recyclerViewHolder.imgItem);
    }

    @Override
    public int getItemCount() {
        return productDetail != null ? productDetail.size() : 0;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem;
        TextView tvItem;
        TextView priceLeft;
        TextView priceRight;

        RecyclerViewHolder(View itemView, int viewType) {
            super(itemView);
            tvItem = (TextView) itemView.findViewById(R.id.text_goods);
            imgItem = itemView.findViewById(R.id.img_item);
            priceLeft = itemView.findViewById(R.id.price1);
            priceRight = itemView.findViewById(R.id.price2);
            priceRight.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    /**
     * RecyclerView没有内置监听器，自定义item点击事件
     */
    public interface SearchListener {

        void onItemClick(int position);
    }


}
