package com.giga.shopping.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.giga.library.util.LogUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.R;
import com.giga.shopping.activity.SearchGoodsActivity;
import com.giga.shopping.bean.Classify;
import com.giga.shopping.widget.ClassGridView;

import java.util.ArrayList;
import java.util.List;

/**
 * description: ${TODO}
 * autour: Knight
 * new date: 2018/9/13 on 15:12
 * e-mail: 37442216knight@gmail.com
 * update: 2018/9/13 on 15:12
 * version: v 1.0
 */
public class RightAdapter extends RecyclerView.Adapter<RightAdapter.ViewHolder> {
    private Context context;

    private List<Classify.ListBeanX.ListBean> gridList = new ArrayList<>();
    private List<Classify.ListBeanX.ListBean> gridListAll = new ArrayList<>();
    private RightListener listener;
    private int selectedPosition;
    private ClassifyRecyclerAdapter classifyGridAdapter;
    private List<Classify.ListBeanX> listBeanXList = new ArrayList<>();
    private RecyclerView.LayoutManager mLayoutManager;


    public RightAdapter(Context context, List<Classify.ListBeanX> classifyList) {
        this.context = context;
        this.listBeanXList = classifyList;
    }

    /**
     * 获取被选中的位置，将选中项移动到中间，并刷新
     *
     * @param selectedPosition
     */
    public void getSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    /**
     * 获取listener,将listener传入ViewHolder中
     *
     * @param listener
     */
    public void setItemClickListener(RightListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_classitem_right, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, listener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        Classify.ListBeanX listBeanX = listBeanXList.get(position);
        holder.itemView.setTag(listBeanX);//传object回去

//        gridList = listBeanXList.get(position).getList();
//        if (gridListAll.size() > 0) {
//            gridListAll.clear();
//        }
//        gridListAll.addAll(gridList);
        mLayoutManager = new GridLayoutManager(context, 3) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        classifyGridAdapter = new ClassifyRecyclerAdapter(context, listBeanXList.get(position).getList());
        classifyGridAdapter.setItemClickListener(new ClassifyRecyclerAdapter.ItemListener() {
            @Override
            public void onItemClick(int position) {

                Classify.ListBeanX.ListBean item = classifyGridAdapter.getItem(position);

                Intent intent = new Intent();
                intent.setClass(context,SearchGoodsActivity.class);
                intent.putExtra("itemCode", item.getLevel1());
                context.startActivity(intent);
            }
        });
        holder.recyclerView.setLayoutManager(mLayoutManager);

        holder.recyclerView.setAdapter(classifyGridAdapter);
        //下面两句是防止刷新商品的recyclerView导致商家recyclerView也发生滑动
        holder.recyclerView.setFocusableInTouchMode(false);
        holder.recyclerView.requestFocus();//        gridAdapter = new ClassifyGridAdapter(context, gridListAll);
//        holder.gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
//        holder.gridView.setAdapter(gridAdapter);
        holder.tvName.setText((listBeanX.getLevel_name() + "").replaceAll("\"", ""));
        // LogUtils.d((listBeanXList.get(position).getLevel_name()+"").replaceAll("\"",""));
    }


    @Override
    public int getItemCount() {
        return listBeanXList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        //        private ClassGridView gridView;
        private RecyclerView recyclerView;

        public ViewHolder(View itemView, final RightListener listener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.leave2_title);
            recyclerView = itemView.findViewById(R.id.grid_layout);

//            gridView = itemView.findViewById(R.id.grid_view);
//            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                }
//            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    /**
     * RecyclerView没有内置监听器，自定义item点击事件
     */
    public interface RightListener {

        void onItemClick(int position);
    }

}
