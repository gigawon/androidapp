package com.giga.shopping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.LogUtils;
import com.giga.shopping.R;
import com.giga.shopping.bean.Classify;
import java.util.ArrayList;
import java.util.List;

public class ClassifyGridAdapter extends BaseAdapter {
    private Context context = null;
    private LayoutInflater layoutInflater = null;
    List<Classify.ListBeanX.ListBean> list = new ArrayList<>();
    private static final int HAND = 1;


    public ClassifyGridAdapter(Context context, List<Classify.ListBeanX.ListBean> list) {
        super();
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public final class ViewHolder {
        public TextView leavel3;
        public ImageView img;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            holder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.grid_item, null, false);
            holder.img = view.findViewById(R.id.image_3);
            holder.leavel3 = view.findViewById(R.id.text_3);
            view.setTag(holder);
        }
        if (EmptyUtils.isNotEmpty(list.get(position).getImage_url())) {
            Glide.with(context).load(list.get(position).getImage_url()).into(holder.img);
        } else {
            holder.img.setImageResource(R.mipmap.img_bitmap);
        }

        holder.leavel3.setText((list.get(position).getLevel_name() + "").replaceAll("\"", ""));
         LogUtils.d((list.get(position).getLevel_name()+"").replaceAll("\"",""));
        return view;
    }

}
