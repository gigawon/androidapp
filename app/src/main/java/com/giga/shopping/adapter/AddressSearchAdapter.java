package com.giga.shopping.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.giga.shopping.R;
import com.giga.shopping.bean.AddressSearchDetail;
import java.util.List;

public class AddressSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<AddressSearchDetail> addressDetail;
    private SearchListener listener;

    public AddressSearchAdapter(Context context, List<AddressSearchDetail> addressDetail) {
        this.context = context;
        this.addressDetail = addressDetail;
    }

    /**
     *
     * @param listener
     */
    public void setItemClickListener(SearchListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address_search, viewGroup, false);
        return new RecyclerViewHolder(view, i);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        RecyclerViewHolder recyclerViewHolder = (RecyclerViewHolder) viewHolder;
        recyclerViewHolder.newZipName.setText("도로명 : " + addressDetail.get(i).getNewZipName());
        recyclerViewHolder.lastZipName.setText("지번 : " + addressDetail.get(i).getLastZipName());
        recyclerViewHolder.zipCode.setText(addressDetail.get(i).getZipCode());
    }

    @Override
    public int getItemCount() {
        return addressDetail != null ? addressDetail.size() : 0;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView newZipName;
        TextView lastZipName;
        TextView zipCode;

        RecyclerViewHolder(View itemView, int viewType) {
            super(itemView);

            newZipName  = (TextView) itemView.findViewById(R.id.new_address);
            lastZipName = (TextView) itemView.findViewById(R.id.last_address);
            zipCode     = (TextView) itemView.findViewById(R.id.zip_code);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface SearchListener {
        void onItemClick(int position);
    }


}
