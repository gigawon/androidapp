
package com.giga.shopping.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.giga.shopping.R;
import com.giga.shopping.fragment.OrderFragment1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Request;


public class MyOrderAdapter extends BaseAdapter {

    private Activity context;

    private OrderFragment1 fragment;

    private Object syncPaymentStatus;

    private String serverTime;

    private class ViewHolder {

        private TextView text_orderTime;

        private TextView text_orderStatus;

        private ListView listView;

        private TextView text_totoal, cancelBtn, paymentBtn, delete_btn, payment_btnAgain;

    }

    public MyOrderAdapter(Activity context, OrderFragment1 fragment) {

        this.context = context;

        this.fragment = fragment;

    }

    @Override
    public int getCount() {

        return 5;
    }

    @Override
    public Object getItem(int position) {

//		try {
//
//			return jsonArray.get(position);
//
//		} catch (Exception e) {
//
//			L.e(e);
//
//		}

        return null;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        try {

            ViewHolder itemView = null;

            if (convertView == null) {

                itemView = new ViewHolder();

                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myorder_common,
                        parent, false);

                itemView.listView = (ListView) convertView.findViewById(R.id.listView);

                itemView.text_orderStatus = (TextView) convertView.findViewById(R.id.text_orderStatus);

                itemView.text_orderTime = (TextView) convertView.findViewById(R.id.text_orderTime);

                itemView.text_totoal = (TextView) convertView.findViewById(R.id.text_totoal);

                itemView.cancelBtn = (TextView) convertView.findViewById(R.id.cancel_btn);

                itemView.paymentBtn = (TextView) convertView.findViewById(R.id.payment_btn);

                itemView.payment_btnAgain = (TextView) convertView.findViewById(R.id.payment_btnAgain);

                itemView.delete_btn = (TextView) convertView.findViewById(R.id.delete_btn);

                convertView.setTag(itemView);

            } else {

                itemView = (ViewHolder) convertView.getTag();

            }
            itemView.cancelBtn.setVisibility(View.GONE);
            itemView.paymentBtn.setVisibility(View.GONE);
//
//			itemView.payment_btnAgain.setVisibility(View.GONE);
//
//			itemView.delete_btn.setVisibility(View.GONE);


            convertView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
//					try {
//
//						Bundle bundle = new Bundle();
//
//						bundle.putString(C.KEY_JSON_FM_ORDERCODE, jsonObject.getString("order_code"));
//
//						if (flag.equals("1")) {
//							PageUtil.jumpTo(context, OrderDetailPayActivity.class, bundle);
//
//							((Activity) context).finish();
//
//						} else {
//							bundle.putString("flag_ass", flag_ass);
//
//							bundle.putString("flag", flag);
//
//							PageUtil.jumpTo(context, OrderDetailActivity.class, bundle);
//
//							((Activity) context).finish();
//
//						}
//
//					} catch (Exception e) {
//
//
//					}

                }
            });


            itemView.listView.setAdapter(new confirmOrderGoodAdapter(context));

            itemView.listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {


                }
            });
            itemView.payment_btnAgain.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                }
            });


//			((BaseActivity) context).setListViewHeight(itemView.listView);

        } catch (Exception e) {

        }

        return convertView;
    }


}
