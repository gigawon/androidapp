package com.giga.shopping.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.giga.library.util.L;
import com.giga.shopping.R;
import com.giga.shopping.bean.ShopCart;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.ViewHolder> {
    private DecimalFormat decimalFormat = new DecimalFormat("#,###");
    private Context mContext;
    private ArrayList<JSONObject> list;
    private ListControllListener listener;

    public MyCartAdapter(Context mContext, ArrayList<JSONObject> list) {
        this.mContext = mContext;
        this.list = list;
    }

    public ArrayList<JSONObject> getListItem() {
        return this.list;
    }
    /**
     * 获取listener,将listener传入ViewHolder中
     *
     * @param listener
     */
    public void setItemClickListener(ListControllListener listener) {
        this.listener = listener;
    }
    @Override
    public MyCartAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_shopcartview, viewGroup, false);
        MyCartAdapter.ViewHolder viewHolder = new MyCartAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyCartAdapter.ViewHolder viewHolder, int position) {

        try {

            JSONObject item = list.get(position);

            viewHolder.title.setText(item.getString("item_name"));
            viewHolder.price.setText(decimalFormat.format(Double.parseDouble(item.getString("item_p"))) + mContext.getString(R.string.price_unit) + " + 배송비 " + decimalFormat.format(Double.parseDouble(item.getString("delivery_p"))) + mContext.getString(R.string.price_unit));
            viewHolder.count.setText(item.getString("sale_q"));
            if (item.has("checked") && "1".equals(item.getString("checked")))
                viewHolder.checkBoxImg.setImageResource(R.mipmap.icon_cart_selected);
            else
                viewHolder.checkBoxImg.setImageResource(R.mipmap.icon_cart_unselected);

            Glide.with(mContext).load(item.getString("image_url")).into(viewHolder.image);

        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout checkBox, minus, plus;
        private ImageView image, delete, checkBoxImg;
        private TextView title, price, count;

        public ViewHolder(View itemView) {
            super(itemView);

            checkBox = itemView.findViewById(R.id.check_box);
            checkBoxImg = itemView.findViewById(R.id.check_box_img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCheckBoxClick(getAdapterPosition());
                }
            });
            image = itemView.findViewById(R.id.image);
            minus = itemView.findViewById(R.id.minus);
            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onMinusClick(getAdapterPosition());
                }
            });
            plus = itemView.findViewById(R.id.plus);
            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPlusClick(getAdapterPosition());
                }
            });
            delete = itemView.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDeleteClick(getAdapterPosition());
                }
            });
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            count = itemView.findViewById(R.id.count);
        }
    }

    public interface ListControllListener {
        void onCheckBoxClick(int position);
        void onPlusClick(int position);
        void onMinusClick(int position);
        void onDeleteClick(int position);
    }

}
