package com.giga.shopping.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.giga.shopping.R;
import com.giga.shopping.bean.ExpandBean;
import com.giga.shopping.widget.NineGridView;

import java.util.ArrayList;
import java.util.List;

import ch.ielse.view.imagewatcher.ImageWatcher;
import de.hdodenhof.circleimageview.CircleImageView;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.BaseFriendCircleViewHolder>
{

    private Context mContext;

    private LayoutInflater mLayoutInflater;

    private List<ExpandBean> mFriendCircleBeans;

    private RequestOptions mRequestOptions;

    private DrawableTransitionOptions mDrawableTransitionOptions;

    private ImageWatcher mImageWatcher;

    public CommentAdapter(Context context, ImageWatcher imageWatcher) {
        this.mContext = context;
        this.mImageWatcher = imageWatcher;
        //        this.mAvatarSize = Utils.dp2px(44f);
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mRequestOptions = new RequestOptions().centerCrop();
        this.mDrawableTransitionOptions = DrawableTransitionOptions.withCrossFade();
//        if (context instanceof OnPraiseOrCommentClickListener) {
//            this.mOnPraiseOrCommentClickListener = (OnPraiseOrCommentClickListener) context;
//        }
    }

    public void setFriendCircleBeans(List<ExpandBean> friendCircleBeans) {
        this.mFriendCircleBeans = friendCircleBeans;
        notifyDataSetChanged();
    }

    public void addFriendCircleBeans(List<ExpandBean> friendCircleBeans) {
        if (friendCircleBeans != null) {
            if (mFriendCircleBeans == null) {
                mFriendCircleBeans = new ArrayList<>();
            }
            this.mFriendCircleBeans.addAll(friendCircleBeans);
            notifyItemRangeInserted(mFriendCircleBeans.size(), friendCircleBeans.size());
        }
    }

    @Override
    public BaseFriendCircleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WordAndImagesViewHolder(mLayoutInflater.inflate(R.layout.item_recycler_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(BaseFriendCircleViewHolder holder, int position) {
        if (holder != null && mFriendCircleBeans != null && position < mFriendCircleBeans.size()) {
            ExpandBean friendCircleBean = mFriendCircleBeans.get(position);
            makeUserBaseData(holder, friendCircleBean, position);

            WordAndImagesViewHolder wordAndImagesViewHolder = (WordAndImagesViewHolder) holder;

            if (mImageWatcher != null) {
                wordAndImagesViewHolder.nineGridView.setOnImageClickListener((position1, view) ->
                        mImageWatcher.show((ImageView) view, wordAndImagesViewHolder.nineGridView.getImageViews(),
                                friendCircleBean.getImageUrls()));
            }

            wordAndImagesViewHolder.nineGridView.setAdapter(new NineImageAdapter(mContext, mRequestOptions,
                    mDrawableTransitionOptions, friendCircleBean.getImageUrls()));
            Glide.with(mContext).load(friendCircleBean.getCustom_img_path()).into(holder.imgAvatar);
            holder.rating_bar.setRating(Float.parseFloat(friendCircleBean.getScore()));
            holder.txtUserName.setText(friendCircleBean.getNick_name());
            holder.txtContent.setText(friendCircleBean.getRep_content());
            String sale_content = friendCircleBean.getSale_content();
            if (sale_content == null || sale_content.trim().equals(""))
                holder.layout_shop_comment.setVisibility(View.GONE);
            else {
                holder.layout_shop_comment.setVisibility(View.VISIBLE);
                holder.text_shop_comment.setText("사장님 댓글 : " + friendCircleBean.getSale_content());
            }
            holder.txtTime.setText(friendCircleBean.getReg_date());

        }
    }


    private void makeUserBaseData(BaseFriendCircleViewHolder holder, ExpandBean friendCircleBean, int position) {
//        holder.txtContent.setText(friendCircleBean.getContentSpan());
        //setContentShowState(holder, friendCircleBean);

    }

//    private void setContentShowState(BaseFriendCircleViewHolder holder, ExpandBean friendCircleBean) {
////        if (friendCircleBean.isShowCheckAll()) {
//        holder.txtState.setVisibility(View.VISIBLE);
//        setTextState(holder, friendCircleBean.isExpanded());
//        holder.txtState.setOnClickListener(v -> {
//            if (friendCircleBean.isExpanded()) {
//                friendCircleBean.setExpanded(false);
//            } else {
//                friendCircleBean.setExpanded(true);
//            }
////            setTextState(holder, friendCircleBean.isExpanded());
//        });
////        } else {
////            holder.txtState.setVisibility(View.GONE);
////            holder.txtContent.setMaxLines(Integer.MAX_VALUE);
////        }
//    }

//    private void setTextState(BaseFriendCircleViewHolder holder, boolean isExpand) {
//        if (isExpand) {
//            holder.txtContent.setMaxLines(Integer.MAX_VALUE);
//            holder.txtState.setText("收起");
//        } else {
//            holder.txtContent.setMaxLines(4);
//            holder.txtState.setText("全文");
//        }
//    }

//    @Override
//    public int getItemViewType(int position) {
//        return mFriendCircleBeans.get(position).getViewType();
//    }

    @Override
    public int getItemCount() {
        return mFriendCircleBeans == null ? 0 : mFriendCircleBeans.size();
    }



//    private void notifyTargetItemView(int position, SpannableStringBuilder translationResult) {
//        View childView = mLayoutManager.findViewByPosition(position);
//        if (childView != null) {
//            RecyclerView.ViewHolder viewHolder = mRecyclerView.getChildViewHolder(childView);
//            if (viewHolder instanceof BaseFriendCircleViewHolder) {
//                BaseFriendCircleViewHolder baseFriendCircleViewHolder = (BaseFriendCircleViewHolder) viewHolder;
//            }
//        }
//    }


    static class WordAndImagesViewHolder extends BaseFriendCircleViewHolder {

        NineGridView nineGridView;

        public WordAndImagesViewHolder(View itemView) {
            super(itemView);
            nineGridView = itemView.findViewById(R.id.nine_grid_view);
        }
    }


    static class BaseFriendCircleViewHolder extends RecyclerView.ViewHolder {

         TextView txtUserName;
         CircleImageView imgAvatar;
         RatingBar rating_bar;
         TextView txtContent;
         TextView txtTime;
         TextView chat_txt;
         TextView chat_time;

         LinearLayout layout_shop_comment;
         TextView text_shop_comment;
         TextView text_shop_comment_time;

        public BaseFriendCircleViewHolder(View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txt_user_name);
            imgAvatar = itemView.findViewById(R.id.img_avatar);
            rating_bar = itemView.findViewById(R.id.rating_bar);
            txtContent = itemView.findViewById(R.id.txt_content);
            txtTime = itemView.findViewById(R.id.time_txt);
            chat_txt = itemView.findViewById(R.id.text_shop_comment);
            chat_time = itemView.findViewById(R.id.text_shop_comment_time);

            layout_shop_comment = itemView.findViewById(R.id.layout_shop_comment);
            text_shop_comment = itemView.findViewById(R.id.text_shop_comment);
            text_shop_comment_time = itemView.findViewById(R.id.text_shop_comment_time);
        }
    }
}

