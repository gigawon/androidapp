package com.giga.shopping.bean;

public class DataOrder {


    /**
     * {"myData":"{sDate:'',eDate:'',page:'1',pageSize:'1',lang_type:'kor',custom_code:'0102109209162c04',token:'0102109209162c049722b1d9-dd70-406c-81be-e18f3782c4f6'}"}
     *
     * {"d":"{\"total_Count\":5,\"total_Page\":1,\"order_c\":35,\"order_pay\":0,\"order_Distribution\":0,\"order_Express\":0,\"order_cancel\":6,\"status\":1,\"msg\":\"Success\",
     * \"data\":[
     * {\"ORDER_DATE\":\"20190119\",\"ORDER_NUM\":\"1021901190000000001W\",\"ITEM_NAME\":\"민트레이스 부직포브라/삼각팬티or햄팬티 (B1303)\",\"ORDER_O\":\"71,640\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"71,640\",\"ORDER_STATUS\":\"상품주문\"},
     * {\"ORDER_DATE\":\"20190107\",\"ORDER_NUM\":\"1021901070000000024W\",\"ITEM_NAME\":\"분짜\",\"ORDER_O\":\"12,500\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"12,500\",\"ORDER_STATUS\":\"상품주문\"},
     * {\"ORDER_DATE\":\"20190107\",\"ORDER_NUM\":\"1021901070000000023W\",\"ITEM_NAME\":\"잠발라야 숯불 볶음밥\",\"ORDER_O\":\"4,900\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"4,900\",\"ORDER_STATUS\":\"상품주문\"},
     * {\"ORDER_DATE\":\"20190107\",\"ORDER_NUM\":\"1021901070000000022W\",\"ITEM_NAME\":\"분짜\",\"ORDER_O\":\"12,500\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"12,500\",\"ORDER_STATUS\":\"상품주문\"},
     * {\"ORDER_DATE\":\"20190107\",\"ORDER_NUM\":\"1021901070000000021W\",\"ITEM_NAME\":\"분짜\",\"ORDER_O\":\"12,500\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"12,500\",\"ORDER_STATUS\":\"상품주문\"}]}"}
     */

    private String d;

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }
}
