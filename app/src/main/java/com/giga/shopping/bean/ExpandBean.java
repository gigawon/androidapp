package com.giga.shopping.bean;

import java.util.List;

public class ExpandBean {

    private boolean isExpanded;

    private List<String> imageUrls;

    private String nick_name;

    private String custom_name;

    private String custom_img_path;

    private String score;

    private String rep_content;

    private String reg_date;

    private String sale_content;

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getNick_name() {
        return nick_name;
    }

    public String getSale_content() {
        return sale_content;
    }

    public void setSale_content(String sale_content) {
        this.sale_content = sale_content;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getCustom_name() {
        return custom_name;
    }

    public void setCustom_name(String custom_name) {
        this.custom_name = custom_name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getRep_content() {
        return rep_content;
    }

    public void setRep_content(String rep_content) {
        this.rep_content = rep_content;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getCustom_img_path() {
        return custom_img_path;
    }

    public void setCustom_img_path(String custom_img_path) {
        this.custom_img_path = custom_img_path;
    }
}
