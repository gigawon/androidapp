package com.giga.shopping.bean;

import java.io.Serializable;
import java.util.List;

public class Classify implements Serializable {

    private String level1;
    private String level2;
    private String level3;
    private String level_name;
    private String image_url;
    private String rank;
    private List<ListBeanX> list;

    public String getLevel1() {
        return level1;
    }

    public void setLevel1(String level1) {
        this.level1 = level1;
    }

    public String getLevel2() {
        return level2;
    }

    public void setLevel2(String level2) {
        this.level2 = level2;
    }

    public String getLevel3() {
        return level3;
    }

    public void setLevel3(String level3) {
        this.level3 = level3;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public List<ListBeanX> getList() {
        return list;
    }

    public void setList(List<ListBeanX> list) {
        this.list = list;
    }

    public static class ListBeanX {
        /**
         * level1 : A201
         * level2 : B201
         * level3 : *
         * level_name : 여성의류
         * image_url : http://fileserver.gigawon.cn:8588/GigaShopMall/icon01.png
         * rank : 1
         * list : [{"level1":"A201","level2":"B201","level3":"C201","level_name":"원피스/정장","image_url":"","rank":"1"},{"level1":"A201","level2":"B201","level3":"C202","level_name":"상의","image_url":"","rank":"2"},{"level1":"A201","level2":"B201","level3":"C203","level_name":"하의","image_url":"","rank":"3"},{"level1":"A201","level2":"B201","level3":"C204","level_name":"아우터/니트","image_url":"","rank":"4"},{"level1":"A201","level2":"B201","level3":"C205","level_name":"테마의류","image_url":"","rank":"5"}]
         */

        private String level1;
        private String level2;
        private String level3;
        private String level_name;
        private String image_url;
        private String rank;
        private List<ListBean> list;

        public String getLevel1() {
            return level1;
        }

        public void setLevel1(String level1) {
            this.level1 = level1;
        }

        public String getLevel2() {
            return level2;
        }

        public void setLevel2(String level2) {
            this.level2 = level2;
        }

        public String getLevel3() {
            return level3;
        }

        public void setLevel3(String level3) {
            this.level3 = level3;
        }

        public String getLevel_name() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * level1 : A201
             * level2 : B201
             * level3 : C201
             * level_name : 원피스/정장
             * image_url :
             * rank : 1
             */

            private String level1;
            private String level2;
            private String level3;
            private String level_name;
            private String image_url;
            private String rank;

            public String getLevel1() {
                return level1;
            }

            public void setLevel1(String level1) {
                this.level1 = level1;
            }

            public String getLevel2() {
                return level2;
            }

            public void setLevel2(String level2) {
                this.level2 = level2;
            }

            public String getLevel3() {
                return level3;
            }

            public void setLevel3(String level3) {
                this.level3 = level3;
            }

            public String getLevel_name() {
                return level_name;
            }

            public void setLevel_name(String level_name) {
                this.level_name = level_name;
            }

            public String getImage_url() {
                return image_url;
            }

            public void setImage_url(String image_url) {
                this.image_url = image_url;
            }

            public String getRank() {
                return rank;
            }

            public void setRank(String rank) {
                this.rank = rank;
            }
        }
    }
}
