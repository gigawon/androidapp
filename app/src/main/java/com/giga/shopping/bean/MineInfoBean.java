package com.giga.shopping.bean;

public class MineInfoBean {

    /**
     * custom_id : null
     * custom_name : null
     * nick_name : null
     * point_card_no : null
     * gaypoint : null
     * img_path : null
     * sex_code : null
     * birt_date : null
     * status : 2
     * flag : 1805
     * msg : 입력 파라메터 오류
     */

    private String custom_id;
    private String custom_name;
    private String nick_name;
    private String point_card_no;
    private String gaypoint;
    private String img_path;
    private String sex_code;
    private String birt_date;
    private String status;
    private String flag;
    private String msg;

    public String getCustom_id() {
        return custom_id;
    }

    public void setCustom_id(String custom_id) {
        this.custom_id = custom_id;
    }

    public String getCustom_name() {
        return custom_name;
    }

    public void setCustom_name(String custom_name) {
        this.custom_name = custom_name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getPoint_card_no() {
        return point_card_no;
    }

    public void setPoint_card_no(String point_card_no) {
        this.point_card_no = point_card_no;
    }

    public String getGaypoint() {
        return gaypoint;
    }

    public void setGaypoint(String gaypoint) {
        this.gaypoint = gaypoint;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public String getSex_code() {
        return sex_code;
    }

    public void setSex_code(String sex_code) {
        this.sex_code = sex_code;
    }

    public String getBirt_date() {
        return birt_date;
    }

    public void setBirt_date(String birt_date) {
        this.birt_date = birt_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
