package com.giga.shopping.bean;

import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class CommentBean {

    private List<LinkedTreeMap<Object,Object>> Assesses;

    private String ass_cnt;

    private String ass_img_cnt;

    private String ass_s_cnt;

    private String ass_z_cnt;

    private String ass_x_cnt;

    private String status;

    private String flag;

    private String msg;

    private String start_page;

    private String end_page;

    private String pg;

    private String tot_page;

    private String tot_cnt;

    public List<LinkedTreeMap<Object,Object>> getAssesses() {
        return Assesses;
    }

    public void setAssesses(List<LinkedTreeMap<Object,Object>> assesses) {
        Assesses = assesses;
    }

    public String getAss_cnt() {
        return ass_cnt;
    }

    public void setAss_cnt(String ass_cnt) {
        this.ass_cnt = ass_cnt;
    }

    public String getAss_img_cnt() {
        return ass_img_cnt;
    }

    public void setAss_img_cnt(String ass_img_cnt) {
        this.ass_img_cnt = ass_img_cnt;
    }

    public String getAss_s_cnt() {
        return ass_s_cnt;
    }

    public void setAss_s_cnt(String ass_s_cnt) {
        this.ass_s_cnt = ass_s_cnt;
    }

    public String getAss_z_cnt() {
        return ass_z_cnt;
    }

    public void setAss_z_cnt(String ass_z_cnt) {
        this.ass_z_cnt = ass_z_cnt;
    }

    public String getAss_x_cnt() {
        return ass_x_cnt;
    }

    public void setAss_x_cnt(String ass_x_cnt) {
        this.ass_x_cnt = ass_x_cnt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStart_page() {
        return start_page;
    }

    public void setStart_page(String start_page) {
        this.start_page = start_page;
    }

    public String getEnd_page() {
        return end_page;
    }

    public void setEnd_page(String end_page) {
        this.end_page = end_page;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public String getTot_page() {
        return tot_page;
    }

    public void setTot_page(String tot_page) {
        this.tot_page = tot_page;
    }

    public String getTot_cnt() {
        return tot_cnt;
    }

    public void setTot_cnt(String tot_cnt) {
        this.tot_cnt = tot_cnt;
    }
}
