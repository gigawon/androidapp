package com.giga.shopping.bean;

public class AddressSearchDetail {

    private String zipCode;

    private String newZipName;

    private String lastZipName;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNewZipName() {
        return newZipName;
    }

    public void setNewZipName(String newZipName) {
        this.newZipName = newZipName;
    }

    public String getLastZipName() {
        return lastZipName;
    }

    public void setLastZipName(String lastZipName) {
        this.lastZipName = lastZipName;
    }

}
