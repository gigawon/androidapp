package com.giga.shopping.bean;

import java.util.List;

public class ClassifyList {
    private List<Classify> classifyList;

    public List<Classify> getClassifyList() {
        return classifyList;
    }

    public void setClassifyList(List<Classify> classifyList) {
        this.classifyList = classifyList;
    }
}
