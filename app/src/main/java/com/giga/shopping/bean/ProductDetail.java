package com.giga.shopping.bean;

public class ProductDetail {

    /*
    ad_image: "http://fileserver.gigawon.co.kr:8588/GigaShopMall/MainPage/main07.png"
    ad_title: "패션의류/잡화.뷰티"
    custom_code: "010530117822fbe4"
    item_code: "180311001600"
    item_img_url: "http://fileserver.gigawon.co.kr:8588/Item/main_0021.png"
    item_name: "과일나라 후르츠왁스 헤어칼라 진주펄 [05호 밝은 ..."
    item_p: "5400.00"
    link_url: "http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4"
    market_p: "6000.00"
    sub_title: ""
     */

    private String ad_image;
    private String ad_title;
    private String custom_code;
    private String item_code;
    private String item_img_url;
    private String item_name;
    private String item_p;
    private String link_url;
    private String market_p;
    private String sub_title;

    public String getAd_image() {
        return ad_image;
    }

    public void setAd_image(String ad_image) {
        this.ad_image = ad_image;
    }

    public String getAd_title() {
        return ad_title;
    }

    public void setAd_title(String ad_title) {
        this.ad_title = ad_title;
    }

    public String getCustom_code() {
        return custom_code;
    }

    public void setCustom_code(String custom_code) {
        this.custom_code = custom_code;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public String getItem_img_url() {
        return item_img_url;
    }

    public void setItem_img_url(String item_img_url) {
        this.item_img_url = item_img_url;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_p() {
        return item_p;
    }

    public void setItem_p(String item_p) {
        this.item_p = item_p;
    }

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public String getMarket_p() {
        return market_p;
    }

    public void setMarket_p(String market_p) {
        this.market_p = market_p;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public ProductDetail() {
    }

    public ProductDetail(String ad_image, String ad_title, String custom_code, String item_code, String item_img_url, String item_name, String item_p, String link_url, String market_p, String sub_title) {
        this.ad_image = ad_image;
        this.ad_title = ad_title;
        this.custom_code = custom_code;
        this.item_code = item_code;
        this.item_img_url = item_img_url;
        this.item_name = item_name;
        this.item_p = item_p;
        this.link_url = link_url;
        this.market_p = market_p;
        this.sub_title = sub_title;
    }

}
