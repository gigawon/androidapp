package com.giga.shopping.bean;

import java.io.Serializable;

public class LoginEntity implements Serializable {


    /**
     * status : 1
     * flag : 1501
     * msg : 작업완료
     * custom_code : 186743935020f829
     * custom_id : 18674393502
     * custom_name : jointest11c00f9
     * nick_name : 깜씨짱
     * token : 186743935020f829ca296aa4-dfe8-477f-8a25-21480a8fa591
     * profile_img : https://api.shelongwang.com:8444//upload/image/2018120/20181202153121250x250.jpg
     * div_code :
     * ssoId : 186743935020f829yc
     * sso_regiKey : 28B03763-9BEF-4281-B05E-BF4950A82E55
     * mall_home_id :
     * point_card_no : 8111100200011997
     * parent_id : 12345678900
     */

    private String status;
    private String flag;
    private String msg;
    private String custom_code;
    private String custom_id;
    private String custom_name;
    private String nick_name;
    private String token;
    private String profile_img;
    private String div_code;
    private String ssoId;
    private String sso_regiKey;
    private String mall_home_id;
    private String point_card_no;
    private String parent_id;
    private String minfo;
    private String business_type;


    public String getMinfo() {
        return minfo;
    }

    public void setMinfo(String minfo) {
        this.minfo = minfo;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCustom_code() {
        return custom_code;
    }

    public void setCustom_code(String custom_code) {
        this.custom_code = custom_code;
    }

    public String getCustom_id() {
        return custom_id;
    }

    public void setCustom_id(String custom_id) {
        this.custom_id = custom_id;
    }

    public String getCustom_name() {
        return custom_name;
    }

    public void setCustom_name(String custom_name) {
        this.custom_name = custom_name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getDiv_code() {
        return div_code;
    }

    public void setDiv_code(String div_code) {
        this.div_code = div_code;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public String getSso_regiKey() {
        return sso_regiKey;
    }

    public void setSso_regiKey(String sso_regiKey) {
        this.sso_regiKey = sso_regiKey;
    }

    public String getMall_home_id() {
        return mall_home_id;
    }

    public void setMall_home_id(String mall_home_id) {
        this.mall_home_id = mall_home_id;
    }

    public String getPoint_card_no() {
        return point_card_no;
    }

    public void setPoint_card_no(String point_card_no) {
        this.point_card_no = point_card_no;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
