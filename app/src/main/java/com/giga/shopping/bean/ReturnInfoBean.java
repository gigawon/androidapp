package com.giga.shopping.bean;

import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

public class ReturnInfoBean {

    private List<LinkedTreeMap<Object,Object>> data;

    private String status;

    private String message;

    private String total_Count;

    private String total_Page;

    private String start_page;

    private String end_page;

    public List<LinkedTreeMap<Object, Object>> getData() {
        return data;
    }

    public void setData(List<LinkedTreeMap<Object, Object>> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal_Count() {
        return total_Count;
    }

    public void setTotal_Count(String total_Count) {
        this.total_Count = total_Count;
    }

    public String getTotal_Page() {
        return total_Page;
    }

    public void setTotal_Page(String total_Page) {
        this.total_Page = total_Page;
    }

    public String getStart_page() {
        return start_page;
    }

    public void setStart_page(String start_page) {
        this.start_page = start_page;
    }

    public String getEnd_page() {
        return end_page;
    }

    public void setEnd_page(String end_page) {
        this.end_page = end_page;
    }
}
