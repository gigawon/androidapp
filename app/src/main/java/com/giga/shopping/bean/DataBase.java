package com.giga.shopping.bean;

/**
 * Created by LaMitad on 2017/7/17.
 */

public class DataBase {

    /**
     * is_success : 1
     * error :
     * encrypt_type : nocrypt
     * sign :
     */

    private int status;
    private String msg;
    private String flag;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
