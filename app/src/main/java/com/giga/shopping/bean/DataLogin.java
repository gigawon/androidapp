package com.giga.shopping.bean;

/**
 * Created by LaMitad on 2017/7/19.
 */

public class DataLogin {

    /**
     * is_success : 1
     * error :
     * param : {"areaID":null,"cPaperPic":null,"certNo":"370634199211110615","certType":"1","cityID":null,"cityName":null,"dPaper":null,"dPaperPic":null,"lastLoginTime":"2017-04-06 19:15:22","levName":null,"levelID":"1","logo_large":null,"logo_middle":null,"logo_small":null,"memberId":"2019","mobile":"13255532509","points":"0","proName":"张宇","provinceID":null,"provinceName":null}
     * encrypt_type : nocrypt
     * sign :
     */

    private int is_success;
    private String error;
    private ParamEntity param;
    private String encrypt_type;
    private String sign;

    public int getIs_success() {
        return is_success;
    }

    public void setIs_success(int is_success) {
        this.is_success = is_success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ParamEntity getParam() {
        return param;
    }

    public void setParam(ParamEntity param) {
        this.param = param;
    }

    public String getEncrypt_type() {
        return encrypt_type;
    }

    public void setEncrypt_type(String encrypt_type) {
        this.encrypt_type = encrypt_type;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static class ParamEntity {
        /**
         * areaID : null
         * cPaperPic : null
         * certNo : 370634199211110615
         * certType : 1
         * cityID : null
         * cityName : null
         * dPaper : null
         * dPaperPic : null
         * lastLoginTime : 2017-04-06 19:15:22
         * levName : null
         * levelID : 1
         * logo_large : null
         * logo_middle : null
         * logo_small : null
         * memberId : 2019
         * mobile : 13255532509
         * points : 0
         * proName : 张宇
         * provinceID : null
         * provinceName : null
         */

        private Object areaID;
        private Object cPaperPic;
        private String certNo;
        private String certType;
        private Object cityID;
        private Object cityName;
        private Object dPaper;
        private Object dPaperPic;
        private String lastLoginTime;
        private Object levName;
        private String levelID;
        private Object logo_large;
        private Object logo_middle;
        private Object logo_small;
        private String memberId;
        private String mobile;
        private String points;
        private String proName;
        private Object provinceID;
        private Object provinceName;

        public Object getAreaID() {
            return areaID;
        }

        public void setAreaID(Object areaID) {
            this.areaID = areaID;
        }

        public Object getCPaperPic() {
            return cPaperPic;
        }

        public void setCPaperPic(Object cPaperPic) {
            this.cPaperPic = cPaperPic;
        }

        public String getCertNo() {
            return certNo;
        }

        public void setCertNo(String certNo) {
            this.certNo = certNo;
        }

        public String getCertType() {
            return certType;
        }

        public void setCertType(String certType) {
            this.certType = certType;
        }

        public Object getCityID() {
            return cityID;
        }

        public void setCityID(Object cityID) {
            this.cityID = cityID;
        }

        public Object getCityName() {
            return cityName;
        }

        public void setCityName(Object cityName) {
            this.cityName = cityName;
        }

        public Object getDPaper() {
            return dPaper;
        }

        public void setDPaper(Object dPaper) {
            this.dPaper = dPaper;
        }

        public Object getDPaperPic() {
            return dPaperPic;
        }

        public void setDPaperPic(Object dPaperPic) {
            this.dPaperPic = dPaperPic;
        }

        public String getLastLoginTime() {
            return lastLoginTime;
        }

        public void setLastLoginTime(String lastLoginTime) {
            this.lastLoginTime = lastLoginTime;
        }

        public Object getLevName() {
            return levName;
        }

        public void setLevName(Object levName) {
            this.levName = levName;
        }

        public String getLevelID() {
            return levelID;
        }

        public void setLevelID(String levelID) {
            this.levelID = levelID;
        }

        public Object getLogo_large() {
            return logo_large;
        }

        public void setLogo_large(Object logo_large) {
            this.logo_large = logo_large;
        }

        public Object getLogo_middle() {
            return logo_middle;
        }

        public void setLogo_middle(Object logo_middle) {
            this.logo_middle = logo_middle;
        }

        public Object getLogo_small() {
            return logo_small;
        }

        public void setLogo_small(Object logo_small) {
            this.logo_small = logo_small;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getProName() {
            return proName;
        }

        public void setProName(String proName) {
            this.proName = proName;
        }

        public Object getProvinceID() {
            return provinceID;
        }

        public void setProvinceID(Object provinceID) {
            this.provinceID = provinceID;
        }

        public Object getProvinceName() {
            return provinceName;
        }

        public void setProvinceName(Object provinceName) {
            this.provinceName = provinceName;
        }
    }
}
