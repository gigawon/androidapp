package com.giga.shopping.bean;

import java.util.List;

public class DataMain {

    /**
     * bannerlist10 : [{"ad_title":"소상공인 BANNER 1","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/TopBanner01.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311020443"},{"ad_title":"소상공인 BANNER 2","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/TopBanner02.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311019626"},{"ad_title":"소상공인 BANNER 3","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/TopBanner03.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311020083"},{"ad_title":"소상공인 BANNER 4","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/TopBanner04.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311015440"}]
     * bannerlist15 : [{"ad_title":"소상공인 BANNER 중앙","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/MidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311019486"}]
     * bannerlist18 : []
     * eventtype20 : [{"ad_title":"핫딜상품 구매하면 배송이 공짜","sub_title":"2018년 1월까지 주문하세요","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311011668","item_name":"[BVLGARI]불가리 오떼블랑 40ml 3종세트","item_p":"40500.00","market_p":"45000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0012.png"},{"ad_title":"핫딜상품 구매하면 배송이 공짜","sub_title":"2018년 1월까지 주문하세요","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311012183","item_name":"[카이젤]3in1 여성전용 멀티제모기 KSR-158...","item_p":"35100.00","market_p":"39000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0016.png"},{"ad_title":"핫딜상품 구매하면 배송이 공짜","sub_title":"2018년 1월까지 주문하세요","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311013088","item_name":"[Dr.EM]닥터이엠 헤어토닉(80ml)","item_p":"26730.00","market_p":"29700.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0014.png"},{"ad_title":"핫딜상품 구매하면 배송이 공짜","sub_title":"2018년 1월까지 주문하세요","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311015995","item_name":"[ANJO]앙주 프로페셔널 마유크림","item_p":"500.00","market_p":"30000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0011.png"},{"ad_title":"핫딜상품 구매하면 배송이 공짜","sub_title":"2018년 1월까지 주문하세요","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311018150","item_name":"[VISKA] 독일 비스카 향균 메모리폼베개/일반형...","item_p":"162000.00","market_p":"180000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0015.png"},{"ad_title":"핫딜상품 구매하면 배송이 공짜","sub_title":"2018년 1월까지 주문하세요","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png","link_url":"","custom_code":"010530117822fbe4","item_code":"180311018181","item_name":"[CANZ]  차량용 공기청정기 CA8820","item_p":"53100.00","market_p":"59000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0013.png"}]
     * eventtype21 : [{"ad_title":"패션의류/잡화.뷰티","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311001600","item_name":"과일나라 후르츠왁스 헤어칼라 진주펄 [05호 밝은 ...","item_p":"5400.00","market_p":"6000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0021.png"},{"ad_title":"패션의류/잡화.뷰티","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311013508","item_name":"라하츠 아로마 거품 입욕제 (800g)","item_p":"26400.00","market_p":"28000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0025.png"},{"ad_title":"패션의류/잡화.뷰티","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311016836","item_name":"러스트리아 쥬얼리 패치(타투) A세트(1~5번)","item_p":"40500.00","market_p":"45000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0026.png"},{"ad_title":"패션의류/잡화.뷰티","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311020463","item_name":"닥터그랜드 마일드 포밍 클렌저 280ml","item_p":"31500.00","market_p":"35000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0022.png"},{"ad_title":"패션의류/잡화.뷰티","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311021237","item_name":"닥터이노덤 AC안티스팟 30ml","item_p":"33750.00","market_p":"37500.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0023.png"},{"ad_title":"패션의류/잡화.뷰티","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311021242","item_name":"닥터이노덤 포스트레이저 미백 앰플 마스크팩 1BOX...","item_p":"59400.00","market_p":"66000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0024.png"}]
     * eventtype22 : [{"ad_title":"홈데코/문구.취미","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311012083","item_name":"세차의 혁명! 매직호스 풀세트(30M)/실리콘재질....","item_p":"14900.00","market_p":"15900.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0032.png"},{"ad_title":"홈데코/문구.취미","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311014763","item_name":"소다스쿨 다용도 세척제(20gx10개)","item_p":"19800.00","market_p":"22000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0033.png"},{"ad_title":"홈데코/문구.취미","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311016624","item_name":"스위티 흡착식 다목적 트윈 수납함","item_p":"5310.00","market_p":"5900.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0034.png"},{"ad_title":"홈데코/문구.취미","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311017708","item_name":"아놀드파마 반달쿨베개세트","item_p":"49500.00","market_p":"55000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0035.png"},{"ad_title":"홈데코/문구.취미","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311019028","item_name":"키모 화장품정리함 (17포켓)","item_p":"31500.00","market_p":"35000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0036.png"},{"ad_title":"홈데코/문구.취미","sub_title":"","ad_image":"http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png","link_url":"http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4","custom_code":"010530117822fbe4","item_code":"180311021391","item_name":"부케가르니 드레스퍼퓸 60ml 3EA","item_p":"27000.00","market_p":"30000.00","item_img_url":"http://fileserver.gigawon.cn:8588/Item/main_0031.png"}]
     * itemlevel : [{"level1":"A201","level2":"*","level3":"*","level_name":"패션의류","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca02.png","rank":"1"},{"level1":"A201","level2":"B201","level3":"*","level_name":"여성의류","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon01.png","rank":"1"},{"level1":"A201","level2":"B201","level3":"C201","level_name":"원피스/정장","image_url":"","rank":"1"},{"level1":"A201","level2":"B201","level3":"C202","level_name":"상의","image_url":"","rank":"2"},{"level1":"A201","level2":"B201","level3":"C203","level_name":"하의","image_url":"","rank":"3"},{"level1":"A201","level2":"B201","level3":"C204","level_name":"아우터/니트","image_url":"","rank":"4"},{"level1":"A201","level2":"B201","level3":"C205","level_name":"테마의류","image_url":"","rank":"5"},{"level1":"A201","level2":"B202","level3":"*","level_name":"남성의류","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon02.png","rank":"2"},{"level1":"A201","level2":"B202","level3":"C201","level_name":"아웃터","image_url":"","rank":"1"},{"level1":"A201","level2":"B202","level3":"C202","level_name":"상의","image_url":"","rank":"2"},{"level1":"A201","level2":"B202","level3":"C203","level_name":"하의/트레이닝","image_url":"","rank":"3"},{"level1":"A201","level2":"B202","level3":"C204","level_name":"니트","image_url":"","rank":"4"},{"level1":"A201","level2":"B202","level3":"C205","level_name":"정장","image_url":"","rank":"5"},{"level1":"A201","level2":"B202","level3":"C206","level_name":"테마의류","image_url":"","rank":"6"},{"level1":"A201","level2":"B203","level3":"*","level_name":"언더웨어","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon03.png","rank":"3"},{"level1":"A201","level2":"B203","level3":"C201","level_name":"여성 언더웨어","image_url":"","rank":"1"},{"level1":"A201","level2":"B203","level3":"C202","level_name":"남성 언더웨어","image_url":"","rank":"2"},{"level1":"A201","level2":"B203","level3":"C203","level_name":"주니어 언더웨어","image_url":"","rank":"3"},{"level1":"A201","level2":"B203","level3":"C204","level_name":"잠옷/홈웨어/체형보정","image_url":"","rank":"4"},{"level1":"A201","level2":"B203","level3":"C205","level_name":"인기브랜드","image_url":"","rank":"5"},{"level1":"A201","level2":"B204","level3":"*","level_name":"유아동의류","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon04.png","rank":"4"},{"level1":"A201","level2":"B204","level3":"C201","level_name":"아동의류","image_url":"","rank":"1"},{"level1":"A201","level2":"B204","level3":"C202","level_name":"주니어의류","image_url":"","rank":"2"},{"level1":"A201","level2":"B204","level3":"C203","level_name":"신생아의류","image_url":"","rank":"3"},{"level1":"A201","level2":"B204","level3":"C204","level_name":"내의","image_url":"","rank":"4"},{"level1":"A201","level2":"B204","level3":"C205","level_name":"시즌/이벤트 의류","image_url":"","rank":"5"},{"level1":"A201","level2":"B204","level3":"C206","level_name":"브랜드관/신발/가방/잡화","image_url":"","rank":"6"},{"level1":"A301","level2":"*","level3":"*","level_name":"잡화/뷰티","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca03.png","rank":"2"},{"level1":"A301","level2":"B301","level3":"*","level_name":"신발","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon05.png","rank":"1"},{"level1":"A301","level2":"B301","level3":"C301","level_name":"운동화/캐주얼화","image_url":"","rank":"1"},{"level1":"A301","level2":"B301","level3":"C302","level_name":"여성화","image_url":"","rank":"2"},{"level1":"A301","level2":"B301","level3":"C303","level_name":"남성화","image_url":"","rank":"3"},{"level1":"A301","level2":"B301","level3":"C304","level_name":"슬리퍼/기능화/용품","image_url":"","rank":"4"},{"level1":"A301","level2":"B301","level3":"C305","level_name":"베스트샵","image_url":"","rank":"5"},{"level1":"A301","level2":"B302","level3":"*","level_name":"가방/잡화","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon06.png","rank":"2"},{"level1":"A301","level2":"B302","level3":"C301","level_name":"가방","image_url":"","rank":"1"},{"level1":"A301","level2":"B302","level3":"C302","level_name":"잡화","image_url":"","rank":"2"},{"level1":"A301","level2":"B303","level3":"*","level_name":"유아동신발/잡화","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon07.png","rank":"3"},{"level1":"A301","level2":"B303","level3":"C301","level_name":"아동신발","image_url":"","rank":"1"},{"level1":"A301","level2":"B303","level3":"C302","level_name":"유아동 잡화/모자","image_url":"","rank":"2"},{"level1":"A301","level2":"B303","level3":"C303","level_name":"아동가방","image_url":"","rank":"3"},{"level1":"A301","level2":"B303","level3":"C304","level_name":"유아동 의류/쥬얼리/시계","image_url":"","rank":"4"},{"level1":"A301","level2":"B303","level3":"C305","level_name":"브랜드 아동화/책가방","image_url":"","rank":"5"},{"level1":"A301","level2":"B304","level3":"*","level_name":"쥬얼리/시계","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon08.png","rank":"4"},{"level1":"A301","level2":"B304","level3":"C301","level_name":"쥬얼리","image_url":"","rank":"1"},{"level1":"A301","level2":"B304","level3":"C302","level_name":"시계","image_url":"","rank":"2"},{"level1":"A301","level2":"B304","level3":"C303","level_name":"아이웨어","image_url":"","rank":"3"},{"level1":"A301","level2":"B304","level3":"C304","level_name":"골드바/실버바","image_url":"","rank":"4"},{"level1":"A301","level2":"B304","level3":"C305","level_name":"헤어액세서리","image_url":"","rank":"5"},{"level1":"A301","level2":"B305","level3":"*","level_name":"수입명품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon09.png","rank":"5"},{"level1":"A301","level2":"B305","level3":"C301","level_name":"명품가방","image_url":"","rank":"1"},{"level1":"A301","level2":"B305","level3":"C302","level_name":"명품 지갑/벨트","image_url":"","rank":"2"},{"level1":"A301","level2":"B305","level3":"C303","level_name":"명품 시계/쥬얼리","image_url":"","rank":"3"},{"level1":"A301","level2":"B305","level3":"C304","level_name":"명품 슈즈/잡화","image_url":"","rank":"4"},{"level1":"A301","level2":"B305","level3":"C305","level_name":"명품 의류","image_url":"","rank":"5"},{"level1":"A301","level2":"B305","level3":"C306","level_name":"리퍼브/중고명품","image_url":"","rank":"6"},{"level1":"A301","level2":"B305","level3":"C307","level_name":"인기브랜드","image_url":"","rank":"7"},{"level1":"A301","level2":"B306","level3":"*","level_name":"백화점 명품화장품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon10.png","rank":"6"},{"level1":"A301","level2":"B306","level3":"C301","level_name":"명품화장품","image_url":"","rank":"1"},{"level1":"A301","level2":"B306","level3":"C302","level_name":"브랜드 공식샵","image_url":"","rank":"2"},{"level1":"A301","level2":"B307","level3":"*","level_name":"화장품/향수","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon11.png","rank":"7"},{"level1":"A301","level2":"B307","level3":"C301","level_name":"스킨케어","image_url":"","rank":"1"},{"level1":"A301","level2":"B307","level3":"C302","level_name":"선케어/메이크업","image_url":"","rank":"2"},{"level1":"A301","level2":"B307","level3":"C303","level_name":"클렌징/마스크팩","image_url":"","rank":"3"},{"level1":"A301","level2":"B307","level3":"C304","level_name":"바디케어/뷰티소품/향수","image_url":"","rank":"4"},{"level1":"A301","level2":"B307","level3":"C305","level_name":"남성화장품","image_url":"","rank":"5"},{"level1":"A301","level2":"B307","level3":"C306","level_name":"브랜드 화장품","image_url":"","rank":"6"},{"level1":"A301","level2":"B307","level3":"C307","level_name":"브랜드 공식샵","image_url":"","rank":"7"},{"level1":"A301","level2":"B308","level3":"*","level_name":"바디/헤어","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon12.png","rank":"8"},{"level1":"A301","level2":"B308","level3":"C301","level_name":"헤어케어","image_url":"","rank":"1"},{"level1":"A301","level2":"B308","level3":"C302","level_name":"바디로션/핸드크림","image_url":"","rank":"2"},{"level1":"A301","level2":"B308","level3":"C303","level_name":"바디케어","image_url":"","rank":"3"},{"level1":"A301","level2":"B308","level3":"C304","level_name":"헤어스타일링","image_url":"","rank":"4"},{"level1":"A301","level2":"B308","level3":"C305","level_name":"남성 면도/바디/헤어/칫솔","image_url":"","rank":"5"},{"level1":"A301","level2":"B308","level3":"C306","level_name":"브랜드 공식샵","image_url":"","rank":"6"},{"level1":"A401","level2":"*","level3":"*","level_name":"출산/유아동","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca04.png","rank":"3"},{"level1":"A401","level2":"B401","level3":"*","level_name":"기저귀/분유/유아식","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon13.png","rank":"1"},{"level1":"A401","level2":"B401","level3":"C401","level_name":"기저귀","image_url":"","rank":"1"},{"level1":"A401","level2":"B401","level3":"C402","level_name":"분유","image_url":"","rank":"2"},{"level1":"A401","level2":"B401","level3":"C403","level_name":"유아식","image_url":"","rank":"3"},{"level1":"A401","level2":"B402","level3":"*","level_name":"육아용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon14.png","rank":"2"},{"level1":"A401","level2":"B402","level3":"C401","level_name":"발육용품","image_url":"","rank":"1"},{"level1":"A401","level2":"B402","level3":"C402","level_name":"수유용품","image_url":"","rank":"2"},{"level1":"A401","level2":"B402","level3":"C403","level_name":"스킨케어","image_url":"","rank":"3"},{"level1":"A401","level2":"B402","level3":"C404","level_name":"생활용품","image_url":"","rank":"4"},{"level1":"A401","level2":"B402","level3":"C405","level_name":"출산물","image_url":"","rank":"5"},{"level1":"A401","level2":"B402","level3":"C406","level_name":"임부복","image_url":"","rank":"6"},{"level1":"A401","level2":"B402","level3":"C407","level_name":"브랜드 육아용품","image_url":"","rank":"7"},{"level1":"A401","level2":"B403","level3":"*","level_name":"장난감","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon15.png","rank":"3"},{"level1":"A401","level2":"B403","level3":"C401","level_name":"신생아완구","image_url":"","rank":"1"},{"level1":"A401","level2":"B403","level3":"C402","level_name":"교육완구","image_url":"","rank":"2"},{"level1":"A401","level2":"B403","level3":"C403","level_name":"인형","image_url":"","rank":"3"},{"level1":"A401","level2":"B403","level3":"C404","level_name":"유아동완구","image_url":"","rank":"4"},{"level1":"A401","level2":"B403","level3":"C405","level_name":"아동완구","image_url":"","rank":"5"},{"level1":"A401","level2":"B403","level3":"C406","level_name":"국내/해외완구","image_url":"","rank":"6"},{"level1":"A401","level2":"B403","level3":"C407","level_name":"캐릭터/작동완구","image_url":"","rank":"7"},{"level1":"A401","level2":"B403","level3":"C408","level_name":"장난감 정리","image_url":"","rank":"8"},{"level1":"A401","level2":"B403","level3":"C409","level_name":"학용/학습용품","image_url":"","rank":"9"},{"level1":"A401","level2":"B404","level3":"*","level_name":"유아동 의류","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon16.png","rank":"4"},{"level1":"A401","level2":"B404","level3":"C401","level_name":"여아/남아의류","image_url":"","rank":"1"},{"level1":"A401","level2":"B404","level3":"C402","level_name":"주니어의류","image_url":"","rank":"2"},{"level1":"A401","level2":"B404","level3":"C403","level_name":"신생아의류","image_url":"","rank":"3"},{"level1":"A401","level2":"B404","level3":"C404","level_name":"내의","image_url":"","rank":"4"},{"level1":"A401","level2":"B404","level3":"C405","level_name":"시즌/이벤트 의류","image_url":"","rank":"5"},{"level1":"A401","level2":"B404","level3":"C406","level_name":"유아동 신발/가방/잡화","image_url":"","rank":"6"},{"level1":"A401","level2":"B404","level3":"C407","level_name":"유아동 브랜드샵","image_url":"","rank":"7"},{"level1":"A401","level2":"B405","level3":"*","level_name":"유아동 신발/잡화","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon17.png","rank":"5"},{"level1":"A401","level2":"B405","level3":"C401","level_name":"아동신발","image_url":"","rank":"1"},{"level1":"A401","level2":"B405","level3":"C402","level_name":"유아동 잡화/모자","image_url":"","rank":"2"},{"level1":"A401","level2":"B405","level3":"C403","level_name":"아동가방","image_url":"","rank":"3"},{"level1":"A401","level2":"B405","level3":"C404","level_name":"유아동의류","image_url":"","rank":"4"},{"level1":"A401","level2":"B405","level3":"C405","level_name":"브랜드 아동화/책가방","image_url":"","rank":"5"},{"level1":"A401","level2":"B406","level3":"*","level_name":"브랜드 아동패션","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon18.png","rank":"6"},{"level1":"A401","level2":"B406","level3":"C401","level_name":"브랜드 의류","image_url":"","rank":"1"},{"level1":"A401","level2":"B406","level3":"C402","level_name":"브랜드 신발/가방/모자/잡화","image_url":"","rank":"2"},{"level1":"A401","level2":"B406","level3":"C403","level_name":"해외 브랜드샵","image_url":"","rank":"3"},{"level1":"A501","level2":"*","level3":"*","level_name":"식품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca05.png","rank":"4"},{"level1":"A501","level2":"B501","level3":"*","level_name":"신선식품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon19.png","rank":"1"},{"level1":"A501","level2":"B501","level3":"C501","level_name":"지역명물","image_url":"","rank":"1"},{"level1":"A501","level2":"B501","level3":"C502","level_name":"양곡","image_url":"","rank":"2"},{"level1":"A501","level2":"B501","level3":"C503","level_name":"과실류","image_url":"","rank":"3"},{"level1":"A501","level2":"B501","level3":"C504","level_name":"채소류","image_url":"","rank":"4"},{"level1":"A501","level2":"B501","level3":"C505","level_name":"축산","image_url":"","rank":"5"},{"level1":"A501","level2":"B501","level3":"C506","level_name":"수산","image_url":"","rank":"6"},{"level1":"A501","level2":"B501","level3":"C507","level_name":"김치/반찬","image_url":"","rank":"7"},{"level1":"A501","level2":"B501","level3":"C508","level_name":"슈퍼푸드","image_url":"","rank":"8"},{"level1":"A501","level2":"B502","level3":"*","level_name":"가공식품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon20.png","rank":"2"},{"level1":"A501","level2":"B502","level3":"C501","level_name":"과자/간식","image_url":"","rank":"1"},{"level1":"A501","level2":"B502","level3":"C502","level_name":"라면/면류","image_url":"","rank":"2"},{"level1":"A501","level2":"B502","level3":"C503","level_name":"조미료/소스/설탕","image_url":"","rank":"3"},{"level1":"A501","level2":"B502","level3":"C504","level_name":"즉석조리식품","image_url":"","rank":"4"},{"level1":"A501","level2":"B502","level3":"C505","level_name":"통조림/식용유/잼","image_url":"","rank":"5"},{"level1":"A501","level2":"B502","level3":"C506","level_name":"명절 선물세트","image_url":"","rank":"6"},{"level1":"A501","level2":"B502","level3":"C507","level_name":"식품 브랜드관","image_url":"","rank":"7"},{"level1":"A501","level2":"B503","level3":"*","level_name":"건강식품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon21.png","rank":"3"},{"level1":"A501","level2":"B503","level3":"C501","level_name":"다이어트식품","image_url":"","rank":"1"},{"level1":"A501","level2":"B503","level3":"C502","level_name":"건강식품","image_url":"","rank":"2"},{"level1":"A501","level2":"B503","level3":"C503","level_name":"홍삼/인삼","image_url":"","rank":"3"},{"level1":"A501","level2":"B503","level3":"C504","level_name":"보충제","image_url":"","rank":"4"},{"level1":"A501","level2":"B503","level3":"C505","level_name":"브랜드관","image_url":"","rank":"5"},{"level1":"A501","level2":"B504","level3":"*","level_name":"커피/음료","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon22.png","rank":"4"},{"level1":"A501","level2":"B504","level3":"C501","level_name":"커피","image_url":"","rank":"1"},{"level1":"A501","level2":"B504","level3":"C502","level_name":"차/티백","image_url":"","rank":"2"},{"level1":"A501","level2":"B504","level3":"C503","level_name":"두유/우유","image_url":"","rank":"3"},{"level1":"A501","level2":"B504","level3":"C504","level_name":"생수/음료","image_url":"","rank":"4"},{"level1":"A501","level2":"B504","level3":"C505","level_name":"전통주","image_url":"","rank":"5"},{"level1":"A601","level2":"*","level3":"*","level_name":"생필품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca06.png","rank":"5"},{"level1":"A601","level2":"B601","level3":"*","level_name":"세제/구강","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon23.png","rank":"1"},{"level1":"A601","level2":"B601","level3":"C601","level_name":"세제/세정제","image_url":"","rank":"1"},{"level1":"A601","level2":"B601","level3":"C602","level_name":"세면용품","image_url":"","rank":"2"},{"level1":"A601","level2":"B601","level3":"C603","level_name":"생활용품","image_url":"","rank":"3"},{"level1":"A601","level2":"B601","level3":"C604","level_name":"전문관","image_url":"","rank":"4"},{"level1":"A601","level2":"B602","level3":"*","level_name":"화장지/물티슈/생리대","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon24.png","rank":"2"},{"level1":"A601","level2":"B602","level3":"C601","level_name":"화장지","image_url":"","rank":"1"},{"level1":"A601","level2":"B602","level3":"C602","level_name":"물티슈","image_url":"","rank":"2"},{"level1":"A601","level2":"B602","level3":"C603","level_name":"생리대","image_url":"","rank":"3"},{"level1":"A601","level2":"B602","level3":"C604","level_name":"성인기저귀","image_url":"","rank":"4"},{"level1":"A601","level2":"B603","level3":"*","level_name":"바디/헤어","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon25.png","rank":"3"},{"level1":"A601","level2":"B603","level3":"C601","level_name":"헤어케어","image_url":"","rank":"1"},{"level1":"A601","level2":"B603","level3":"C602","level_name":"바디로션/핸드크림","image_url":"","rank":"2"},{"level1":"A601","level2":"B603","level3":"C603","level_name":"바디케어","image_url":"","rank":"3"},{"level1":"A601","level2":"B603","level3":"C604","level_name":"헤어스타일링","image_url":"","rank":"4"},{"level1":"A601","level2":"B603","level3":"C605","level_name":"청결/바디용품","image_url":"","rank":"5"},{"level1":"A601","level2":"B603","level3":"C606","level_name":"브랜드관","image_url":"","rank":"6"},{"level1":"A701","level2":"*","level3":"*","level_name":"홈데코","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca07.png","rank":"6"},{"level1":"A701","level2":"B701","level3":"*","level_name":"가구/DIY","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon26.png","rank":"1"},{"level1":"A701","level2":"B701","level3":"C701","level_name":"어린이/학생가구","image_url":"","rank":"1"},{"level1":"A701","level2":"B701","level3":"C702","level_name":"침실가구","image_url":"","rank":"2"},{"level1":"A701","level2":"B701","level3":"C703","level_name":"거실가구","image_url":"","rank":"3"},{"level1":"A701","level2":"B701","level3":"C704","level_name":"주방가구","image_url":"","rank":"4"},{"level1":"A701","level2":"B701","level3":"C705","level_name":"가구리폼","image_url":"","rank":"5"},{"level1":"A701","level2":"B701","level3":"C706","level_name":"야외/정원가구","image_url":"","rank":"6"},{"level1":"A701","level2":"B701","level3":"C707","level_name":"이사/혼수/전시가구","image_url":"","rank":"7"},{"level1":"A701","level2":"B701","level3":"C708","level_name":"브랜드관","image_url":"","rank":"8"},{"level1":"A701","level2":"B702","level3":"*","level_name":"침구/커튼","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon27.png","rank":"2"},{"level1":"A701","level2":"B702","level3":"C701","level_name":"침구","image_url":"","rank":"1"},{"level1":"A701","level2":"B702","level3":"C702","level_name":"커튼/블라인드","image_url":"","rank":"2"},{"level1":"A701","level2":"B702","level3":"C703","level_name":"카페트","image_url":"","rank":"3"},{"level1":"A701","level2":"B702","level3":"C704","level_name":"홈패션/수예용품","image_url":"","rank":"4"},{"level1":"A701","level2":"B702","level3":"C705","level_name":"공예/DIY재료","image_url":"","rank":"5"},{"level1":"A701","level2":"B703","level3":"*","level_name":"조명/인테리어","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon28.png","rank":"3"},{"level1":"A701","level2":"B703","level3":"C701","level_name":"조명/스탠드","image_url":"","rank":"1"},{"level1":"A701","level2":"B703","level3":"C702","level_name":"홈프래그런스","image_url":"","rank":"2"},{"level1":"A701","level2":"B703","level3":"C703","level_name":"홈리뉴얼","image_url":"","rank":"3"},{"level1":"A701","level2":"B703","level3":"C704","level_name":"인테리어장식","image_url":"","rank":"4"},{"level1":"A701","level2":"B703","level3":"C705","level_name":"디자인 전문샵","image_url":"","rank":"5"},{"level1":"A701","level2":"B704","level3":"*","level_name":"생활용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon29.png","rank":"4"},{"level1":"A701","level2":"B704","level3":"C701","level_name":"수납정리","image_url":"","rank":"1"},{"level1":"A701","level2":"B704","level3":"C702","level_name":"욕실목욕용품","image_url":"","rank":"2"},{"level1":"A701","level2":"B704","level3":"C703","level_name":"청소세탁용품","image_url":"","rank":"3"},{"level1":"A701","level2":"B704","level3":"C704","level_name":"가정생활잡화","image_url":"","rank":"4"},{"level1":"A701","level2":"B704","level3":"C705","level_name":"디지털도어락/보안용품","image_url":"","rank":"5"},{"level1":"A701","level2":"B704","level3":"C706","level_name":"서비스/렌탈","image_url":"","rank":"6"},{"level1":"A701","level2":"B705","level3":"*","level_name":"주방용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon30.png","rank":"5"},{"level1":"A701","level2":"B705","level3":"C701","level_name":"쿡웨어","image_url":"","rank":"1"},{"level1":"A701","level2":"B705","level3":"C702","level_name":"테이블웨어","image_url":"","rank":"2"},{"level1":"A701","level2":"B705","level3":"C703","level_name":"보관/밀폐용기","image_url":"","rank":"3"},{"level1":"A701","level2":"B705","level3":"C704","level_name":"주방용품","image_url":"","rank":"4"},{"level1":"A701","level2":"B705","level3":"C705","level_name":"전문주방용품","image_url":"","rank":"5"},{"level1":"A701","level2":"B706","level3":"*","level_name":"꽃/이벤트용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon31.png","rank":"6"},{"level1":"A701","level2":"B706","level3":"C701","level_name":"꽃배달","image_url":"","rank":"1"},{"level1":"A701","level2":"B706","level3":"C702","level_name":"원예","image_url":"","rank":"2"},{"level1":"A701","level2":"B706","level3":"C703","level_name":"원예용품","image_url":"","rank":"3"},{"level1":"A701","level2":"B706","level3":"C704","level_name":"조화/트리","image_url":"","rank":"4"},{"level1":"A701","level2":"B706","level3":"C705","level_name":"파티용품","image_url":"","rank":"5"},{"level1":"A801","level2":"*","level3":"*","level_name":"건강","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca08.png","rank":"7"},{"level1":"A801","level2":"B801","level3":"*","level_name":"건강/의료용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon32.png","rank":"1"},{"level1":"A801","level2":"B801","level3":"C801","level_name":"건강관리","image_url":"","rank":"1"},{"level1":"A801","level2":"B801","level3":"C802","level_name":"기능성 건강용품","image_url":"","rank":"2"},{"level1":"A801","level2":"B801","level3":"C803","level_name":"안마의자/용품","image_url":"","rank":"3"},{"level1":"A801","level2":"B801","level3":"C804","level_name":"정수기/비데","image_url":"","rank":"4"},{"level1":"A801","level2":"B801","level3":"C805","level_name":"병원/의원/약국용품","image_url":"","rank":"5"},{"level1":"A801","level2":"B801","level3":"C806","level_name":"실버/성인용품","image_url":"","rank":"6"},{"level1":"A801","level2":"B802","level3":"*","level_name":"건강식품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon33.png","rank":"2"},{"level1":"A801","level2":"B802","level3":"C801","level_name":"다이어트식품","image_url":"","rank":"1"},{"level1":"A801","level2":"B802","level3":"C802","level_name":"건강식품","image_url":"","rank":"2"},{"level1":"A801","level2":"B802","level3":"C803","level_name":"홍삼/인삼","image_url":"","rank":"3"},{"level1":"A801","level2":"B802","level3":"C804","level_name":"보충제","image_url":"","rank":"4"},{"level1":"A801","level2":"B802","level3":"C805","level_name":"브랜드관","image_url":"","rank":"5"},{"level1":"A901","level2":"*","level3":"*","level_name":"문구/취미","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca09.png","rank":"8"},{"level1":"A901","level2":"B901","level3":"*","level_name":"문구/사무용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon34.png","rank":"1"},{"level1":"A901","level2":"B901","level3":"C901","level_name":"필기구","image_url":"","rank":"1"},{"level1":"A901","level2":"B901","level3":"C902","level_name":"지류","image_url":"","rank":"2"},{"level1":"A901","level2":"B901","level3":"C903","level_name":"교육용품","image_url":"","rank":"3"},{"level1":"A901","level2":"B901","level3":"C904","level_name":"파일/사무용품","image_url":"","rank":"4"},{"level1":"A901","level2":"B901","level3":"C905","level_name":"앨범/팬시용품","image_url":"","rank":"5"},{"level1":"A901","level2":"B901","level3":"C906","level_name":"화방/제도용품","image_url":"","rank":"6"},{"level1":"A901","level2":"B901","level3":"C907","level_name":"설치/제작","image_url":"","rank":"7"},{"level1":"A901","level2":"B901","level3":"C908","level_name":"포장용품","image_url":"","rank":"8"},{"level1":"A901","level2":"B901","level3":"C909","level_name":"전문관","image_url":"","rank":"9"},{"level1":"A901","level2":"B902","level3":"*","level_name":"사무기기","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon35.png","rank":"2"},{"level1":"A901","level2":"B902","level3":"C901","level_name":"사무기기","image_url":"","rank":"1"},{"level1":"A901","level2":"B903","level3":"*","level_name":"악기/수집","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon36.png","rank":"3"},{"level1":"A901","level2":"B903","level3":"C901","level_name":"악기/악기용품","image_url":"","rank":"1"},{"level1":"A901","level2":"B903","level3":"C902","level_name":"수집용품","image_url":"","rank":"2"},{"level1":"A901","level2":"B903","level3":"C903","level_name":"전문수집","image_url":"","rank":"3"},{"level1":"A901","level2":"B904","level3":"*","level_name":"반려동물용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon37.png","rank":"4"},{"level1":"A901","level2":"B904","level3":"C901","level_name":"강아지 먹거리","image_url":"","rank":"1"},{"level1":"A901","level2":"B904","level3":"C902","level_name":"강아지 미용용품","image_url":"","rank":"2"},{"level1":"A901","level2":"B904","level3":"C903","level_name":"강아지 패션잡화","image_url":"","rank":"3"},{"level1":"A901","level2":"B904","level3":"C904","level_name":"강아지 생활용품","image_url":"","rank":"4"},{"level1":"A901","level2":"B904","level3":"C905","level_name":"고양이 먹거리","image_url":"","rank":"5"},{"level1":"A901","level2":"B904","level3":"C906","level_name":"고양이 이미용용품","image_url":"","rank":"6"},{"level1":"A901","level2":"B904","level3":"C907","level_name":"고양이 놀이용품","image_url":"","rank":"7"},{"level1":"A901","level2":"B904","level3":"C908","level_name":"고양이 생활용품","image_url":"","rank":"8"},{"level1":"A901","level2":"B904","level3":"C909","level_name":"수조/관상어용품","image_url":"","rank":"9"},{"level1":"A901","level2":"B904","level3":"C910","level_name":"소동물용품","image_url":"","rank":"10"},{"level1":"A1001","level2":"*","level3":"*","level_name":"스포츠","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca10.png","rank":"9"},{"level1":"A1001","level2":"B1001","level3":"*","level_name":"스포츠의류/운동화","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon38.png","rank":"1"},{"level1":"A1001","level2":"B1001","level3":"C1001","level_name":"스포츠 남성의류","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1001","level3":"C1002","level_name":"스포츠 여성의류","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1001","level3":"C1003","level_name":"스포츠 이너웨어","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1001","level3":"C1004","level_name":"스포츠 신발","image_url":"","rank":"4"},{"level1":"A1001","level2":"B1001","level3":"C1005","level_name":"스포츠 잡화","image_url":"","rank":"5"},{"level1":"A1001","level2":"B1001","level3":"C1006","level_name":"공식스토어","image_url":"","rank":"6"},{"level1":"A1001","level2":"B1002","level3":"*","level_name":"휘트니스/수영","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon39.png","rank":"2"},{"level1":"A1001","level2":"B1002","level3":"C1001","level_name":"헬스","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1002","level3":"C1002","level_name":"요가","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1002","level3":"C1003","level_name":"수영","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1002","level3":"C1004","level_name":"격투스포츠","image_url":"","rank":"4"},{"level1":"A1001","level2":"B1003","level3":"*","level_name":"구기/라켓","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon40.png","rank":"3"},{"level1":"A1001","level2":"B1003","level3":"C1001","level_name":"구기스포츠","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1003","level3":"C1002","level_name":"라켓스포츠","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1003","level3":"C1003","level_name":"학교스포츠","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1003","level3":"C1004","level_name":"생활스포츠","image_url":"","rank":"4"},{"level1":"A1001","level2":"B1003","level3":"C1005","level_name":"스포츠용품","image_url":"","rank":"5"},{"level1":"A1001","level2":"B1003","level3":"C1006","level_name":"용품잡화","image_url":"","rank":"6"},{"level1":"A1001","level2":"B1003","level3":"C1007","level_name":"사회인야구","image_url":"","rank":"7"},{"level1":"A1001","level2":"B1003","level3":"C1008","level_name":"프로야구/MLB 팬샵","image_url":"","rank":"8"},{"level1":"A1001","level2":"B1004","level3":"*","level_name":"골프","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon41.png","rank":"4"},{"level1":"A1001","level2":"B1004","level3":"C1001","level_name":"골프클럽","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1004","level3":"C1002","level_name":"골프용품","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1004","level3":"C1003","level_name":"골프의류","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1004","level3":"C1004","level_name":"브랜드 스토어","image_url":"","rank":"4"},{"level1":"A1001","level2":"B1004","level3":"C1005","level_name":"브랜드 골프웨어","image_url":"","rank":"5"},{"level1":"A1001","level2":"B1005","level3":"*","level_name":"자전거/보드","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon42.png","rank":"5"},{"level1":"A1001","level2":"B1005","level3":"C1001","level_name":"전동휠/레저","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1005","level3":"C1002","level_name":"보드/스키","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1005","level3":"C1003","level_name":"인라인/킥보드","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1005","level3":"C1004","level_name":"자전거","image_url":"","rank":"4"},{"level1":"A1001","level2":"B1005","level3":"C1005","level_name":"자전거의류/잡화","image_url":"","rank":"5"},{"level1":"A1001","level2":"B1005","level3":"C1006","level_name":"자전거용품","image_url":"","rank":"6"},{"level1":"A1001","level2":"B1005","level3":"C1007","level_name":"자전거부품","image_url":"","rank":"7"},{"level1":"A1001","level2":"B1006","level3":"*","level_name":"캠핑/낚시","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon43.png","rank":"6"},{"level1":"A1001","level2":"B1006","level3":"C1001","level_name":"캠핑","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1006","level3":"C1002","level_name":"낚시","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1006","level3":"C1003","level_name":"브랜드 스토어","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1007","level3":"*","level_name":"등산","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon44.png","rank":"7"},{"level1":"A1001","level2":"B1007","level3":"C1001","level_name":"등산복","image_url":"","rank":"1"},{"level1":"A1001","level2":"B1007","level3":"C1002","level_name":"등산화/트레킹화","image_url":"","rank":"2"},{"level1":"A1001","level2":"B1007","level3":"C1003","level_name":"등산용품/장비","image_url":"","rank":"3"},{"level1":"A1001","level2":"B1007","level3":"C1004","level_name":"브랜드 스토어","image_url":"","rank":"4"},{"level1":"A1101","level2":"*","level3":"*","level_name":"자동차/공구","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca11.png","rank":"10"},{"level1":"A1101","level2":"B1101","level3":"*","level_name":"자동차용품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon45.png","rank":"1"},{"level1":"A1101","level2":"B1101","level3":"C1101","level_name":"내부용품","image_url":"","rank":"1"},{"level1":"A1101","level2":"B1101","level3":"C1102","level_name":"외부용품","image_url":"","rank":"2"},{"level1":"A1101","level2":"B1101","level3":"C1103","level_name":"네비게이션/블랙박스","image_url":"","rank":"3"},{"level1":"A1101","level2":"B1101","level3":"C1104","level_name":"하이패스/카오디오","image_url":"","rank":"4"},{"level1":"A1101","level2":"B1101","level3":"C1105","level_name":"오토바이/용품","image_url":"","rank":"5"},{"level1":"A1101","level2":"B1102","level3":"*","level_name":"타이어/부품","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon46.png","rank":"2"},{"level1":"A1101","level2":"B1102","level3":"C1101","level_name":"타이어/장착","image_url":"","rank":"1"},{"level1":"A1101","level2":"B1102","level3":"C1102","level_name":"엔진오일/배터리","image_url":"","rank":"2"},{"level1":"A1101","level2":"B1102","level3":"C1103","level_name":"부품/튜닝","image_url":"","rank":"3"},{"level1":"A1101","level2":"B1103","level3":"*","level_name":"공구설비/자재","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon47.png","rank":"3"},{"level1":"A1101","level2":"B1103","level3":"C1101","level_name":"작업공구","image_url":"","rank":"1"},{"level1":"A1101","level2":"B1103","level3":"C1102","level_name":"전동/엔진/기계","image_url":"","rank":"2"},{"level1":"A1101","level2":"B1103","level3":"C1103","level_name":"산업안전용품","image_url":"","rank":"3"},{"level1":"A1101","level2":"B1103","level3":"C1104","level_name":"전기,산업자재/DIY","image_url":"","rank":"4"},{"level1":"A1101","level2":"B1103","level3":"C1105","level_name":"브랜드 스토어","image_url":"","rank":"5"},{"level1":"A1201","level2":"*","level3":"*","level_name":"가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca12.png","rank":"11"},{"level1":"A1201","level2":"B1201","level3":"*","level_name":"대형가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon48.png","rank":"1"},{"level1":"A1201","level2":"B1201","level3":"C1201","level_name":"TV","image_url":"","rank":"1"},{"level1":"A1201","level2":"B1201","level3":"C1202","level_name":"TV액세서리","image_url":"","rank":"2"},{"level1":"A1201","level2":"B1201","level3":"C1203","level_name":"냉장고","image_url":"","rank":"3"},{"level1":"A1201","level2":"B1201","level3":"C1204","level_name":"세탁기","image_url":"","rank":"4"},{"level1":"A1201","level2":"B1201","level3":"C1205","level_name":"프로젝터","image_url":"","rank":"5"},{"level1":"A1201","level2":"B1201","level3":"C1206","level_name":"중고/기타","image_url":"","rank":"6"},{"level1":"A1201","level2":"B1202","level3":"*","level_name":"주방가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon49.png","rank":"2"},{"level1":"A1201","level2":"B1202","level3":"C1201","level_name":"주방가전","image_url":"","rank":"1"},{"level1":"A1201","level2":"B1202","level3":"C1202","level_name":"홈메이드가전","image_url":"","rank":"2"},{"level1":"A1201","level2":"B1202","level3":"C1203","level_name":"커피메이커","image_url":"","rank":"3"},{"level1":"A1201","level2":"B1202","level3":"C1204","level_name":"블렌더/기타","image_url":"","rank":"4"},{"level1":"A1201","level2":"B1203","level3":"*","level_name":"계절가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon50.png","rank":"3"},{"level1":"A1201","level2":"B1203","level3":"C1201","level_name":"가습/청전가전","image_url":"","rank":"1"},{"level1":"A1201","level2":"B1203","level3":"C1202","level_name":"전기매트/요","image_url":"","rank":"2"},{"level1":"A1201","level2":"B1203","level3":"C1203","level_name":"남방가전","image_url":"","rank":"3"},{"level1":"A1201","level2":"B1203","level3":"C1204","level_name":"보일러/온수기","image_url":"","rank":"4"},{"level1":"A1201","level2":"B1203","level3":"C1205","level_name":"선풍기","image_url":"","rank":"5"},{"level1":"A1201","level2":"B1203","level3":"C1206","level_name":"에어컨","image_url":"","rank":"6"},{"level1":"A1201","level2":"B1203","level3":"C1207","level_name":"여름가전","image_url":"","rank":"7"},{"level1":"A1201","level2":"B1203","level3":"C1208","level_name":"타워팬","image_url":"","rank":"8"},{"level1":"A1201","level2":"B1204","level3":"*","level_name":"생활/미용가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon51.png","rank":"4"},{"level1":"A1201","level2":"B1204","level3":"C1201","level_name":"청소기","image_url":"","rank":"1"},{"level1":"A1201","level2":"B1204","level3":"C1202","level_name":"생활가전","image_url":"","rank":"2"},{"level1":"A1201","level2":"B1204","level3":"C1203","level_name":"이미용가전","image_url":"","rank":"3"},{"level1":"A1201","level2":"B1204","level3":"C1204","level_name":"사무가전","image_url":"","rank":"4"},{"level1":"A1201","level2":"B1204","level3":"C1205","level_name":"중고/소형가전","image_url":"","rank":"5"},{"level1":"A1201","level2":"B1205","level3":"*","level_name":"음향가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon52.png","rank":"5"},{"level1":"A1201","level2":"B1205","level3":"C1201","level_name":"이어폰","image_url":"","rank":"1"},{"level1":"A1201","level2":"B1205","level3":"C1202","level_name":"헤드폰","image_url":"","rank":"2"},{"level1":"A1201","level2":"B1205","level3":"C1203","level_name":"스피커","image_url":"","rank":"3"},{"level1":"A1201","level2":"B1205","level3":"C1204","level_name":"홈시어터/오디오","image_url":"","rank":"4"},{"level1":"A1201","level2":"B1205","level3":"C1205","level_name":"HiFi / 소스기기","image_url":"","rank":"5"},{"level1":"A1201","level2":"B1205","level3":"C1206","level_name":"휴대용 음향기기","image_url":"","rank":"6"},{"level1":"A1201","level2":"B1205","level3":"C1207","level_name":"노래방/마이크","image_url":"","rank":"7"},{"level1":"A1201","level2":"B1206","level3":"*","level_name":"건강가전","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon53.png","rank":"6"},{"level1":"A1201","level2":"B1206","level3":"C1201","level_name":"건강관리","image_url":"","rank":"1"},{"level1":"A1201","level2":"B1206","level3":"C1202","level_name":"기능성 건강용품","image_url":"","rank":"2"},{"level1":"A1201","level2":"B1206","level3":"C1203","level_name":"안마의자/용품","image_url":"","rank":"3"},{"level1":"A1201","level2":"B1206","level3":"C1204","level_name":"정수기/비데","image_url":"","rank":"4"},{"level1":"A1201","level2":"B1206","level3":"C1205","level_name":"병원/의원/약국용품","image_url":"","rank":"5"},{"level1":"A1201","level2":"B1206","level3":"C1206","level_name":"실버/성인용품","image_url":"","rank":"6"},{"level1":"A1301","level2":"*","level3":"*","level_name":"디지털","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca13.png","rank":"12"},{"level1":"A1301","level2":"B1301","level3":"*","level_name":"휴대폰","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon54.png","rank":"1"},{"level1":"A1301","level2":"B1301","level3":"C1301","level_name":"휴대폰 가입상품","image_url":"","rank":"1"},{"level1":"A1301","level2":"B1301","level3":"C1302","level_name":"공기계/중고폰","image_url":"","rank":"2"},{"level1":"A1301","level2":"B1301","level3":"C1303","level_name":"휴대폰 케이스","image_url":"","rank":"3"},{"level1":"A1301","level2":"B1301","level3":"C1304","level_name":"휴대폰 액세서리","image_url":"","rank":"4"},{"level1":"A1301","level2":"B1301","level3":"C1305","level_name":"충전/배터리","image_url":"","rank":"5"},{"level1":"A1301","level2":"B1301","level3":"C1306","level_name":"웨어러블/액세러리","image_url":"","rank":"6"},{"level1":"A1301","level2":"B1301","level3":"C1307","level_name":"인터넷 결합상품","image_url":"","rank":"7"},{"level1":"A1301","level2":"B1301","level3":"C1308","level_name":"태블릿 액세서리","image_url":"","rank":"8"},{"level1":"A1301","level2":"B1301","level3":"C1309","level_name":"휴대폰샵","image_url":"","rank":"9"},{"level1":"A1301","level2":"B1302","level3":"*","level_name":"카메라","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon55.png","rank":"2"},{"level1":"A1301","level2":"B1302","level3":"C1301","level_name":"카메라/렌즈","image_url":"","rank":"1"},{"level1":"A1301","level2":"B1302","level3":"C1302","level_name":"사진출력","image_url":"","rank":"2"},{"level1":"A1301","level2":"B1302","level3":"C1303","level_name":"저장장치","image_url":"","rank":"3"},{"level1":"A1301","level2":"B1302","level3":"C1304","level_name":"동영상","image_url":"","rank":"4"},{"level1":"A1301","level2":"B1302","level3":"C1305","level_name":"액세서리","image_url":"","rank":"5"},{"level1":"A1301","level2":"B1302","level3":"C1306","level_name":"중고/리퍼/반품/전시","image_url":"","rank":"6"},{"level1":"A1301","level2":"B1302","level3":"C1307","level_name":"헬리캠/드론","image_url":"","rank":"7"},{"level1":"A1301","level2":"B1302","level3":"C1308","level_name":"브랜드 가방/삼각대","image_url":"","rank":"8"},{"level1":"A1301","level2":"B1303","level3":"*","level_name":"태블릿","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon56.png","rank":"3"},{"level1":"A1301","level2":"B1303","level3":"C1301","level_name":"태블릿PC","image_url":"","rank":"1"},{"level1":"A1301","level2":"B1303","level3":"C1302","level_name":"전자사전/학습기","image_url":"","rank":"2"},{"level1":"A1301","level2":"B1303","level3":"C1303","level_name":"메모리카드","image_url":"","rank":"3"},{"level1":"A1301","level2":"B1303","level3":"C1304","level_name":"인텔인사이드","image_url":"","rank":"4"},{"level1":"A1301","level2":"B1304","level3":"*","level_name":"게임","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon57.png","rank":"4"},{"level1":"A1301","level2":"B1304","level3":"C1301","level_name":"게임기","image_url":"","rank":"1"},{"level1":"A1301","level2":"B1304","level3":"C1302","level_name":"게임타이틀","image_url":"","rank":"2"},{"level1":"A1301","level2":"B1304","level3":"C1303","level_name":"게임주변기기","image_url":"","rank":"3"},{"level1":"A1301","level2":"B1304","level3":"C1304","level_name":"중고","image_url":"","rank":"4"},{"level1":"A1301","level2":"B1304","level3":"C1305","level_name":"게임이용권","image_url":"","rank":"5"},{"level1":"A1301","level2":"B1304","level3":"C1306","level_name":"게이밍 노트북/모니터/주변기기","image_url":"","rank":"6"},{"level1":"A1301","level2":"B1305","level3":"*","level_name":"음향기기","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon58.png","rank":"5"},{"level1":"A1301","level2":"B1305","level3":"C1301","level_name":"이어폰","image_url":"","rank":"1"},{"level1":"A1301","level2":"B1305","level3":"C1302","level_name":"헤드폰","image_url":"","rank":"2"},{"level1":"A1301","level2":"B1305","level3":"C1303","level_name":"스피커","image_url":"","rank":"3"},{"level1":"A1301","level2":"B1305","level3":"C1304","level_name":"홈시어터/오디오","image_url":"","rank":"4"},{"level1":"A1301","level2":"B1305","level3":"C1305","level_name":"HiFi / 소스기기","image_url":"","rank":"5"},{"level1":"A1301","level2":"B1305","level3":"C1306","level_name":"휴대용 음향기기","image_url":"","rank":"6"},{"level1":"A1301","level2":"B1305","level3":"C1307","level_name":"노래방/마이크","image_url":"","rank":"7"},{"level1":"A1401","level2":"*","level3":"*","level_name":"컴퓨터","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/ca14.png","rank":"13"},{"level1":"A1401","level2":"B1401","level3":"*","level_name":"노트북/PC","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon59.png","rank":"1"},{"level1":"A1401","level2":"B1401","level3":"C1401","level_name":"노트북","image_url":"","rank":"1"},{"level1":"A1401","level2":"B1401","level3":"C1402","level_name":"노트북 액세서리","image_url":"","rank":"2"},{"level1":"A1401","level2":"B1401","level3":"C1403","level_name":"데스크탑","image_url":"","rank":"3"},{"level1":"A1401","level2":"B1401","level3":"C1404","level_name":"조립컴퓨터","image_url":"","rank":"4"},{"level1":"A1401","level2":"B1401","level3":"C1405","level_name":"태블릿PC","image_url":"","rank":"5"},{"level1":"A1401","level2":"B1401","level3":"C1406","level_name":"중고/리퍼/반품/전시","image_url":"","rank":"6"},{"level1":"A1401","level2":"B1401","level3":"C1407","level_name":"Microsoft Surface","image_url":"","rank":"7"},{"level1":"A1401","level2":"B1402","level3":"*","level_name":"모니터/주변기기","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon60.png","rank":"2"},{"level1":"A1401","level2":"B1402","level3":"C1401","level_name":"모니터","image_url":"","rank":"1"},{"level1":"A1401","level2":"B1402","level3":"C1402","level_name":"레이저복합기/프린터","image_url":"","rank":"2"},{"level1":"A1401","level2":"B1402","level3":"C1403","level_name":"잉크젯복합기/프린터","image_url":"","rank":"3"},{"level1":"A1401","level2":"B1402","level3":"C1404","level_name":"토너","image_url":"","rank":"4"},{"level1":"A1401","level2":"B1402","level3":"C1405","level_name":"잉크","image_url":"","rank":"5"},{"level1":"A1401","level2":"B1402","level3":"C1406","level_name":"포토/라벨/3D/스캐너","image_url":"","rank":"6"},{"level1":"A1401","level2":"B1403","level3":"*","level_name":"PC주변기기","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon61.png","rank":"3"},{"level1":"A1401","level2":"B1403","level3":"C1401","level_name":"컴퓨터 주변기기/용품","image_url":"","rank":"1"},{"level1":"A1401","level2":"B1403","level3":"C1402","level_name":"네트워크/보안","image_url":"","rank":"2"},{"level1":"A1401","level2":"B1403","level3":"C1403","level_name":"스피커/헤드셋","image_url":"","rank":"3"},{"level1":"A1401","level2":"B1403","level3":"C1404","level_name":"케이블","image_url":"","rank":"4"},{"level1":"A1401","level2":"B1403","level3":"C1405","level_name":"컴퓨터 부품","image_url":"","rank":"5"},{"level1":"A1401","level2":"B1403","level3":"C1406","level_name":"소프트웨어","image_url":"","rank":"6"},{"level1":"A1401","level2":"B1403","level3":"C1407","level_name":"기업용/게임용","image_url":"","rank":"7"},{"level1":"A1401","level2":"B1404","level3":"*","level_name":"저장장치","image_url":"http://fileserver.gigawon.cn:8588/GigaShopMall/icon62.png","rank":"4"},{"level1":"A1401","level2":"B1404","level3":"C1401","level_name":"외장하드/USB메모리","image_url":"","rank":"1"},{"level1":"A1401","level2":"B1404","level3":"C1402","level_name":"SSD/HDD","image_url":"","rank":"2"},{"level1":"A1401","level2":"B1404","level3":"C1403","level_name":"NAS/ODD/DIVX","image_url":"","rank":"3"},{"level1":"A1401","level2":"B1404","level3":"C1404","level_name":"메모리","image_url":"","rank":"4"},{"level1":"A1401","level2":"B1404","level3":"C1405","level_name":"무선","image_url":"","rank":"5"}]
     * LeftBottomCate21 : [{"level1":"A201","level2":"B201","level_name":"여성의류"},{"level1":"A201","level2":"B202","level_name":"남성의류"},{"level1":"A201","level2":"B203","level_name":"언더웨어"},{"level1":"A201","level2":"B204","level_name":"유아동의류"},{"level1":"A301","level2":"B301","level_name":"신발"},{"level1":"A301","level2":"B302","level_name":"가방/잡화"},{"level1":"A301","level2":"B303","level_name":"유아동신발/잡화"},{"level1":"A301","level2":"B304","level_name":"쥬얼리/시계"},{"level1":"A301","level2":"B305","level_name":"수입명품"},{"level1":"A301","level2":"B306","level_name":"백화점 명품화장품"},{"level1":"A301","level2":"B307","level_name":"화장품/향수"},{"level1":"A301","level2":"B308","level_name":"바디/헤어"}]
     * LeftBottomCate22 : [{"level1":"A701","level2":"B701","level_name":"가구/DIY"},{"level1":"A701","level2":"B702","level_name":"침구/커튼"},{"level1":"A701","level2":"B703","level_name":"조명/인테리어"},{"level1":"A701","level2":"B704","level_name":"생활용품"},{"level1":"A701","level2":"B705","level_name":"주방용품"},{"level1":"A701","level2":"B706","level_name":"꽃/이벤트용품"},{"level1":"A901","level2":"B901","level_name":"문구/사무용품"},{"level1":"A901","level2":"B902","level_name":"사무기기"},{"level1":"A901","level2":"B903","level_name":"악기/수집"},{"level1":"A901","level2":"B904","level_name":"반려동물용품"}]
     * status : 1
     * flag : 1501
     * msg : 작업완료
     * start_page : null
     * end_page : null
     * pg : null
     * tot_page : null
     * tot_cnt : null
     */

    private String status;
    private String flag;
    private String msg;
    private Object start_page;
    private Object end_page;
    private Object pg;
    private Object tot_page;
    private Object tot_cnt;
    private List<Bannerlist10Bean> bannerlist10;
    private List<Bannerlist15Bean> bannerlist15;
    private List<?> bannerlist18;
    private List<Eventtype20Bean> eventtype20;
    private List<Eventtype21Bean> eventtype21;
    private List<Eventtype22Bean> eventtype22;
    private List<ItemlevelBean> itemlevel;
    private List<LeftBottomCate21Bean> LeftBottomCate21;
    private List<LeftBottomCate22Bean> LeftBottomCate22;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getStart_page() {
        return start_page;
    }

    public void setStart_page(Object start_page) {
        this.start_page = start_page;
    }

    public Object getEnd_page() {
        return end_page;
    }

    public void setEnd_page(Object end_page) {
        this.end_page = end_page;
    }

    public Object getPg() {
        return pg;
    }

    public void setPg(Object pg) {
        this.pg = pg;
    }

    public Object getTot_page() {
        return tot_page;
    }

    public void setTot_page(Object tot_page) {
        this.tot_page = tot_page;
    }

    public Object getTot_cnt() {
        return tot_cnt;
    }

    public void setTot_cnt(Object tot_cnt) {
        this.tot_cnt = tot_cnt;
    }

    public List<Bannerlist10Bean> getBannerlist10() {
        return bannerlist10;
    }

    public void setBannerlist10(List<Bannerlist10Bean> bannerlist10) {
        this.bannerlist10 = bannerlist10;
    }

    public List<Bannerlist15Bean> getBannerlist15() {
        return bannerlist15;
    }

    public void setBannerlist15(List<Bannerlist15Bean> bannerlist15) {
        this.bannerlist15 = bannerlist15;
    }

    public List<?> getBannerlist18() {
        return bannerlist18;
    }

    public void setBannerlist18(List<?> bannerlist18) {
        this.bannerlist18 = bannerlist18;
    }

    public List<Eventtype20Bean> getEventtype20() {
        return eventtype20;
    }

    public void setEventtype20(List<Eventtype20Bean> eventtype20) {
        this.eventtype20 = eventtype20;
    }

    public List<Eventtype21Bean> getEventtype21() {
        return eventtype21;
    }

    public void setEventtype21(List<Eventtype21Bean> eventtype21) {
        this.eventtype21 = eventtype21;
    }

    public List<Eventtype22Bean> getEventtype22() {
        return eventtype22;
    }

    public void setEventtype22(List<Eventtype22Bean> eventtype22) {
        this.eventtype22 = eventtype22;
    }

    public List<ItemlevelBean> getItemlevel() {
        return itemlevel;
    }

    public void setItemlevel(List<ItemlevelBean> itemlevel) {
        this.itemlevel = itemlevel;
    }

    public List<LeftBottomCate21Bean> getLeftBottomCate21() {
        return LeftBottomCate21;
    }

    public void setLeftBottomCate21(List<LeftBottomCate21Bean> LeftBottomCate21) {
        this.LeftBottomCate21 = LeftBottomCate21;
    }

    public List<LeftBottomCate22Bean> getLeftBottomCate22() {
        return LeftBottomCate22;
    }

    public void setLeftBottomCate22(List<LeftBottomCate22Bean> LeftBottomCate22) {
        this.LeftBottomCate22 = LeftBottomCate22;
    }

    public static class Bannerlist10Bean {
        /**
         * ad_title : 소상공인 BANNER 1
         * ad_image : http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/TopBanner01.png
         * link_url :
         * custom_code : 010530117822fbe4
         * item_code : 180311020443
         */

        private String ad_title;
        private String ad_image;
        private String link_url;
        private String custom_code;
        private String item_code;

        public String getAd_title() {
            return ad_title;
        }

        public void setAd_title(String ad_title) {
            this.ad_title = ad_title;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getLink_url() {
            return link_url;
        }

        public void setLink_url(String link_url) {
            this.link_url = link_url;
        }

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getItem_code() {
            return item_code;
        }

        public void setItem_code(String item_code) {
            this.item_code = item_code;
        }
    }

    public static class Bannerlist15Bean {
        /**
         * ad_title : 소상공인 BANNER 중앙
         * ad_image : http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/MidBanner.png
         * link_url :
         * custom_code : 010530117822fbe4
         * item_code : 180311019486
         */

        private String ad_title;
        private String ad_image;
        private String link_url;
        private String custom_code;
        private String item_code;

        public String getAd_title() {
            return ad_title;
        }

        public void setAd_title(String ad_title) {
            this.ad_title = ad_title;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getLink_url() {
            return link_url;
        }

        public void setLink_url(String link_url) {
            this.link_url = link_url;
        }

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getItem_code() {
            return item_code;
        }

        public void setItem_code(String item_code) {
            this.item_code = item_code;
        }
    }

    public static class Eventtype20Bean {
        /**
         * ad_title : 핫딜상품 구매하면 배송이 공짜
         * sub_title : 2018년 1월까지 주문하세요
         * ad_image : http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/LeftMidBanner.png
         * link_url :
         * custom_code : 010530117822fbe4
         * item_code : 180311011668
         * item_name : [BVLGARI]불가리 오떼블랑 40ml 3종세트
         * item_p : 40500.00
         * market_p : 45000.00
         * item_img_url : http://fileserver.gigawon.cn:8588/Item/main_0012.png
         */

        private String ad_title;
        private String sub_title;
        private String ad_image;
        private String link_url;
        private String custom_code;
        private String item_code;
        private String item_name;
        private String item_p;
        private String market_p;
        private String item_img_url;

        public String getAd_title() {
            return ad_title;
        }

        public void setAd_title(String ad_title) {
            this.ad_title = ad_title;
        }

        public String getSub_title() {
            return sub_title;
        }

        public void setSub_title(String sub_title) {
            this.sub_title = sub_title;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getLink_url() {
            return link_url;
        }

        public void setLink_url(String link_url) {
            this.link_url = link_url;
        }

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getItem_code() {
            return item_code;
        }

        public void setItem_code(String item_code) {
            this.item_code = item_code;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getItem_p() {
            return item_p;
        }

        public void setItem_p(String item_p) {
            this.item_p = item_p;
        }

        public String getMarket_p() {
            return market_p;
        }

        public void setMarket_p(String market_p) {
            this.market_p = market_p;
        }

        public String getItem_img_url() {
            return item_img_url;
        }

        public void setItem_img_url(String item_img_url) {
            this.item_img_url = item_img_url;
        }
    }

    public static class Eventtype21Bean {
        /**
         * ad_title : 패션의류/잡화.뷰티
         * sub_title :
         * ad_image : http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main07.png
         * link_url : http://www.gigawon.co.kr:8800/html/product_detail?code=180311016352&supply_code=010530117822fbe4
         * custom_code : 010530117822fbe4
         * item_code : 180311001600
         * item_name : 과일나라 후르츠왁스 헤어칼라 진주펄 [05호 밝은 ...
         * item_p : 5400.00
         * market_p : 6000.00
         * item_img_url : http://fileserver.gigawon.cn:8588/Item/main_0021.png
         */

        private String ad_title;
        private String sub_title;
        private String ad_image;
        private String link_url;
        private String custom_code;
        private String item_code;
        private String item_name;
        private String item_p;
        private String market_p;
        private String item_img_url;

        public String getAd_title() {
            return ad_title;
        }

        public void setAd_title(String ad_title) {
            this.ad_title = ad_title;
        }

        public String getSub_title() {
            return sub_title;
        }

        public void setSub_title(String sub_title) {
            this.sub_title = sub_title;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getLink_url() {
            return link_url;
        }

        public void setLink_url(String link_url) {
            this.link_url = link_url;
        }

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getItem_code() {
            return item_code;
        }

        public void setItem_code(String item_code) {
            this.item_code = item_code;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getItem_p() {
            return item_p;
        }

        public void setItem_p(String item_p) {
            this.item_p = item_p;
        }

        public String getMarket_p() {
            return market_p;
        }

        public void setMarket_p(String market_p) {
            this.market_p = market_p;
        }

        public String getItem_img_url() {
            return item_img_url;
        }

        public void setItem_img_url(String item_img_url) {
            this.item_img_url = item_img_url;
        }
    }

    public static class Eventtype22Bean {
        /**
         * ad_title : 홈데코/문구.취미
         * sub_title :
         * ad_image : http://fileserver.gigawon.cn:8588/GigaShopMall/MainPage/main08.png
         * link_url : http://www.gigawon.co.kr:8800/html/product_detail?code=180311019888&supply_code=010530117822fbe4
         * custom_code : 010530117822fbe4
         * item_code : 180311012083
         * item_name : 세차의 혁명! 매직호스 풀세트(30M)/실리콘재질....
         * item_p : 14900.00
         * market_p : 15900.00
         * item_img_url : http://fileserver.gigawon.cn:8588/Item/main_0032.png
         */

        private String ad_title;
        private String sub_title;
        private String ad_image;
        private String link_url;
        private String custom_code;
        private String item_code;
        private String item_name;
        private String item_p;
        private String market_p;
        private String item_img_url;

        public String getAd_title() {
            return ad_title;
        }

        public void setAd_title(String ad_title) {
            this.ad_title = ad_title;
        }

        public String getSub_title() {
            return sub_title;
        }

        public void setSub_title(String sub_title) {
            this.sub_title = sub_title;
        }

        public String getAd_image() {
            return ad_image;
        }

        public void setAd_image(String ad_image) {
            this.ad_image = ad_image;
        }

        public String getLink_url() {
            return link_url;
        }

        public void setLink_url(String link_url) {
            this.link_url = link_url;
        }

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getItem_code() {
            return item_code;
        }

        public void setItem_code(String item_code) {
            this.item_code = item_code;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getItem_p() {
            return item_p;
        }

        public void setItem_p(String item_p) {
            this.item_p = item_p;
        }

        public String getMarket_p() {
            return market_p;
        }

        public void setMarket_p(String market_p) {
            this.market_p = market_p;
        }

        public String getItem_img_url() {
            return item_img_url;
        }

        public void setItem_img_url(String item_img_url) {
            this.item_img_url = item_img_url;
        }
    }

    public static class ItemlevelBean {
        /**
         * level1 : A201
         * level2 : *
         * level3 : *
         * level_name : 패션의류
         * image_url : http://fileserver.gigawon.cn:8588/GigaShopMall/ca02.png
         * rank : 1
         */

        private String level1;
        private String level2;
        private String level3;
        private String level_name;
        private String image_url;
        private String rank;

        public String getLevel1() {
            return level1;
        }

        public void setLevel1(String level1) {
            this.level1 = level1;
        }

        public String getLevel2() {
            return level2;
        }

        public void setLevel2(String level2) {
            this.level2 = level2;
        }

        public String getLevel3() {
            return level3;
        }

        public void setLevel3(String level3) {
            this.level3 = level3;
        }

        public String getLevel_name() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }
    }

    public static class LeftBottomCate21Bean {
        /**
         * level1 : A201
         * level2 : B201
         * level_name : 여성의류
         */

        private String level1;
        private String level2;
        private String level_name;

        public String getLevel1() {
            return level1;
        }

        public void setLevel1(String level1) {
            this.level1 = level1;
        }

        public String getLevel2() {
            return level2;
        }

        public void setLevel2(String level2) {
            this.level2 = level2;
        }

        public String getLevel_name() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }
    }

    public static class LeftBottomCate22Bean {
        /**
         * level1 : A701
         * level2 : B701
         * level_name : 가구/DIY
         */

        private String level1;
        private String level2;
        private String level_name;

        public String getLevel1() {
            return level1;
        }

        public void setLevel1(String level1) {
            this.level1 = level1;
        }

        public String getLevel2() {
            return level2;
        }

        public void setLevel2(String level2) {
            this.level2 = level2;
        }

        public String getLevel_name() {
            return level_name;
        }

        public void setLevel_name(String level_name) {
            this.level_name = level_name;
        }
    }
}
