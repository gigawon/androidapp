package com.giga.shopping.http;

import android.content.Context;

import com.giga.library.util.EmptyUtils;
import com.giga.library.util.LogUtils;
import com.giga.library.widget.MessageView;
import com.giga.shopping.AppConstants;
import com.giga.shopping.MyApplication;
import com.giga.shopping.bean.DataBase;
import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.MediaType;

/**
 * Created by LaMitad on 2017/1/22.
 */

public class HttpHelper {

    private static HttpHelper instance;

    public static HttpHelper getInstance() {
        if (instance == null) {
            synchronized (HttpHelper.class) {
                if (instance == null) {
                    instance = new HttpHelper();
                }
            }
        }
        return instance;
    }

    public static class Create {

        private Context context;

        public Create(Context context) {
            this.context = context;
        }

        public Build setUrl(String url) {
            return new Build(context, url);
        }

        public static class Build {

            private Context context;
            private String url;

            private String service;
            private HashMap<String, String> params;
            private Object tag;
            private Object clazz;

            public Build(Context context, String base) {
                this.context = context;
                this.url = base;
                this.params = new HashMap<>();
            }

            public Build url(String url) {
                this.url = url;
                return this;
            }

            public Build service(String service) {
                this.service = service;
                return this;
            }

            public Build addParam(String key, String value) {
                this.params.put(key, value);
                return this;
            }

            public Build addParam(String key, int value) {
                this.params.put(key, String.valueOf(value));
                return this;
            }

            public Build setParam(Object value) {
                clazz = value;
                return this;
            }

            public Build tag(Object tag) {
                this.tag = tag;
                return this;
            }

            public void execute(Callback callback) {
                HttpHelper.getInstance().execute(this, callback);
            }

            public void execute(String content, Callback callback) {
                HttpHelper.getInstance().execute(this, callback, content);
            }

            public Context getContext() {
                return context;
            }

            public String getUrl() {
                return url;
            }

            public String getService() {
                return service;
            }

            public HashMap<String, String> getParams() {
                return params;
            }

            public Object getTag() {
                return tag;
            }

            public Object getClazz() {
                return clazz;
            }
        }
    }

    private void execute(HttpHelper.Create.Build build, Callback callback) {
        String params = "";
        if (build.getParams().size() > 0) {
            JSONObject jsonObject = new JSONObject();
            Iterator iter = build.getParams().entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                String key = (String) entry.getKey();
                String value = (String) entry.getValue();
                try {
                    jsonObject.put(key, value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
                params = jsonObject.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (EmptyUtils.isEmpty(params) && EmptyUtils.isNotEmpty(build.getClazz())) {
            params = new Gson().toJson(build.getClazz());
        }
        execute(build, callback, params);
    }

    private void execute(HttpHelper.Create.Build build, Callback callback, String params) {
        switch (MyApplication.Config.ENCRYPT) {
            case AppConstants.ENCRYPT_TYPE.NOCRYPT:
                nocrypt(build, callback, params);
                break;
            case AppConstants.ENCRYPT_TYPE.DES:
                des(build, callback, params);
                break;
            case AppConstants.ENCRYPT_TYPE.MD5:
                md5(build, callback, params);
                break;
        }

    }

    private void nocrypt(HttpHelper.Create.Build build, Callback callback, String params) {
        String url = build.getUrl()+build.getService();
        //发起请求
        OkHttpUtils.getInstance()
                .postString()
                .url(url)
                .content(params)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .tag(EmptyUtils.isEmpty(build.getTag()) ? build.getContext() : build.getTag())
                .build()
                .execute(callback);
    }

    private void des(HttpHelper.Create.Build build, Callback callback, String params) {

    }

    private void md5(HttpHelper.Create.Build build, Callback callback, String params) {

    }

    public static boolean processData(Object object, MessageView messageView) {
        if (EmptyUtils.isEmpty(object)) {
            messageView.showRequestFailure();
            return false;
        }
        try {
            String s = new Gson().toJson(object);
            DataBase dataBase = new Gson().fromJson(s, DataBase.class);
            if (dataBase.getStatus() == 1) {
                return true;
            } else {
                messageView.showWarningMessage(dataBase.getMsg());
                return false;
            }
        } catch (Exception e) {
            LogUtils.e(e.toString());
            messageView.showRequestFailure();
            return false;
        }
    }

    public static boolean processData(Object object) {
        if (EmptyUtils.isEmpty(object)) {
            return false;
        }
        try {
            String s = new Gson().toJson(object);
            DataBase dataBase = new Gson().fromJson(s, DataBase.class);
            if (dataBase.getStatus() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LogUtils.e(e.toString());
            return false;
        }
    }

}
