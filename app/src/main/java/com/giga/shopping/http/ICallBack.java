package com.giga.shopping.http;


import com.giga.library.http.IGenericsSerializator;
import com.giga.library.http.JsonGenericsSerializator;
import com.giga.library.util.EmptyUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.bean.DataBase;
import com.zhy.http.okhttp.callback.Callback;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;

import okhttp3.Response;

/**
 * Created by LaMitad on 2017/1/23.
 */

public abstract class ICallBack<T> extends Callback<T> {
    private IGenericsSerializator iGenericsSerializator;

    public ICallBack() {
        this.iGenericsSerializator = new JsonGenericsSerializator();
    }

    @Override
    public T parseNetworkResponse(Response response, int id) throws IOException {
        //原始数据
        String string = response.body().string();
        if (EmptyUtils.isEmpty(string)) {
            return null;
        }
        DataBase dataBase = iGenericsSerializator.transform(string, DataBase.class);
        if (EmptyUtils.isEmpty(dataBase)) {
            return null;
        }
       return nocrypt(string);
    }

    private T nocrypt(String string) {
        Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        if (entityClass == String.class) {
            return (T) string;
        }
        T bean = iGenericsSerializator.transform(string, entityClass);
        return bean;
    }

    private T des(String string) {
        Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        if (entityClass == String.class) {
            return (T) string;
        }
        T bean = iGenericsSerializator.transform(string, entityClass);
        return bean;
    }

    private T md5(String string) {
        Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        if (entityClass == String.class) {
            return (T) string;
        }
        T bean = iGenericsSerializator.transform(string, entityClass);
        return bean;
    }

}