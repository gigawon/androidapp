package com.giga.shopping.activity;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.C;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.L;
import com.giga.library.util.LogUtils;
import com.giga.library.util.SPUtils;
import com.giga.library.util.StringUtil;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.giga.shopping.utils.CustomerData.putSearchHistory;

public class SeachLevelActivity extends AppBaseActivity {

    @BindView(R.id.edit_text)
    EditText editText;
    @BindView(R.id.arrow_back)
    ImageView arrow_back;
    @BindView(R.id.text_delete)
    TextView textDelete;
    @BindView(R.id.flowView)
    com.giga.shopping.widget.FlowGroupView flowView;

    private List<String> mHistoryKeywords;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_sm_seach);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String query = editText.getText().toString();
                    save(query);
                    Intent intent = new Intent();
                    intent.putExtra("searchkey", editText.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                    return true;
                }

                return false;
            }
        });

    }


    @Override
    protected void initData() {
        super.initData();
        SPUtils.getInstance().getString(AppConstants.KEY.KEY_SP_HISTORYKEY);
        initSearchHistory();
    }


    /**
     * 动态添加布局
     *
     * @param str
     */
    private void addTextView(String str) {

        try {

            ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT);
            lp.setMargins(StringUtil.dip2px(getApplicationContext(), 10), StringUtil.dip2px(getApplicationContext(), 10), StringUtil.dip2px(getApplicationContext(), 10), StringUtil.dip2px(getApplicationContext(), 10));
            // 新的TextView对象
            TextView tv = new TextView(this);
            tv.setPadding(StringUtil.dip2px(getApplicationContext(), 10), StringUtil.dip2px(getApplicationContext(), 10), StringUtil.dip2px(getApplicationContext(), 10), StringUtil.dip2px(getApplicationContext(), 10));
            tv.setBackgroundResource(R.color.white);
            tv.setTextColor(getResources().getColor(R.color.inputblack));

            tv.setText(str);
            tv.setGravity(Gravity.CENTER);
            tv.setLines(1);
            initEvents(tv);

            flowView.addView(tv, lp);
            flowView.requestLayout();

        } catch (Exception e) {

            LogUtils.e(e);

        }

    }

    /**
     * 为每个view 添加点击事件
     *
     * @param tv
     */
    private void initEvents(final TextView tv) {

        tv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.putExtra("searchkey", tv.getText().toString());
                setResult(RESULT_OK, intent);
                finish();

            }
        });
    }

    public void initSearchHistory() {

        mHistoryKeywords = new ArrayList<String>();
        String history = SPUtils.getInstance().getString(AppConstants.KEY.KEY_SP_HISTORYKEY_level1);
        if (!TextUtils.isEmpty(history)) {
            List<String> list = new ArrayList<String>();
            for (Object o : history.split(",")) {
                list.add((String) o);
                addTextView(o.toString());
            }
            mHistoryKeywords = list;
        }
    }

    public void save(String text) {

        try {

            // String text = serchView.getText().toString();
            String oldText = SPUtils.getInstance().getString(AppConstants.KEY.KEY_SP_HISTORYKEY_level1);
            if (EmptyUtils.isNotEmpty(oldText)) {
                if (EmptyUtils.isNotEmpty(text) && !oldText.contains(text)) {
                    SPUtils.getInstance().put(AppConstants.KEY.KEY_SP_HISTORYKEY_level1, text + "," + oldText);
                    mHistoryKeywords.add(0, text);
                }
            } else {
                if (EmptyUtils.isNotEmpty(text)) {
                    SPUtils.getInstance().put(AppConstants.KEY.KEY_SP_HISTORYKEY_level1, text);
                    mHistoryKeywords.add(0, text);
                }
            }

            putSearchHistory(text);

        } catch (Exception e) {
            L.e(e);
        }

    }

    @OnClick({R.id.arrow_back, R.id.text_delete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.arrow_back:
                finish();
                break;
            case R.id.text_delete:
                flowView.removeAllViews();
                SPUtils.getInstance().put(AppConstants.KEY.KEY_SP_HISTORYKEY, "");
                break;
        }
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

}
