package com.giga.shopping.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

public class AddressEditActivity extends IBaseActivity {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.address_search_btn)
    TextView address_search_btn;
    @BindView(R.id.address_detail)
    EditText address_detail;
    @BindView(R.id.mobile)
    EditText mobile;
    @BindView(R.id.msg)
    EditText msg;
    @BindView(R.id.default_btn)
    LinearLayout default_btn;
    @BindView(R.id.default_img)
    ImageView default_img;

    private String addressName;
    private String addressZipName;
    private String addressZipDetail;
    private String addressMobile;
    private String addressMsg;
    private String seq_num;
    private String zip_code;
    private boolean isDefault = false;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_address_edit);
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        super.initData();

        Intent getIntent = getIntent();

        addressName         = getIntent.getStringExtra("addressName");
        addressZipName      = getIntent.getStringExtra("addressZipName");
        addressZipDetail    = getIntent.getStringExtra("addressZipDetail");
        addressMobile       = getIntent.getStringExtra("addressMobile");
        addressMsg          = getIntent.getStringExtra("addressMsg");
        seq_num             = getIntent.getStringExtra("seq_num");
        zip_code            = getIntent.getStringExtra("zip_code");
        isDefault           = getIntent.getBooleanExtra("default_add", false);

        if (addressName != null && !addressName.equals(""))
            name.setText(addressName);
        if (addressZipName != null && !addressZipName.equals("")) {
            address_search_btn.setText(addressZipName);
            address_detail.setVisibility(View.VISIBLE);
            address_detail.setText("");
        } else {
            address_detail.setVisibility(View.GONE);
        }
        if (addressZipDetail != null && !addressZipDetail.equals(""))
            address_detail.setText(addressZipDetail);
        if (addressMobile != null && !addressMobile.equals(""))
            mobile.setText(addressMobile);
        if (addressMsg != null && !addressMsg.equals(""))
            msg.setText(addressMsg);
        if (isDefault == true)
            default_img.setImageResource(R.mipmap.icon_cart_selected);
        else
            default_img.setImageResource(R.mipmap.icon_cart_unselected);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1000) {
                addressZipName = data.getStringExtra("zipName");
                zip_code = data.getStringExtra("zipCode");
                if (addressZipName != null && !addressZipName.equals("")) {
                    address_search_btn.setText(addressZipName);
                    address_detail.setVisibility(View.VISIBLE);
                    address_detail.setText("");
                } else {
                    address_detail.setVisibility(View.GONE);
                }
            }
        }
    }

    @OnClick({R.id.back_btn, R.id.address_search_btn, R.id.save_btn, R.id.default_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.address_search_btn:
                startActivityForResult(new Intent(AddressEditActivity.this, AddressSearchActivity.class), 1000);
                break;
            case R.id.save_btn:
                save();
                break;
            case R.id.default_btn:
                isDefault = !isDefault;
                if (isDefault == true)
                    default_img.setImageResource(R.mipmap.icon_cart_selected);
                else
                    default_img.setImageResource(R.mipmap.icon_cart_unselected);
                break;

        }
    }

    public void save() {

        try {

            addressName         = name.getText().toString();
            addressZipName      = address_search_btn.getText().toString();
            addressZipDetail    = address_detail.getText().toString();
            addressMobile       = mobile.getText().toString();
            addressMsg          = msg.getText().toString();

            if (addressName == null || addressName.equals("")
                || addressZipName == null || addressZipName.equals("")
                || addressZipDetail == null || addressZipDetail.equals("")
                || addressMobile == null || addressMobile.equals("")) {
                Toast.makeText(AddressEditActivity.this, "주소정보를 입력하세요!", Toast.LENGTH_SHORT).show();
                return;
            }

            String service =(seq_num != null && !seq_num.equals(""))? AppConstants.SERVICE.REQUEST_ADDRESS_EDIT:AppConstants.SERVICE.REQUEST_ADDRESS_ADD;

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.MALL_BASEURL)
                    .service(service)
                    .addParam("lang_type", "kor")
                    .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                    .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                    .addParam("delivery_name", addressName)
                    .addParam("to_address", addressZipDetail)
                    .addParam("zip_code", zip_code)
                    .addParam("zip_name", addressZipName)
                    .addParam("mobilepho", addressMobile)
                    .addParam("seq_num", seq_num)
                    .addParam("remark", addressMsg)
                    .addParam("default_add", isDefault == true? "1":"0")
                    .execute(new ICallBack<Object>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(Object response, int id) {

                            try {
                                if (response == null) {
                                    return;
                                }
                                JSONObject result = new JSONObject(response.toString());
                                Toast.makeText(AddressEditActivity.this, result.getString("msg"), Toast.LENGTH_SHORT).show();
                                finish();
                            } catch (Exception e) {
                                L.e(e);
                            }


                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }
}
