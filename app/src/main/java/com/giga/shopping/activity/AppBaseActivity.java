package com.giga.shopping.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.giga.library.base.IBaseActivity;
import com.giga.shopping.AppConstants;

public abstract class AppBaseActivity extends IBaseActivity {

    private BroadcastReceiver mReceiver = null;

    @Override
    protected void initialize(Bundle savedInstanceState) {
    }
    @Override
    protected void onAfter() {
        super.onAfter();
    }

    @Override
    protected boolean onPressBack() {
        return super.onPressBack();
    }

    public abstract void onBriadcastReceive(Intent intent);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    private void registerReceiver(){

        if(mReceiver != null) return;

        final IntentFilter theFilter = new IntentFilter();
        theFilter.addAction(AppConstants.INTENT_FILTER.INTENT_FINISH);

        this.mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onBriadcastReceive(intent);
            }
        };

        this.registerReceiver(this.mReceiver, theFilter);

    }

    /** 동적으로(코드상으로) 브로드 캐스트를 종료한다. **/
    private void unregisterReceiver() {
        if(mReceiver != null){
            this.unregisterReceiver(mReceiver);
            mReceiver = null;
        }

    }

}
