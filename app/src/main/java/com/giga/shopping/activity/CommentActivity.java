package com.giga.shopping.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PathMeasure;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.library.base.IBaseActivity;
import com.giga.library.util.L;
import com.giga.library.util.LogUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.CommentAdapter;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ExpandBean;
import com.giga.shopping.http.GlideImageLoader;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.SonicRuntimeImpl;
import com.giga.shopping.utils.GlideSimpleTarget;
import com.giga.shopping.utils.NineUtils;
import com.giga.shopping.widget.IdeaScrollView;
import com.giga.shopping.widget.NewBanner;
import com.tencent.sonic.sdk.SonicConfig;
import com.tencent.sonic.sdk.SonicEngine;
import com.tencent.sonic.sdk.SonicSession;
import com.youth.banner.BannerConfig;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ch.ielse.view.imagewatcher.ImageWatcher;
import okhttp3.Call;
import okhttp3.Request;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import qiu.niorgai.StatusBarCompat;

public class CommentActivity extends AppBaseActivity implements ImageWatcher.OnPictureLongPressListener, ImageWatcher.Loader {

    @BindView(R.id.title_text_view)
    TextView titleTextView;
    @BindView(R.id.list_view)
    RecyclerView listView;

    private CommentAdapter commentAdapter;
    private ImageWatcher mImageWatcher;

    private String itemCode;
    private String supplyCode;

    @Override
    protected void onBefore() {
        super.onBefore();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        if (!SonicEngine.isGetInstanceAllowed()) {
            SonicEngine.createInstance(new SonicRuntimeImpl(getApplication()), new SonicConfig.Builder().build());
        }
    }

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_comment);
        StatusBarCompat.translucentStatusBar(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();

        mImageWatcher = (ImageWatcher) findViewById(R.id.image_watcher);
        mImageWatcher.setTranslucentStatus(NineUtils.calcStatusBarHeight(this));
        mImageWatcher.setErrorImageRes(R.mipmap.error_picture);
        mImageWatcher.setOnPictureLongPressListener(this);
        mImageWatcher.setLoader(this);
        listView.setHasFixedSize(true);
        listView.setNestedScrollingEnabled(false);
        listView.setLayoutManager(new LinearLayoutManager(this));

        commentAdapter = new CommentAdapter(this, mImageWatcher);
        listView.setAdapter(commentAdapter);

    }

    @Override
    protected void initData() {
        super.initData();

        itemCode = getIntent().getStringExtra("itemCode");
        supplyCode = getIntent().getStringExtra("supplyCode");

        asyncMakeData();

    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    private void asyncMakeData() {

        List<ExpandBean> friendCircleBeans = new ArrayList<>();
        List<String> urls = new ArrayList<>();

        urls.add("http://pic.qiantucdn.com/58pic/22/06/55/57b2d98e109c6_1024.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201204/20120412123916285.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201210/20121006203300515.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201204/20120412123916285.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201210/20121006203300515.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201204/20120412123916285.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201210/20121006203300515.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201204/20120412123916285.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201210/20121006203300515.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201204/20120412123916285.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201210/20121006203300515.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201204/20120412123916285.jpg");
        urls.add("http://www.zhlzw.com/UploadFiles/Article_UploadFiles/201210/20121006203300515.jpg");
        ExpandBean bean = new ExpandBean();
        bean.setExpanded(true);
        bean.setImageUrls(urls);
        friendCircleBeans.add(bean);
        friendCircleBeans.add(bean);
        friendCircleBeans.add(bean);
        friendCircleBeans.add(bean);
        friendCircleBeans.add(bean);
        friendCircleBeans.add(bean);

        if (friendCircleBeans != null) {
            commentAdapter.setFriendCircleBeans(friendCircleBeans);
        }
    }

    @Override
    public void load(Context context, String url, ImageWatcher.LoadCallback lc) {
        Glide.with(context).asBitmap().load(url).into(new GlideSimpleTarget(lc));
    }

    @Override
    public void onPictureLongPress(ImageView v, String url, int pos) {

    }


}
