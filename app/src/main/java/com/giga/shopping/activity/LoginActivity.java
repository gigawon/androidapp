package com.giga.shopping.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.DeviceUtil;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.EncryptUtils;
import com.giga.library.util.GsonUtils;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.library.widget.MessageView;
import com.giga.shopping.AppConstants;
import com.giga.shopping.MyApplication;
import com.giga.shopping.R;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.LoginEntity;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.http.OkHttpHelper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

import static com.giga.shopping.AppConstants.URL.REGIST_URL;

public class LoginActivity extends AppBaseActivity implements TextView.OnEditorActionListener {


    @BindView(R.id.numberEdit)
    EditText numberEdit;
    @BindView(R.id.numberInputLayout)
    TextInputLayout numberInputLayout;
    @BindView(R.id.passwordEdit)
    EditText passwordEdit;
    @BindView(R.id.passwordInputLayout)
    TextInputLayout passwordInputLayout;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.codeLoginButton)
    TextView codeLoginButton;
    @BindView(R.id.forgetPasswordButton)
    TextView forgetPasswordButton;
    @BindView(R.id.messageView)
    MessageView messageView;
    @BindView(R.id.arrow_back)
    ImageView arrowBack;
    @BindView(R.id.registerButton)
    Button registerButton;

    private String message;
    private static final int REGISTER = 1;
    private static final int FORGET = 2;

    @Override
    protected void onBefore() {
        super.onBefore();
        message = getIntent().getStringExtra(AppConstants.KEY.MESSAGE);
    }

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();
        if (EmptyUtils.isNotEmpty(SPUtils.getInstance().getString(AppConstants.KEY.USERNAME))) {
            numberEdit.setText(SPUtils.getInstance().getString(AppConstants.KEY.USERNAME));
            requestFocus(passwordEdit);
        } else {
            requestFocus(numberEdit);
        }
        numberEdit.setOnEditorActionListener(this);
        passwordEdit.setOnEditorActionListener(this);
    }

    @Override
    protected void onAfter() {
        super.onAfter();
        if (!EmptyUtils.isEmpty(message)) {
            messageView.showMessage(message);
        }
    }

    @OnClick({R.id.loginButton, R.id.forgetPasswordButton, R.id.registerButton, R.id.arrow_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
//                doLogin();
                doLogin();
                break;
            case R.id.forgetPasswordButton:
//                startActivityForResult(new Intent(context, ForgetPasswordActivity.class), FORGET);
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            case R.id.registerButton:
                Intent intent = new Intent(LoginActivity.this, WebActivity.class);
                intent.putExtra("title", getString(R.string.register_login));
                intent.putExtra("url", REGIST_URL);
                startActivity(intent);
                break;
            case R.id.arrow_back:
                finish();
                break;
        }
    }

    private boolean checkInput () {
        String id = numberEdit.getText().toString().trim();
        if (id.length() < 3) {
            numberInputLayout.setError(getString(R.string.check_id));
            requestFocus(numberEdit);
            return false;
        }

        String password = passwordEdit.getText().toString().trim();
        if (password.length() < 3) {
            passwordInputLayout.setError(getString(R.string.check_password));
            requestFocus(passwordEdit);
            return false;
        } else {
            passwordInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    public void doLogin() {

        try {

            if (!checkInput()) {
                return;
            }
            final String phoneNumber = numberEdit.getText().toString().trim();
            final String password = passwordEdit.getText().toString().trim();

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_LOGIN)
                    .addParam("myData", "{lang_type:'kor',ver:'1',memid:'" + phoneNumber + "',mempwd:'" + EncryptUtils.SHA512(password) + "',s_id:'t1b1dwteigkxgwzkw3yxjzyb',deviceNo:'" + DeviceUtil.getDeviceId(LoginActivity.this) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));

                                String status = main.getString("status");

                                if (status != null && status.equals("1")) {

                                    SPUtils.getInstance().put(AppConstants.KEY.APP_DATA, main.toString());
                                    SPUtils.getInstance().put(AppConstants.KEY.LOGIN_STATUS, main.getString("status"));
                                    SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_CODE, main.getString("custom_code"));
                                    SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_ID, main.getString("custom_id"));
                                    SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_NAME, main.getString("custom_name"));
                                    SPUtils.getInstance().put(AppConstants.KEY.NICK_NAME, main.getString("nick_name"));
                                    SPUtils.getInstance().put(AppConstants.KEY.PROFILE_IMG, main.getString("profile_img"));
                                    SPUtils.getInstance().put(AppConstants.KEY.TOKEN, main.getString("token"));
                                    SPUtils.getInstance().put(AppConstants.KEY.DIV_CODE, main.getString("div_code"));
                                    SPUtils.getInstance().put(AppConstants.KEY.SSOID, main.getString("ssoId"));
                                    SPUtils.getInstance().put(AppConstants.KEY.SSO_REGIKEY, main.getString("sso_regiKey"));
                                    SPUtils.getInstance().put(AppConstants.KEY.MALL_HOME_ID, "gigakorea");//main.getString("mall_home_id"));
                                    SPUtils.getInstance().put(AppConstants.KEY.POINT_CART_NO, main.getString("point_card_no"));
                                    SPUtils.getInstance().put(AppConstants.KEY.PARENT_ID, main.getString("parent_id"));
                                    SPUtils.getInstance().put(AppConstants.KEY.BUSINESS_TYPE, main.getString("business_type"));
                                    SPUtils.getInstance().put(AppConstants.KEY.MINFO, main.getString("minfo"));

                                    updateToken(main.getString("custom_code"), SPUtils.getInstance().getString(AppConstants.KEY.PUSH_TOKEN));

                                }

                            } catch (Exception e) {
                                L.e(e);
                                SPUtils.getInstance().put(AppConstants.KEY.LOGIN_STATUS, "0");
                            }

                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }


    /***
     * MENU
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.register:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REGISTER:
                    if (EmptyUtils.isNotEmpty(SPUtils.getInstance().getString(AppConstants.KEY.USERNAME))) {
                        numberEdit.setText(SPUtils.getInstance().getString(AppConstants.KEY.USERNAME));
                        requestFocus(passwordEdit);
                    } else {
                        requestFocus(numberEdit);
                    }
                    messageView.showMessage("注册成功请登录");
                    break;
                case FORGET:
                    messageView.showMessage("密码修改成功请重新登录");
                    break;
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (i) {
            case EditorInfo.IME_ACTION_NEXT:
                requestFocus(passwordEdit);
                return true;
            case EditorInfo.IME_ACTION_GO:
                if (checkInput() && EmptyUtils.isNotEmpty(numberEdit.getText().toString())) {
//                  doLogin();
                }
                return true;
        }
        return false;
    }
    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    public void checkActivityResult() {
        try {

            //웹 인터페이스로 호출시
            String from = getIntent().getStringExtra("from");
            if (from != null && from.equals("web")) {
                String scriptName = getIntent().getStringExtra("scriptName");
                String values = getIntent().getStringExtra("values");
                Intent returnIntent = new Intent();
                returnIntent.putExtra("scriptName", scriptName);
                returnIntent.putExtra("values", values);
                setResult(RESULT_OK, returnIntent);
            } else if(from != null && from.equals("main")) {
                int tab = getIntent().getIntExtra("tab", 0);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("tab", tab);
                setResult(RESULT_OK, returnIntent);
            }

            finish();

        } catch (Exception e) {
            L.e(e);
        }
    }

    public void updateToken(String custom_code, String push_token) {

        L.d("updateToken : " + push_token + " / " + custom_code);

        if (custom_code != null && !custom_code.equals("") && push_token != null && !push_token.equals("")) {

            String deviceId = custom_code;

            if (ContextCompat.checkSelfPermission( this, Manifest.permission.READ_PHONE_STATE ) == PackageManager.PERMISSION_GRANTED ) {
                TelephonyManager telephonyManager;
                telephonyManager = (TelephonyManager) getSystemService(Context.
                        TELEPHONY_SERVICE);
                deviceId = telephonyManager.getDeviceId();
            }

            HashMap<String, String> params = new HashMap<>();
            params.put("custom_code", custom_code);
            params.put("appl_gubun", "11");
            params.put("device_id", deviceId);
            params.put("token", push_token);
            params.put("os_gubun", "aos");

            OkHttpHelper okHttpHelper = new OkHttpHelper(this);
            okHttpHelper.addPostRequest(new OkHttpHelper.CallbackLogic() {
                @Override
                public void onBizSuccess(String responseDescription, JSONObject data, String flag) {
                    checkActivityResult();
                }

                @Override
                public void onBizFailure(String responseDescription, JSONObject data, String flag) {
                    Toast.makeText(LoginActivity.this, "토큰이 업데이트 되지 않았습니다.(F)", Toast.LENGTH_SHORT).show();
                    checkActivityResult();
                }

                @Override
                public void onNetworkError(Request request, IOException e) {
                    Toast.makeText(LoginActivity.this, "토큰이 업데이트 되지 않았습니다.(N)", Toast.LENGTH_SHORT).show();
                    checkActivityResult();
                }
            }, "http://samanager.gigawon.co.kr:8824/api/PushInfo/requestPushInfo", GsonUtils.createGsonString(params));
        } else {
            Toast.makeText(LoginActivity.this, "토큰이 업데이트 되지 않았습니다.(W)", Toast.LENGTH_SHORT).show();
            checkActivityResult();
        }
    }

}
