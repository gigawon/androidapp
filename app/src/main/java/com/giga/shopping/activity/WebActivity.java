package com.giga.shopping.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.L;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.SearchAdapter;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ProductDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.EndlessRecyclerOnScrollListener;
import com.giga.shopping.widget.LoadMoreWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

import static com.giga.shopping.AppConstants.WEB.CATEGORY_NO;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_CATEGORY;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_PRODUCT_DETAIL;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_SEARCH;
import static com.giga.shopping.AppConstants.WEB.HOST_FINISH;
import static com.giga.shopping.AppConstants.WEB.PRODUCT_NO;
import static com.giga.shopping.AppConstants.WEB.SCHEME;
import static com.giga.shopping.AppConstants.WEB.SUPPLY_CODE;


public class WebActivity extends AppBaseActivity {

    @BindView(R.id.arrow_back)
    RelativeLayout arrowBack;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.web_view)
    WebView webView;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();

        try {

            arrowBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            //webView.clearCache(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.getSettings().setDisplayZoomControls(false);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webView.getSettings().setSupportMultipleWindows(true);
            webView.setEnabled(false);

            // 이전 버전에서 적용되지 않는다
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
                webView.getSettings().setMixedContentMode(
                        WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            webView.setWebChromeClient(new WebChromeClient() {

            });

            webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    // TODO Auto-generated method stub

                    try {

                        L.d("shouldOverrideUrlLoading : " + url);

                        Uri uri = Uri.parse(url);
                        String scheme = uri.getScheme();
                        String host = uri.getHost();

                        L.d("shouldOverrideUrlLoading scheme : " + scheme);
                        L.d("shouldOverrideUrlLoading host : " + host);

                        if (scheme.equals(SCHEME)) {

                            if (host.equals(HOST_CALL_PRODUCT_DETAIL)) {

                                //giga://showProductDetail?supplyCode=0&productNo=0

                                String supplyCode = uri.getQueryParameter(SUPPLY_CODE);
                                String productNo = uri.getQueryParameter(PRODUCT_NO);


                            } else if (host.equals(HOST_CALL_CATEGORY)) {

                                //giga://showCategory?categoryNo=0

                                String categoryNo = uri.getQueryParameter(CATEGORY_NO);

                                Intent intent = new Intent();
                                intent.setClass(WebActivity.this,SearchGoodsActivity.class);
                                intent.putExtra("itemCode", "A201");
                                startActivity(intent);

                            } else if (host.equals(HOST_CALL_SEARCH)) {

                                //giga://showSearch
                                Intent intent = new Intent();
                                intent.setClass(WebActivity.this,SearchGoodsActivity.class);
                                intent.putExtra("itemCode", "search");
                                startActivity(intent);

                            } else if (host.equals(HOST_FINISH)) {

                                //giga://close
                                finish();

                            }
                            return true;
                        }
                    } catch (Exception e) {
                        L.e(e);
                        return true;
                    }
                    return super.shouldOverrideUrlLoading(view, url);
                }

                @Override
                public void onPageStarted(final WebView view, String url, Bitmap favicon) {
                    // TODO Auto-generated method stub
                    showProgressDialog();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    // TODO Auto-generated method stub
                    hideProgressDialog();
                }

                @Override
                public void onReceivedError(final WebView view, int errorCode, String description, String failingUrl) {
                    super.onReceivedError(view, errorCode, description, failingUrl);
                }

                @Override
                public void onReceivedSslError(final WebView view, SslErrorHandler handler, SslError error) {
                    super.onReceivedSslError(view, handler, error);
                }

            });

        } catch (Exception e) {
            L.e(e);
        }
    }

    @Override
    protected void initData() {
        super.initData();
        title.setText(getIntent().getStringExtra("title"));
        webView.loadUrl(getIntent().getStringExtra("url"));
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

}
