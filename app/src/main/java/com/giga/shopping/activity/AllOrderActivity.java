package com.giga.shopping.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.EndlessRecyclerOnScrollListener;
import com.giga.shopping.widget.LoadMoreWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Request;

import static com.giga.shopping.AppConstants.SERVICE.REQUEST_CANCEL_ORDER;
import static com.giga.shopping.AppConstants.SERVICE.REQUEST_RETURN_ORDER;

public class AllOrderActivity extends AppBaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private LoadMoreWrapper loadMoreWrapper;
    private JSONArray listItems = new JSONArray();
    private OrderListAdapter loadMoreWrapperAdapter;
    private int pg          = 1;
    private int tot_page    = 1;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_all_order);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();
        setSwipeColor(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        loadMoreWrapperAdapter = new OrderListAdapter(this);
        loadMoreWrapper = new LoadMoreWrapper(loadMoreWrapperAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(loadMoreWrapper);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING);

                if (pg < tot_page) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadList();
                                }
                            });
                        }
                    }, 1000);
                } else {
                    loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_END);
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();

        try {

            pg = 1;
            listItems = new JSONArray();
            loadList();

        } catch (Exception e) {
            L.e(e);
        }
    }

    private void addData(JSONArray list) {

        for (int i = 0; i < list.length(); i++) {
            try {
                JSONObject item = list.getJSONObject(i);
                listItems.put(item);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    private void loadList() {

        try {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.MYSHOP_URL)
                    .service(AppConstants.SERVICE.REQUEST_GET_ORDER_LIST)
                    .addParam("myData",  "{" +
                            "custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "'," +
                            "progress_status:''," +
                            "s_option:'1'," +
                            "searchKeyword:''," +
                            "lang_type:'kor'," +
                            "end_date:''," +
                            "start_date:''," +
                            "token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'," +
                            "pg:'" + pg + "'," +
                            "pageSize:'10'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_COMPLETE);
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                JSONArray list = main.getJSONArray("data");
                                pg = pg + 1;
                                tot_page = Integer.parseInt(main.getString("tot_page"));
                                addData(list);
                                loadMoreWrapperAdapter.notifyDataSetChanged();
                                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_COMPLETE);

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    public void onRefresh() {
        initData();
        stopRefreshing();
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder> {
        private DecimalFormat decimalFormat = new DecimalFormat("#,###");
        private Context mContext;

        public OrderListAdapter(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        public AllOrderActivity.OrderListAdapter.OrderListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_all_order, viewGroup, false);
            OrderListViewHolder viewHolder = new OrderListViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull AllOrderActivity.OrderListAdapter.OrderListViewHolder viewHolder, int position) {

            try {

                final JSONObject item = listItems.getJSONObject(position);
                String order_status = item.getString("order_status");

                viewHolder.title.setText(item.getString("item_name"));
                viewHolder.order_num.setText(getString(R.string.order_num) + " : " + item.getString("order_num"));
                if (order_status.equals("6")) { //반품 취소 완료
                    viewHolder.price.setText("취소금액 " + decimalFormat.format(Double.parseDouble(item.getString("payment_o"))) + "원");
                    viewHolder.count.setText("");
                } else if (order_status.equals("53")) { //반품 취소 완료
                    viewHolder.price.setText("환불금액 " + decimalFormat.format(Double.parseDouble(item.getString("payment_o"))) + "원");
                    viewHolder.count.setText("");
                } else {
                    viewHolder.price.setText("결제금액 " + decimalFormat.format(Double.parseDouble(item.getString("payment_o"))) + "원 "
                            + "(배송비 " + decimalFormat.format(Double.parseDouble(item.getString("delivery_o"))) + "원, ");
                    viewHolder.count.setText("수량 " + item.getString("order_q") + "개)");
                }
                viewHolder.state.setText(item.getString("order_status_name"));
                viewHolder.date.setText(getString(R.string.order_date) + " : " + item.getString("order_date"));

                Glide.with(AllOrderActivity.this).load(item.getString("item_image")).into(viewHolder.image);

                if (order_status.equals("1")) { //주문완료
                    viewHolder.btn_area.setVisibility(View.VISIBLE);
                    viewHolder.comment_btn.setVisibility(View.GONE);
                    viewHolder.return_btn.setVisibility(View.GONE);
                    viewHolder.cancel_btn.setVisibility(View.VISIBLE);
                    viewHolder.delivery_btn.setVisibility(View.GONE);
                    viewHolder.confirm_btn.setVisibility(View.GONE);
                } else if (order_status.equals("2")) { //배송중
                    viewHolder.btn_area.setVisibility(View.VISIBLE);
                    viewHolder.comment_btn.setVisibility(View.GONE);
                    viewHolder.return_btn.setVisibility(View.VISIBLE);
                    viewHolder.cancel_btn.setVisibility(View.GONE);
                    viewHolder.delivery_btn.setVisibility(View.VISIBLE);
                    viewHolder.confirm_btn.setVisibility(View.GONE);
                } else if (order_status.equals("3")) { //배송완료
                    viewHolder.btn_area.setVisibility(View.VISIBLE);
                    viewHolder.return_btn.setVisibility(View.VISIBLE);
                    viewHolder.cancel_btn.setVisibility(View.GONE);
                    viewHolder.delivery_btn.setVisibility(View.GONE);
                    viewHolder.comment_btn.setVisibility(View.VISIBLE);
                    viewHolder.confirm_btn.setVisibility(View.VISIBLE);
                    if (item.getString("assess_status").equals("0")) {
                        viewHolder.comment_btn.setText("상품평쓰기");
                    } else {
                        viewHolder.comment_btn.setText("상품평보기");
                    }
                } else if (order_status.equals("9")) { //구매확정
                    viewHolder.btn_area.setVisibility(View.VISIBLE);
                    viewHolder.return_btn.setVisibility(View.GONE);
                    viewHolder.cancel_btn.setVisibility(View.GONE);
                    viewHolder.delivery_btn.setVisibility(View.GONE);
                    viewHolder.comment_btn.setVisibility(View.VISIBLE);
                    viewHolder.confirm_btn.setVisibility(View.GONE);
                    if (item.getString("assess_status").equals("0")) {
                        viewHolder.comment_btn.setText("상품평쓰기");
                    } else {
                        viewHolder.comment_btn.setText("상품평보기");
                    }
                } else {
                    viewHolder.btn_area.setVisibility(View.GONE);
                    viewHolder.comment_btn.setVisibility(View.GONE);
                    viewHolder.return_btn.setVisibility(View.GONE);
                    viewHolder.cancel_btn.setVisibility(View.GONE);
                    viewHolder.delivery_btn.setVisibility(View.GONE);
                    viewHolder.confirm_btn.setVisibility(View.GONE);
                }

                //상품상세
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            Intent intent = new Intent();
                            intent.setClass(AllOrderActivity.this, GoodsDetailActivity.class);
                            intent.putExtra("itemCode", item.getString("item_code"));
                            intent.putExtra("supplyCode", item.getString("supplier_custom_code"));
                            startActivity(intent);
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

                //장바구니
                viewHolder.cart_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            new HttpHelper.Create(AllOrderActivity.this)
                                    .setUrl(AppConstants.URL.WWW_BASEURL)
                                    .service(AppConstants.SERVICE.REQUEST_ADD_CART_MALL_ORDER)
                                    .addParam("myData", "{" +
                                            "lang_type:'kor'," +
                                            "div_code:'2'," +
                                            "item_p:'" + item.getString("order_o") + "'," +
                                            "token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'," +
                                            "item_code:'" + item.getString("item_code").replaceAll(" ", "") + "'," +
                                            "supply_code:'" + item.getString("supplier_custom_code") + "'," +
                                            "custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "'," +
                                            "SALE_Q:'1'," +
                                            "delivery_p:'" + item.getString("delivery_o") + "'," +
                                            "mall_custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.MALL_HOME_ID) + "'}")
                                    .execute(new ICallBack<DataDetail>() {

                                        @Override
                                        public void onBefore(Request request, int id) {
                                            super.onBefore(request, id);
                                            showProgressDialog();
                                        }

                                        @Override
                                        public void onAfter(int id) {
                                            super.onAfter(id);
                                            hideProgressDialog();
                                        }

                                        @Override
                                        public void onError(Call call, Exception e, int id) {
                                            ToastUtils.showShortToast(e.getMessage());
                                        }

                                        @Override
                                        public void onResponse(DataDetail response, int id) {
                                            if (response == null) {
                                                return;
                                            }

                                            try {
                                                JSONObject main = new JSONObject(response.getD());
                                                ToastUtils.showShortToast(main.getString("msg"));
                                            } catch (Exception e) {
                                                L.e(e);
                                            }
                                        }
                                    });

                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

                //배송조회
                viewHolder.delivery_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            String dvry_num = item.getString("dvry_num");
                            if (dvry_num != null || !dvry_num.equals("")) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getString("dvry_search_url") + dvry_num));
                                startActivity(intent);
                            } else {
                                Toast.makeText(AllOrderActivity.this, "배송 준비중입니다.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

                //구매확정
                viewHolder.confirm_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AllOrderActivity.this);
                            builder.setTitle("구매확정");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage("물건을 잘 수령하셨습니까?\n구매확정하시겠습니까?");
                            builder.setPositiveButton("구매확정",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            try {

                                                dialog.dismiss();

                                                new HttpHelper.Create(AllOrderActivity.this)
                                                        .setUrl(AppConstants.URL.MALL_BASEURL)
                                                        .service(AppConstants.SERVICE.REQUEST_CONFIRM_ORDER)
                                                        .addParam("lang_type", "kor")
                                                        .addParam("div_code", "2")
                                                        .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                                                        .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                                                        .addParam("order_num", item.getString("order_num"))
                                                        .addParam("ser_no", item.getString("ser_no"))
                                                        .addParam("item_code", item.getString("item_code"))
                                                        .addParam("supply_custom_code", item.getString("supplier_custom_code"))
                                                        .addParam("mall_custom_code", item.getString("mall_custom_code"))
                                                        .execute(new ICallBack<Object>() {

                                                            @Override
                                                            public void onBefore(Request request, int id) {
                                                                super.onBefore(request, id);
                                                                showProgressDialog();
                                                            }

                                                            @Override
                                                            public void onAfter(int id) {
                                                                super.onAfter(id);
                                                                hideProgressDialog();
                                                            }

                                                            @Override
                                                            public void onError(Call call, Exception e, int id) {
                                                                ToastUtils.showShortToast(e.getMessage());
                                                            }

                                                            @Override
                                                            public void onResponse(Object response, int id) {

                                                                try {
                                                                    if (response == null) {
                                                                        return;
                                                                    }
                                                                    JSONObject result = new JSONObject(response.toString());
                                                                    Toast.makeText(AllOrderActivity.this, result.getString("msg"), Toast.LENGTH_SHORT).show();
                                                                    initData();
                                                                } catch (Exception e) {
                                                                    L.e(e);
                                                                }


                                                            }
                                                        });

                                            } catch (Exception e) {
                                                L.e(e);

                                            }
                                        }
                                    });
                            builder.setNegativeButton("취소",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            builder.show();
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

                //반품
                viewHolder.return_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AllOrderActivity.this);
                            builder.setTitle("반품신청");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage("반품신청하시겠습니까?");
                            builder.setPositiveButton("반품신청",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();
                                            try {

                                                Intent i = new Intent(AllOrderActivity.this, ReturnActivity.class);
                                                i.putExtra("order_num", item.getString("order_num"));
                                                startActivityForResult(i, 1002);

                                            } catch (Exception e) {
                                                L.e(e);

                                            }
                                        }
                                    });
                            builder.setNegativeButton("취소",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            builder.show();
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

                //취소
                viewHolder.cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            AlertDialog.Builder builder = new AlertDialog.Builder(AllOrderActivity.this);
                            builder.setTitle("주문취소");
                            builder.setIcon(R.mipmap.ic_launcher);
                            builder.setMessage("주문취소하시겠습니까?");
                            builder.setPositiveButton("주문취소",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            try {

                                                dialog.dismiss();
                                                String auth_no = item.getString("auth_no");
                                                if (auth_no == null || auth_no.equals("")) {
                                                    cancelOrder(item.getString("order_num"));
                                                    return;
                                                }
                                                String customCode = SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE);
                                                Intent intent = new Intent(AllOrderActivity.this, LGUPlusPayActivity.class);
                                                intent.putExtra("url", "http://211.43.220.140:8080/LgMPay/Cancel.jsp");
                                                intent.putExtra("title", "주문취소");
                                                intent.putExtra("position", position);
                                                intent.putExtra("order_num", item.getString("order_num"));
                                                String postdata = "CST_PLATFORM=" + AppConstants.PAYMENT.LG_PAY + "&LGD_TID=" + item.getString("auth_no") + "&CUSTOM_CODE=" + customCode + "&LGD_OID=" + item.getString("order_num");
                                                intent.putExtra("postData", postdata);
                                                startActivityForResult(intent, 1001);

                                            } catch (Exception e) {
                                                L.e(e);

                                            }
                                        }
                                    });
                            builder.setNegativeButton("취소",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            builder.show();

                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

                //평가하기
                viewHolder.comment_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            Intent i = new Intent(AllOrderActivity.this, RateActivity.class);
                            i.putExtra("isCreate", item.getString("assess_status").equals("0"));
                            i.putExtra("item_image", item.getString("item_image"));
                            i.putExtra("item_name", item.getString("item_name"));
                            i.putExtra("order_num", item.getString("order_num"));
                            i.putExtra("sale_custom_code", item.getString("supplier_custom_code"));
                            i.putExtra("item_code", item.getString("item_code").replaceAll(" ", ""));
                            i.putExtra("position", position);
                            startActivityForResult(i, 1000);
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

            } catch (Exception e) {
                L.e(e);
            }

        }

        @Override
        public int getItemCount() {
            return listItems.length();
        }

        public class OrderListViewHolder extends RecyclerView.ViewHolder {

            private LinearLayout btn_area;

            private TextView title, order_num, date, price, state, count, comment_btn, return_btn, cancel_btn, delivery_btn, cart_btn, confirm_btn;

            private ImageView image;

            public OrderListViewHolder(View itemView) {
                super(itemView);

                title = itemView.findViewById(R.id.title);
                order_num = itemView.findViewById(R.id.order_num);
                date = itemView.findViewById(R.id.date);
                price = itemView.findViewById(R.id.price);
                state = itemView.findViewById(R.id.state);
                count = itemView.findViewById(R.id.count);
                confirm_btn = itemView.findViewById(R.id.confirm_btn);
                comment_btn = itemView.findViewById(R.id.comment_btn);
                return_btn = itemView.findViewById(R.id.return_btn);
                cancel_btn = itemView.findViewById(R.id.cancel_btn);
                delivery_btn = itemView.findViewById(R.id.delivery_btn);
                btn_area = itemView.findViewById(R.id.btn_area);
                cart_btn = itemView.findViewById(R.id.cart_btn);
                image = itemView.findViewById(R.id.image);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 1000) {
                    //상품평 작성
                    initData();
                } else if (requestCode == 1001) {
                    //주문취소
                    cancelOrder(data.getStringExtra("order_num"));
                } else if (requestCode == 1002) {
                    //반품신청
                    initData();
                }
            }
        } catch (Exception e) {
            L.e(e);
        }
    }

    public void cancelOrder(String order_num) {

        try {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.MALL_BASEURL)
                    .service(REQUEST_CANCEL_ORDER)
                    .addParam("lang_type", "kor")
                    .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                    .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                    .addParam("order_num", order_num)
                    .execute(new ICallBack<Object>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(Object response, int id) {

                            try {
                                if (response == null) {
                                    return;
                                }
                                initData();
                            } catch (Exception e) {
                                L.e(e);
                            }


                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }
}
