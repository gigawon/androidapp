package com.giga.shopping.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PathMeasure;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.library.base.IBaseActivity;
import com.giga.library.util.L;
import com.giga.library.util.LogUtils;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.CommentAdapter;
import com.giga.shopping.bean.CommentBean;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ExpandBean;
import com.giga.shopping.http.GlideImageLoader;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.SonicRuntimeImpl;
import com.giga.shopping.listener.SonicSessionClientImpl;
import com.giga.shopping.utils.GlideSimpleTarget;
import com.giga.shopping.utils.NineUtils;
import com.giga.shopping.widget.IdeaScrollView;
import com.giga.shopping.widget.NewBanner;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.tencent.sonic.sdk.SonicConfig;
import com.tencent.sonic.sdk.SonicEngine;
import com.tencent.sonic.sdk.SonicSession;
import com.tencent.sonic.sdk.SonicSessionConfig;
import com.youth.banner.BannerConfig;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ch.ielse.view.imagewatcher.ImageWatcher;
import okhttp3.Call;
import okhttp3.Request;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import qiu.niorgai.StatusBarCompat;

public class GoodsDetailActivity extends AppBaseActivity implements ImageWatcher.OnPictureLongPressListener, ImageWatcher.Loader {

    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.view_pager)
    NewBanner viewPager;
    @BindView(R.id.one)
    LinearLayout one;
    @BindView(R.id.two)
    LinearLayout two;
    @BindView(R.id.three)
    LinearLayout three;
    @BindView(R.id.ideaScrollView)
    IdeaScrollView ideaScrollView;
    @BindView(R.id.title_text_view)
    TextView titleTextView;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.layer)
    View layer;
    @BindView(R.id.headerParent)
    LinearLayout headerParent;
    @BindView(R.id.price_red)
    TextView priceRed;
    @BindView(R.id.price_gray)
    TextView priceGray;
    @BindView(R.id.goods_name)
    TextView goodsName;
    @BindView(R.id.express_price)
    TextView expressPrice;
    @BindView(R.id.sold_num)
    TextView soldNum;
    @BindView(R.id.location_sold)
    TextView locationSold;
    @BindView(R.id.text_spec)
    TextView textSpec;
    @BindView(R.id.button_choose)
    TextView buttonChoose;
    @BindView(R.id.layout_choose_spec)
    LinearLayout layoutChooseSpec;
    @BindView(R.id.all_comment)
    TextView allComment;
    @BindView(R.id.comment_recycler)
    RecyclerView commentRecycler;
    @BindView(R.id.share_img)
    ImageView shareImg;
    @BindView(R.id.text_share)
    TextView textShare;
    @BindView(R.id.layout_share)
    RelativeLayout layoutShare;
    @BindView(R.id.collection_img)
    ImageView collectionImg;
    @BindView(R.id.text_collection)
    TextView textCollection;
    @BindView(R.id.layout_collection)
    RelativeLayout layoutCollection;
    @BindView(R.id.shopcart_img)
    ImageView shopcartImg;
    @BindView(R.id.text_shopcart)
    TextView textShopcart;
    @BindView(R.id.layout_shop_cart)
    RelativeLayout layoutShopCart;
    @BindView(R.id.shop_cart)
    TextView shopCart;
    @BindView(R.id.layout_add_cart)
    RelativeLayout layoutAddCart;
    @BindView(R.id.buy_now)
    TextView buyNow;
    @BindView(R.id.layout_buy_now)
    RelativeLayout layoutBuyNow;
    @BindView(R.id.linear_shop_cart)
    LinearLayout linearShopCart;

    private DecimalFormat decimalFormat = new DecimalFormat("#,###");
    private CommentAdapter commentAdapter;
    private ImageWatcher mImageWatcher;
    private SonicSession sonicSession;
    private int height, width;
    private float currentPercentage = 0;
    private boolean isNeedScrollTo = true;
    private List<String> listimg = new ArrayList<>();
    private Badge badge;
    private int goodsCount = 0;
    private RadioGroup.OnCheckedChangeListener radioGroupListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            try {
                for (int i = 0; i < radioGroup.getChildCount(); i++) {
                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
                    radioButton.setTextColor(!radioButton.isChecked() ? getRadioCheckedAlphaColor(currentPercentage) : getRadioAlphaColor(currentPercentage));
                    titleTextView.setTextColor(getRadioAlphaColor(currentPercentage));
                    if (radioButton.isChecked() && isNeedScrollTo) {
                        ideaScrollView.setPosition(i);
                    }
                }
            } catch (Exception e) {
                L.e(e);
            }
        }
    };

    private JSONObject produceData;

    private WebView detailwebView;

    private String itemCode;

    private String supplyCode;

    private String customCode;

    private String token;

    @Override
    protected void onBefore() {
        super.onBefore();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        if (!SonicEngine.isGetInstanceAllowed()) {
            SonicEngine.createInstance(new SonicRuntimeImpl(getApplication()), new SonicConfig.Builder().build());
        }
    }

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_goods_detail);
        StatusBarCompat.translucentStatusBar(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        DisplayMetrics dm = getResources().getDisplayMetrics();
        width = dm.widthPixels;
        height = dm.heightPixels;
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewPager.getLayoutParams();
        params.height = width;
        viewPager.setLayoutParams(params);
        viewPager.setFocusable(true);
        viewPager.setFocusableInTouchMode(true);
        viewPager.requestFocus();

        Rect rectangle = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
        ideaScrollView.setViewPager(viewPager, getMeasureHeight(headerParent) - rectangle.top);
        radioGroup.setAlpha(0);
        radioGroup.check(radioGroup.getChildAt(0).getId());

//        ArrayList<Integer> araryDistance = new ArrayList<>();
//        araryDistance.add(0);
//        araryDistance.add(getMeasureHeight(one) - getMeasureHeight(headerParent));
//        araryDistance.add(getMeasureHeight(one) + getMeasureHeight(two) * 3 - getMeasureHeight(headerParent));
//        LogUtils.d(getMeasureHeight(one));
//        ideaScrollView.setArrayDistance(araryDistance);

        ideaScrollView.setOnScrollChangedColorListener(new IdeaScrollView.OnScrollChangedColorListener() {
            @Override
            public void onChanged(float percentage) {
                int color = getAlphaColor(percentage > 0.9f ? 1.0f : percentage);
                header.setBackgroundDrawable(new ColorDrawable(color));
                radioGroup.setBackgroundDrawable(new ColorDrawable(color));
                radioGroup.setAlpha((percentage > 0.9f ? 1.0f : percentage) * 255);
                setRadioButtonTextColor(percentage);

            }

            @Override
            public void onChangedFirstColor(float percentage) {

            }

            @Override
            public void onChangedSecondColor(float percentage) {

            }
        });

        ideaScrollView.setOnSelectedIndicateChangedListener(new IdeaScrollView.OnSelectedIndicateChangedListener() {
            @Override
            public void onSelectedChanged(int position) {
                isNeedScrollTo = false;
                radioGroup.check(radioGroup.getChildAt(position).getId());
                isNeedScrollTo = true;
            }
        });

        radioGroup.setOnCheckedChangeListener(radioGroupListener);
        mImageWatcher = (ImageWatcher) findViewById(R.id.image_watcher);
        mImageWatcher.setTranslucentStatus(NineUtils.calcStatusBarHeight(this));
        mImageWatcher.setErrorImageRes(R.mipmap.error_picture);
        mImageWatcher.setOnPictureLongPressListener(this);
        mImageWatcher.setLoader(this);
        commentRecycler.setHasFixedSize(true);
        commentRecycler.setNestedScrollingEnabled(false);
        commentRecycler.setLayoutManager(new LinearLayoutManager(this));

        commentAdapter = new CommentAdapter(this, mImageWatcher);
        commentRecycler.setAdapter(commentAdapter);
        badge = new QBadgeView(this).bindTarget(linearShopCart);
        badge.setBadgeGravity(Gravity.END | Gravity.TOP);
        initWeb();
    }

    private void initWeb() {
        detailwebView = (WebView) findViewById(R.id.webview);
        detailwebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (sonicSession != null) {
                    sonicSession.getSessionClient().pageFinish(url);
                }

                ArrayList<Integer> araryDistance = new ArrayList<>();
                araryDistance.add(0);
                araryDistance.add(getMeasureHeight(one) - getMeasureHeight(headerParent));
                araryDistance.add(getMeasureHeight(one) + getMeasureHeight(two) - getMeasureHeight(headerParent));
                LogUtils.d(getMeasureHeight(one));
                ideaScrollView.setArrayDistance(araryDistance);
            }

            @TargetApi(21)
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                return shouldInterceptRequest(view, request.getUrl().toString());
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                if (sonicSession != null) {
                    return (WebResourceResponse) sonicSession.getSessionClient().requestResource(url);
                }
                return null;
            }
        });

        WebSettings webSettings = detailwebView.getSettings();
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);

    }

    @Override
    protected void initData() {
        super.initData();

        itemCode = getIntent().getStringExtra("itemCode");
        supplyCode = getIntent().getStringExtra("supplyCode");
        customCode = SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE);
        token = SPUtils.getInstance().getString(AppConstants.KEY.TOKEN);

        asyncMakeData();

    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    private void initViewPager(JSONObject data) {

        try {
            for (int i = 0; i < 5; i++) {
                String imgUrl = data.getString("item_img_url" + (i + 1));
                if (!"".equals(imgUrl))
                    listimg.add(imgUrl);
            }
            viewPager.setImageLoader(new GlideImageLoader());
            viewPager.setImages(listimg);
            viewPager.updateBannerStyle(BannerConfig.NUM_INDICATOR);
            viewPager.isAutoPlay(false);
            viewPager.start();

        } catch (Exception e) {
            L.e(e);
        }
    }


    public void setRadioButtonTextColor(float percentage) {
        if (Math.abs(percentage - currentPercentage) >= 0.1f) {
            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
                radioButton.setTextColor(!radioButton.isChecked() ? getRadioCheckedAlphaColor(percentage) : getRadioAlphaColor(percentage));
            }
            titleTextView.setTextColor(getRadioAlphaColor(percentage));
            this.currentPercentage = percentage;
        }
    }

    public int getMeasureHeight(View view) {
        int width = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int height = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        view.measure(width, height);
        return view.getMeasuredHeight();
    }

    public int getAlphaColor(float f) {
        return Color.argb((int) (f * 255), 0xea, 0x3a, 0x3c);
    }

    public int getLayerAlphaColor(float f) {
        return Color.argb((int) (f * 255), 0x09, 0xc1, 0xf4);
    }

    public int getRadioCheckedAlphaColor(float f) {
        return Color.argb((int) (f * 255), 0x44, 0x44, 0x44);
    }

    public int getRadioAlphaColor(float f) {
        return Color.argb((int) (f * 255), 0xFF, 0xFF, 0xFF);
    }

    @Override
    protected void setStatusBar() {
    }

    private void asyncMakeData() {

        //'myData': "{\"lang_type\": \"kor\", \"user_id\":\"\", \"supply_code\": \"010530117822fbe4\",\"div_code\": \"2\",\"custom_code\": \"\",\"token\": \"\", \"item_code\": \"180311013088\", \"level1\":\"\", \"level2\":\"\", \"level3\":\"\", \"level4\":\"\", \"level5\":\"\"}"

        new HttpHelper.Create(this)
                .setUrl(AppConstants.URL.WWW_BASEURL)
                .service(AppConstants.SERVICE.REQUEST_GOODS_DETAIL)
                .addParam("myData", "{lang_type:'kor',div_code:'2',user_id:'',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "',item_code:'" + itemCode + "',supply_code:'" + supplyCode + "',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',level1:'',level2:'',level3:'',level4:'',level5:''}")
                .execute(new ICallBack<DataDetail>() {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        showProgressDialog();
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        hideProgressDialog();
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ToastUtils.showShortToast(e.getMessage());
                    }

                    @Override
                    public void onResponse(DataDetail response, int id) {
                        if (response == null) {
                            return;
                        }

                        try {

                            JSONObject main = new JSONObject(response.getD());
                            JSONArray list = main.getJSONArray("data");
                            JSONObject data = list.getJSONObject(0);

                            produceData = data;

                            titleTextView.setText(data.getString("item_name"));
                            goodsName.setText(data.getString("item_name"));
                            priceRed.setText("할인가 : " + decimalFormat.format(Double.parseDouble(data.getString("item_p"))) + getString(R.string.price_unit));
                            priceGray.setText("판매가 : " + decimalFormat.format(Double.parseDouble(data.getString("market_p"))) + getString(R.string.price_unit));
                            priceGray.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                            expressPrice.setText(getString(R.string.delivery_p) + " : " + decimalFormat.format(Double.parseDouble(data.getString("basic_delivery_p"))) + getString(R.string.price_unit));
                            soldNum.setText(getString(R.string.stock) + " : " + decimalFormat.format(Double.parseDouble(data.getString("stock_q"))) + " " + data.getString("order_unit"));
                            locationSold.setText("원산지 : 대한민국" + data.getString("country_origin"));

                            String item_detail_html = data.getString("item_detail_html");
                            detailwebView.loadData("<html><body>" + item_detail_html + "</body></html>", "text/html","charset=utf-8");
                            //viewpager
                            initViewPager(data);

                            if (!"".equals(token) && !"".equals(customCode))
                                getCartInfo();

                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

    }

    public void getCartInfo() {

        //'myData': "{\"lang_type\":\"kor\" ,\"custom_code\":\"0102109209162c04\" ,\"token\":\"0102109209162c04929d0b56-d941-4ad9-b460-edbd5c356331\"}"

        new HttpHelper.Create(this)
                .setUrl(AppConstants.URL.WWW_BASEURL)
                .service(AppConstants.SERVICE.REQUEST_CART_INFO)
                .addParam("myData", "{lang_type:'kor',token:'" + token + "',custom_code:'" + customCode + "'}")
                .execute(new ICallBack<DataDetail>() {

                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                    }

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ToastUtils.showShortToast(e.getMessage());
                    }

                    @Override
                    public void onResponse(DataDetail response, int id) {
                        if (response == null) {
                            return;
                        }

                        try {
                            JSONObject main = new JSONObject(response.getD());
                            goodsCount = Integer.parseInt(main.getString("carnumber"));
                            badge.setBadgeNumber(goodsCount);

                            getCommentList();

                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

    }

    public void addCart() {

        try {

            //	'myData': "{\"lang_type\":\"kor\",\"custom_code\":\"0102109209162c04\",\"token\":\"0102109209162c042b92f0de-5aa0-4420-9029-980d34c9ee4e\",\"mall_custom_code\":\"gigakorea\",\"supply_code\":\"010530117822fbe4\",\"item_code\":\"180311013088 \",\"item_p\":\"26730.00\",\"SALE_Q\":\"1\",\"delivery_p\":\"1500\"}"

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_ADD_CART_MALL_ORDER)
                    .addParam("myData", "{" +
                            "lang_type:'kor" +
                            "',div_code:'2" +
                            "',item_p:'" + produceData.getString("item_p") +
                            "',token:'" + token +
                            "',item_code:'" + itemCode +
                            "',supply_code:'" + supplyCode +
                            "',custom_code:'" + customCode +
                            "',SALE_Q:'1" +
                            "',delivery_p:'" + produceData.getString("basic_delivery_p") +
                            "',mall_custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.MALL_HOME_ID) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {
                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));
                                getCartInfo();
                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public void addMallOrderCart() {

        try {

            //	'myData': "{\"lang_type\":\"kor\",\"custom_code\":\"0102109209162c04\",\"token\":\"0102109209162c042b92f0de-5aa0-4420-9029-980d34c9ee4e\",\"mall_custom_code\":\"gigakorea\",\"supply_code\":\"010530117822fbe4\",\"item_code\":\"180311013088 \",\"item_p\":\"26730.00\",\"SALE_Q\":\"1\",\"delivery_p\":\"1500\"}"

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_ADD_CART_MALL_ORDER)
                    .addParam("myData", "{" +
                            "lang_type:'kor" +
                            "',div_code:'2" +
                            "',item_p:'" + produceData.getString("item_p") +
                            "',token:'" + token +
                            "',item_code:'" + itemCode +
                            "',supply_code:'" + supplyCode +
                            "',custom_code:'" + customCode +
                            "',SALE_Q:'1" +
                            "',delivery_p:'" + produceData.getString("basic_delivery_p") +
                            "',mall_custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.MALL_HOME_ID) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {
                                JSONObject item = new JSONObject();
                                item.put("item_name", produceData.getString("item_name"));
                                item.put("item_code",itemCode);
                                item.put("supply_code",supplyCode);
                                item.put("sale_q","1");
                                JSONArray orderList = new JSONArray();
                                orderList.put(item);

                                Intent intent = new Intent(GoodsDetailActivity.this, ConfirmActivity.class);
                                intent.putExtra("itemList", orderList.toString());
                                intent.putExtra("totalAmount", Integer.parseInt(produceData.getString("item_p")) + Integer.parseInt(produceData.getString("basic_delivery_p")));
                                intent.putExtra("deliveryAmount", Integer.parseInt(produceData.getString("basic_delivery_p")));
                                startActivity(intent);

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    public void load(Context context, String url, ImageWatcher.LoadCallback lc) {
        Glide.with(context).asBitmap().load(url).into(new GlideSimpleTarget(lc));
    }

    @Override
    public void onPictureLongPress(ImageView v, String url, int pos) {

    }

    @OnClick({R.id.button_choose, R.id.all_comment, R.id.layout_collection, R.id.layout_shop_cart, R.id.layout_add_cart, R.id.layout_buy_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_choose:
                break;
            case R.id.all_comment:
                Intent i = new Intent(GoodsDetailActivity.this, AllCommentActivity.class);
                i.putExtra("itemCode", itemCode);
                i.putExtra("supplyCode", supplyCode);
                startActivityForResult(i, 1000);
                break;
            case R.id.layout_share:
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.addCategory(Intent.CATEGORY_DEFAULT);
                msg.putExtra(Intent.EXTRA_TEXT, goodsName.getText().toString());
                msg.setType("text/plain");
                startActivity(Intent.createChooser(msg, goodsName.getText().toString()));
                break;
            case R.id.layout_collection:
                break;
            case R.id.layout_shop_cart:
                if ("1".equals(SPUtils.getInstance().getString(AppConstants.KEY.LOGIN_STATUS))) {
                    Intent intent = new Intent(AppConstants.INTENT_FILTER.INTENT_FINISH);
                    intent.putExtra("index",2);
                    sendBroadcast(intent);
                }else
                    startActivity(new Intent(GoodsDetailActivity.this, LoginActivity.class));
                break;
            case R.id.layout_add_cart:
                if ("1".equals(SPUtils.getInstance().getString(AppConstants.KEY.LOGIN_STATUS)))
                    addCart();
                else
                    startActivity(new Intent(GoodsDetailActivity.this, LoginActivity.class));
                break;
            case R.id.layout_buy_now:
                if ("1".equals(SPUtils.getInstance().getString(AppConstants.KEY.LOGIN_STATUS))) {
                    try {
                        addMallOrderCart();
                    } catch (Exception e) {
                        L.e(e);
                    }
                }else
                    startActivity(new Intent(GoodsDetailActivity.this, LoginActivity.class));
                break;
        }
    }

    public void getCommentList() {

        try {
            new HttpHelper.Create(GoodsDetailActivity.this)
                    .setUrl(AppConstants.URL.MALL_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_COMMENT_LIST)
                    .addParam("lang_type", "kor")
                    .addParam("sale_custom_code", supplyCode)
                    .addParam("item_code", itemCode)
                    .addParam("pg", "1")
                    .addParam("pagesize", "10")
                    .execute(new ICallBack<CommentBean>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(CommentBean response, int id) {
                            try {
                                if (response == null) {
                                    return;
                                }

                                if (response.getStatus().equals("1")) {
//                                    String Assesses = result.getJSONArray("Assesses");
                                    List<LinkedTreeMap<Object,Object>> list = response.getAssesses();
                                    List<ExpandBean> friendCircleBeans = new ArrayList<>();
                                    for (int i = 0; i < list.size(); i++) {

                                        ExpandBean b = new ExpandBean();
                                        LinkedTreeMap<Object,Object> j = list.get(i);
                                        b.setCustom_name(j.get("custom_name").toString());
                                        b.setNick_name(j.get("nick_name").toString());
                                        b.setReg_date(j.get("reg_date").toString());
                                        b.setScore(j.get("score").toString());
                                        b.setRep_content(j.get("rep_content").toString());
                                        b.setSale_content(j.get("sale_content").toString());
                                        b.setExpanded(true);
                                        b.setCustom_img_path(j.get("custom_img_path").toString());
                                        friendCircleBeans.add(b);
                                    }

                                    if (friendCircleBeans != null) {
                                        commentAdapter.setFriendCircleBeans(friendCircleBeans);
                                    }
                                }

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });
        } catch (Exception e) {
            L.e(e);
        }

    }
}