package com.giga.shopping.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

import static com.giga.library.util.Utils.getContext;

public class RateActivity extends AppBaseActivity {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.product_name)
    TextView product_name;
    @BindView(R.id.rating_bar)
    RatingBar rating_bar;
    @BindView(R.id.comment)
    EditText comment;
    @BindView(R.id.grid_view)
    GridView grid_view;
    @BindView(R.id.save_btn)
    LinearLayout save_btn;

    private ArrayList<String> imageList = new ArrayList<>();

    private boolean isCreate = true;
    private String comment_id;
    private String item_image;
    private String item_name;
    private String order_num;
    private String sale_custom_code;
    private String item_code;
    private int position;
    private File tempFile;
    private int gridPostion = -1;
    private GridAdapter gridAdapter;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //httpMultiPart(String.valueOf((int)rating_bar.getRating()), comment.getText().toString());
                //new UploadTask().execute(String.valueOf((int)rating_bar.getRating()), comment.getText().toString());

                if (isCreate == true) {
                    new HttpHelper.Create(RateActivity.this)
                            .setUrl(AppConstants.URL.MALL_BASEURL)
                            .service(AppConstants.SERVICE.REQUEST_COMMENT_CREATE)
                            .addParam("lang_type", "kor")
                            .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                            .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                            .addParam("rep_content", comment.getText().toString())
                            .addParam("score", String.valueOf((int) rating_bar.getRating()))
                            .addParam("item_code", item_code)
                            .addParam("order_num", order_num)
                            .addParam("sale_custom_code", sale_custom_code)
                            .execute(new ICallBack<Object>() {

                                @Override
                                public void onBefore(Request request, int id) {
                                    super.onBefore(request, id);
                                    showProgressDialog();
                                }

                                @Override
                                public void onAfter(int id) {
                                    super.onAfter(id);
                                    hideProgressDialog();
                                }

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    ToastUtils.showShortToast(e.getMessage());
                                }

                                @Override
                                public void onResponse(Object response, int id) {
                                    try {
                                        if (response == null) {
                                            return;
                                        }

                                        JSONObject result = new JSONObject(response.toString());
                                        if (result.getString("status").equals("1")) {
                                            Toast.makeText(RateActivity.this, "상품평이 등록되었습니다.", Toast.LENGTH_SHORT).show();
                                            setResult(RESULT_OK);
                                            finish();
                                        } else {
                                            Toast.makeText(RateActivity.this, result.getString("msg"), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e) {
                                        L.e(e);
                                    }
                                }
                            });
                } else {
                    new HttpHelper.Create(RateActivity.this)
                            .setUrl(AppConstants.URL.MALL_BASEURL)
                            .service(AppConstants.SERVICE.REQUEST_COMMENT_EDIT)
                            .addParam("lang_type", "kor")
                            .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                            .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                            .addParam("rep_content", comment.getText().toString())
                            .addParam("score", String.valueOf((int) rating_bar.getRating()))
                            .addParam("id", comment_id)
                            .execute(new ICallBack<Object>() {

                                @Override
                                public void onBefore(Request request, int id) {
                                    super.onBefore(request, id);
                                    showProgressDialog();
                                }

                                @Override
                                public void onAfter(int id) {
                                    super.onAfter(id);
                                    hideProgressDialog();
                                }

                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    ToastUtils.showShortToast(e.getMessage());
                                }

                                @Override
                                public void onResponse(Object response, int id) {
                                    try {
                                        if (response == null) {
                                            return;
                                        }

                                        JSONObject result = new JSONObject(response.toString());
                                        if (result.getString("status").equals("1")) {
                                            Toast.makeText(RateActivity.this, "상품평이 등록되었습니다.", Toast.LENGTH_SHORT).show();
                                            setResult(RESULT_OK);
                                            finish();
                                        } else {
                                            Toast.makeText(RateActivity.this, result.getString("msg"), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception e) {
                                        L.e(e);
                                    }
                                }
                            });
                }
            }
        });

        tedPermission();

    }

    @Override
    public void setRequestedOrientation(int requestedOrientation) {
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            super.setRequestedOrientation(requestedOrientation);
        }
    }

    @Override
    protected void initData() {
        super.initData();

        try {

            isCreate = getIntent().getBooleanExtra("isCreate", true);
            comment_id = getIntent().getStringExtra("comment_id");
            item_image = getIntent().getStringExtra("item_image");
            item_name = getIntent().getStringExtra("item_name");
            order_num = getIntent().getStringExtra("order_num");
            sale_custom_code = getIntent().getStringExtra("sale_custom_code");
            item_code = getIntent().getStringExtra("item_code");
            position = getIntent().getIntExtra("position", -1);

            loadData();
        } catch (Exception e) {
            L.e(e);
        }
    }

    private void loadData() {

        try {

            Glide.with(RateActivity.this).load(item_image).into(image);
            product_name.setText(item_name);

            grid_view.setVisibility(View.GONE);
            grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RateActivity.this);
                    builder.setTitle("사진가져오기");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("상품평에 등록할 사진을 가져옵니다");
                    builder.setPositiveButton("앨범",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    takeFromAlbum(position);
                                }
                            });
                    builder.setNegativeButton("사진촬영",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    takePhoto(position);
                                }
                            });
                    builder.show();
                }
            });

            grid_view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    if (imageList.get(position) == null || imageList.get(position).equals("")) {
                        return true;
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(RateActivity.this);
                    builder.setTitle("사진삭제");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("등록된 사진을 삭제 합니다");
                    builder.setPositiveButton("취소",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.setNegativeButton("삭제",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    imageList.set(position, "");
                                    gridAdapter.notifyDataSetChanged();
                                }
                            });
                    builder.show();
                    return true;
                }
            });

            if (isCreate == true) {
//                for (int i =0; i < 5; i++)
//                    imageList.add("");
//                gridAdapter = new GridAdapter();
//                grid_view.setAdapter(gridAdapter);
            } else {

                new HttpHelper.Create(RateActivity.this)
                        .setUrl(AppConstants.URL.MALL_BASEURL)
                        .service(AppConstants.SERVICE.REQUEST_COMMENT_GET)
                        .addParam("lang_type", "kor")
                        .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                        .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                        .addParam("order_num", order_num)
                        .addParam("sale_custom_code", sale_custom_code)
                        .addParam("item_code", item_code)
                        .execute(new ICallBack<Object>() {

                            @Override
                            public void onBefore(Request request, int id) {
                                super.onBefore(request, id);
                                showProgressDialog();
                            }

                            @Override
                            public void onAfter(int id) {
                                super.onAfter(id);
                                hideProgressDialog();
                            }

                            @Override
                            public void onError(Call call, Exception e, int id) {
                                ToastUtils.showShortToast(e.getMessage());
                            }

                            @Override
                            public void onResponse(Object response, int id) {
                                try {
                                    if (response == null) {
                                        return;
                                    }

                                    LinkedTreeMap<Object,Object> r = (LinkedTreeMap)response;
                                    JSONObject result = new JSONObject();
                                    result.put("assess_img_list", r.get("assess_img_list").toString());
                                    result.put("status", r.get("status").toString());
                                    result.put("flag", r.get("flag").toString());
                                    result.put("msg", r.get("msg").toString());
                                    result.put("id", r.get("id").toString());
                                    result.put("score", r.get("score").toString());
                                    result.put("rep_content", r.get("rep_content").toString());
                                    result.put("sale_content", r.get("sale_content").toString());
                                    result.put("reg_date", r.get("reg_date").toString());
                                    result.put("sale_reg_date", r.get("sale_reg_date").toString());
                                    if (result.getString("status").equals("1")) {
                                        comment.setText(result.getString("rep_content"));
                                        rating_bar.setRating(Float.parseFloat(result.getString("score")));
                                        comment_id = result.getString("id");
                                    }
                                } catch (Exception e) {
                                    L.e(e);
                                }
                            }
                        });

            }

        } catch (Exception e) {
            L.e(e);
        }

    }


    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    public class GridAdapter extends BaseAdapter {

        int width;

        public GridAdapter() {
            DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();

            width = dm.widthPixels;
        }

        public int getCount() {
            return imageList.size();
        }

        public Object getItem(int position) {
            return imageList.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {

            int rowWidth = (int)((width) / 5.7);

            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(RateActivity.this);
                imageView.setLayoutParams(new GridView.LayoutParams(rowWidth,rowWidth));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setPadding(1, 1, 1, 1);
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setBackgroundResource(R.drawable.order_list_box);

            if (imageList.get(position) == null || imageList.get(position).equals("")) {
                imageView.setImageResource(R.mipmap.img_bitmap);
            } else if (imageList.get(position).startsWith("http")){
                Glide.with(RateActivity.this).load(imageList.get(position)).into(imageView);
            } else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap originalBm = BitmapFactory.decodeFile(imageList.get(position), options);
                imageView.setImageBitmap(originalBm);
            }
            return imageView;
        }
    }

    private void tedPermission() {

        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                // 권한 요청 성공

            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                // 권한 요청 실패
            }
        };

        TedPermission.with(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage("사진 및 파일을 저장하기 위하여 접근 권한이 필요합니다.")
                .setDeniedMessage("[설정] > [권한] 에서 권한을 허용할 수 있습니다.")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();

    }

    private void takeFromAlbum(int position) {
        gridPostion = position;
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, 1000);
    }

    private void takePhoto(int position) {
        gridPostion = position;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            tempFile = createImageFile();
        } catch (IOException e) {
            Toast.makeText(this, "이미지 처리 오류! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return;
        }
        if (tempFile != null) {
            //Uri photoUri = Uri.fromFile(tempFile);
            Uri photoUri = FileProvider.getUriForFile(getContext(), "com.giga.gigawon.fileprovider", tempFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, 1001);
        }
    }

    private File createImageFile() throws IOException {

        // 이미지 파일 이름 ( blackJin_{시간}_ )
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "blackJin_" + timeStamp + "_";

        // 이미지가 저장될 폴더 이름 ( blackJin )
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/blackJin/");
        if (!storageDir.exists()) storageDir.mkdirs();

        // 빈 파일 생성
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        return image;
    }

    private void setImage() {
        imageList.set(gridPostion, tempFile.getAbsolutePath());
        gridAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
            if(tempFile != null) {
                if (tempFile.exists()) {
                    if (tempFile.delete()) {
                        tempFile = null;
                    }
                }
            }
            return;
        }

        if (requestCode == 1000) {

            Uri photoUri = data.getData();

            Cursor cursor = null;

            try {

                /*
                 *  Uri 스키마를
                 *  content:/// 에서 file:/// 로  변경한다.
                 */
                String[] proj = { MediaStore.Images.Media.DATA };

                assert photoUri != null;
                cursor = getContentResolver().query(photoUri, proj, null, null, null);

                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

                cursor.moveToFirst();

                tempFile = new File(cursor.getString(column_index));

            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

            setImage();

        } else if (requestCode == 1001) {

            setImage();

        }
    }

    private boolean upload(String score, String rep_content){


        try {

            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost("http://211.43.220.230:8075/70_gigawon/uploadEvaluation.aspx?ProgressID=0");
            MultipartEntity mpEntity = new MultipartEntity();

            for (int i = 0; i < 5; i++) {

                String fileName = imageList.get(i);

                if (fileName == null || fileName.equals("") || fileName.startsWith("http")) {
                    mpEntity.addPart("file" + (i + 1), new StringBody(""));
                    continue;
                }

                File file = new File(fileName);
                ContentBody cbFile = new FileBody(file, "image/jpeg");
                mpEntity.addPart("file" + (i + 1), cbFile);

            }
            mpEntity.addPart("lang_type", new StringBody("kor"));
            mpEntity.addPart("order_num", new StringBody(order_num));
            mpEntity.addPart("custom_code", new StringBody(SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE)));
            mpEntity.addPart("token", new StringBody(SPUtils.getInstance().getString(AppConstants.KEY.TOKEN)));
            mpEntity.addPart("sale_custom_code", new StringBody(sale_custom_code));
            mpEntity.addPart("item_code", new StringBody(item_code));
            mpEntity.addPart("score", new StringBody(score));
            mpEntity.addPart("rep_content", new StringBody(rep_content));

            httppost.setEntity(mpEntity);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            String responseData = EntityUtils.toString(resEntity).toString(); // XML문서 읽기

            L.d("return : " + responseData);
            httpclient.getConnectionManager().shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public class UploadTask extends AsyncTask<Object, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }
        @Override
        protected Integer doInBackground(Object... parmas) {

            String score = (String) parmas[0];
            String rep_content = (String) parmas[1];

            return upload(score, rep_content) == true ? 0 : 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // doInBackground 에서 받아온 total 값 사용 장소
            hideProgressDialog();
            if (result == 0) {
                Toast.makeText(RateActivity.this, "상품평이 등록되었습니다.", Toast.LENGTH_SHORT).show();
                finish();
            } else
                Toast.makeText(RateActivity.this, "상품평 등록이 실패되었습니다.", Toast.LENGTH_SHORT).show();
        }

    }


    private void httpMultiPart(String score, String rep_content){

        new AsyncTask<Void, Void, String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog();
            }

            @Override
            protected String doInBackground(Void... voids) {

                String boundary = "^-----^";
                String LINE_FEED = "\r\n";
                String charset = "UTF-8";
                OutputStream outputStream;
                PrintWriter writer;

                String result = null;
                try{

                    URL url = new URL("http://211.43.220.230:8075/70_gigawon/uploadEvaluation.aspx?ProgressID=0");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    connection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=" + boundary);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);
                    connection.setConnectTimeout(15000);

                    outputStream = connection.getOutputStream();
                    writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"lang_type\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append("kor").append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"order_num\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(order_num).append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"custom_code\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE)).append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"token\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(SPUtils.getInstance().getString(AppConstants.KEY.TOKEN)).append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"sale_custom_code\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(sale_custom_code).append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"item_code\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(item_code).append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"score\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(score).append(LINE_FEED);
                    writer.flush();

                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"rep_content\"").append(LINE_FEED);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.append(rep_content).append(LINE_FEED);
                    writer.flush();

                    for (int i = 0; i < imageList.size(); i++) {

                        String fileName = imageList.get(i);

                        if (fileName == null || fileName.equals("") || fileName.startsWith("http")) {
                            continue;
                        }

                        File file = new File(fileName);

                        /** 파일 데이터를 넣는 부분**/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"file"+ (i + 1) + "\"; filename=\"" + file.getName() + "\"").append(LINE_FEED);
                        //writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(LINE_FEED);
                        String contentType = URLConnection.guessContentTypeFromName(file.getName());
                        writer.append("Content-Type: " + contentType).append(LINE_FEED);
                        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.flush();

                        FileInputStream inputStream = new FileInputStream(file);
                        byte[] buffer = new byte[(int) file.length()];
                        int bytesRead = -1;
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                        outputStream.flush();
                        inputStream.close();
                        writer.append(LINE_FEED);
                        writer.flush();

                    }

                    writer.append("--" + boundary + "--").append(LINE_FEED);
                    writer.close();

                    int responseCode = connection.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();

                        try {
                            result = response.toString();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        result = response.toString();
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                L.d("result : " + result);

                hideProgressDialog();

            }

        }.execute();
    }

    public String multipartRequest(String urlTo, Map<String, String> parmas, String filepath, String filefield, String fileMimeType) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;

        String twoHyphens = "--";
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String lineEnd = "\r\n";

        String result = "";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        String[] q = filepath.split("/");
        int idx = q.length - 1;

        try {
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL(urlTo);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);

            // Upload POST Data
            Iterator<String> keys = parmas.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = parmas.get(key);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


            if (200 != connection.getResponseCode()) {
                return "Failed to upload code:" + connection.getResponseCode() + " " + connection.getResponseMessage();
            }

            inputStream = connection.getInputStream();

            result = this.convertStreamToString(inputStream);

            fileInputStream.close();
            inputStream.close();
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            result = e.getMessage();
        }

        return result;

    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
