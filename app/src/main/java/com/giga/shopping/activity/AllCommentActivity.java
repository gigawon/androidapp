package com.giga.shopping.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.CommentAdapter;
import com.giga.shopping.bean.CommentBean;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ExpandBean;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.EndlessRecyclerOnScrollListener;
import com.giga.shopping.utils.GlideSimpleTarget;
import com.giga.shopping.utils.NineUtils;
import com.giga.shopping.widget.LoadMoreWrapper;
import com.giga.shopping.widget.NineGridView;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import ch.ielse.view.imagewatcher.ImageWatcher;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Request;

public class AllCommentActivity extends AppBaseActivity implements SwipeRefreshLayout.OnRefreshListener,ImageWatcher.OnPictureLongPressListener, ImageWatcher.Loader  {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private LoadMoreWrapper loadMoreWrapper;
    List<ExpandBean> friendCircleBeans = new ArrayList<>();
    private CommentAdapter loadMoreWrapperAdapter;
    private int pg          = 1;
    private int tot_page    = 1;
    private String supplyCode;
    private String itemCode;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_all_comment);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();
        setSwipeColor(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        loadMoreWrapperAdapter = new CommentAdapter(this, null);
        loadMoreWrapper = new LoadMoreWrapper(loadMoreWrapperAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(loadMoreWrapper);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING);

                if (pg < tot_page) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    getCommentList();
                                }
                            });
                        }
                    }, 1000);
                } else {
                    loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_END);
                }
            }

        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();

        try {

            pg = 1;
            friendCircleBeans = new ArrayList<>();
            itemCode = getIntent().getStringExtra("itemCode");
            supplyCode = getIntent().getStringExtra("supplyCode");
            getCommentList();

        } catch (Exception e) {
            L.e(e);
        }
    }

    public void getCommentList() {

        try {
            new HttpHelper.Create(AllCommentActivity.this)
                    .setUrl(AppConstants.URL.MALL_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_COMMENT_LIST)
                    .addParam("lang_type", "kor")
                    .addParam("sale_custom_code", supplyCode)
                    .addParam("item_code", itemCode)
                    .addParam("pg", pg)
                    .addParam("pagesize", "20")
                    .execute(new ICallBack<CommentBean>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(CommentBean response, int id) {
                            try {
                                if (response == null) {
                                    loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_END);
                                    return;
                                }

                                if (response.getStatus().equals("1")) {
//                                    String Assesses = result.getJSONArray("Assesses");
                                    List<LinkedTreeMap<Object,Object>> list = response.getAssesses();
                                    for (int i = 0; i < list.size(); i++) {

                                        ExpandBean b = new ExpandBean();
                                        LinkedTreeMap<Object,Object> j = list.get(i);
                                        b.setCustom_name(j.get("custom_name").toString());
                                        b.setNick_name(j.get("nick_name").toString());
                                        b.setReg_date(j.get("reg_date").toString());
                                        b.setRep_content(j.get("rep_content").toString());
                                        b.setSale_content(j.get("sale_content").toString());
                                        b.setScore(j.get("score").toString());
                                        b.setCustom_img_path(j.get("custom_img_path").toString());
                                        b.setExpanded(true);
                                        friendCircleBeans.add(b);
                                    }

                                    if (friendCircleBeans != null) {
                                        loadMoreWrapperAdapter.setFriendCircleBeans(friendCircleBeans);
                                    }
                                    loadMoreWrapperAdapter.notifyDataSetChanged();
                                    loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_END);
                                    pg = pg + 1;
                                    tot_page = Integer.parseInt(response.getTot_page());
                                }

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });
        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    public void onRefresh() {
        initData();
        stopRefreshing();
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    @Override
    public void load(Context context, String url, ImageWatcher.LoadCallback lc) {
        Glide.with(context).asBitmap().load(url).into(new GlideSimpleTarget(lc));
    }

    @Override
    public void onPictureLongPress(ImageView v, String url, int pos) {

    }

}
