package com.giga.shopping.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.giga.library.base.IBaseActivity;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;

public class RegisterActivity extends AppBaseActivity {


    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_register);
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }
}
