package com.giga.shopping.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.giga.library.util.GsonUtils;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.bean.DataMain;
import com.giga.shopping.fragment.ClassifyFragment;
import com.giga.shopping.fragment.HomePageFragment;
import com.giga.shopping.fragment.MineFragment;
import com.giga.shopping.fragment.CartFragment;
import com.giga.shopping.http.HttpHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

import com.giga.shopping.http.ICallBack;
import com.giga.shopping.http.OkHttpHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class MainActivity extends AppBaseActivity {

    @BindView(R.id.pageImage1)  ImageView       pageImage1;
    @BindView(R.id.pageImage2)  ImageView       pageImage2;
    @BindView(R.id.pageImage3)  ImageView       pageImage3;
    @BindView(R.id.pageImage4)  ImageView       pageImage4;

    @BindView(R.id.pageText1)   TextView        pageText1;
    @BindView(R.id.pageText2)   TextView        pageText2;
    @BindView(R.id.pageText3)   TextView        pageText3;
    @BindView(R.id.pageText4)   TextView        pageText4;

    @BindView(R.id.page1)       RelativeLayout  page1;
    @BindView(R.id.page2)       RelativeLayout  page2;
    @BindView(R.id.page3)       RelativeLayout  page3;
    @BindView(R.id.page4)       RelativeLayout  page4;

    @BindView(R.id.menuBar)     LinearLayout    menuBar;
    @BindView(R.id.finger_print_state)   TextView        finger_print_state;
    @BindView(R.id.finger_print)  ImageView       finger_print;


    public @BindView(R.id.loading_area)RelativeLayout    loadingArea;

    private int                 index = 0;
    private ImageView[]         pageImages;
    private TextView[]          pageTexts;
    private Fragment[]          fragments;
    public HomePageFragment     homePageFragment;
    public ClassifyFragment     classifyFragment;
    public CartFragment         cartFragment;
    public MineFragment         mineFragment;

    private JSONArray           categoryList;

    private DataMain            dataMain;

    public JSONArray getCategoryList() {
        return this.categoryList;
    }

    private int tab = 0;

    private String viewName;

    @Override
    protected void onBefore() {
        super.onBefore();
    }

    @Override
    protected void onAfter() {
        super.onAfter();

        checkPermission();
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    L.d("getInstanceId failed : " + task.getException());
                    initMainData();
                    return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();

                // Log and toast
                L.d("token : " + token);
                updateToken(token);
            }
        });
    }

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_gsmain);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();
        pageImages          = new ImageView[]{pageImage1, pageImage2, pageImage3,pageImage4};
        pageTexts           = new TextView[]{pageText1, pageText2, pageText3,pageText4};
        homePageFragment    = new HomePageFragment();
        classifyFragment    = new ClassifyFragment();
        cartFragment        = new CartFragment();
        mineFragment        = new MineFragment();
        fragments           = new Fragment[]{homePageFragment,classifyFragment, cartFragment,mineFragment};

        loadingArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        loadingArea.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                finger_print_state.setText("지문인증이 성공되었습니다.");
                finger_print_state.setTextColor(Color.parseColor("#FF0000"));
                finger_print.setBackgroundResource(R.drawable.ic_fingerprint_success);
                //sound effect
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone((Activity)context, notification);
                r.play();
                showMain();

                return false;
            }
        });
    }

    private void initMainData() {

        new HttpHelper.Create(this)
        .setUrl(AppConstants.URL.MALL_BASEURL)
        .service(AppConstants.SERVICE.REQUEST_GET_MAIN)
        .addParam("lang_type", "kor")
        .addParam("div_code", "2")
        .addParam("user_id", "")
        .addParam("token", "")
        .addParam("cateevent1", "")
        .addParam("cateEvent2", "")
        .execute(new ICallBack<DataMain>() {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                //showProgressDialog();
            }

            @Override
            public void onAfter(int id) {
                super.onAfter(id);
                //hideProgressDialog();
            }

            @Override
            public void onError(Call call, Exception e, int id) {
            }

            @Override
            public void onResponse(DataMain response, int id) {
                if (response == null) {
                    return;
                }

                dataMain = response;
                parseCategoryData();
                switchPage(index);

                showMain();

//                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//
//                    if(SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) == null || SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE).equals("")) {
//                        showMain();
//                        return;
//                    }
//                    fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
//                    keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//
//                    if(!fingerprintManager.isHardwareDetected()){//Manifest에 Fingerprint 퍼미션을 추가해 워야 사용가능
//                        showMain();
//                    } else if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){
//                        //Toast.makeText(MainActivity.this, "지문사용을 허용해 주세요.", Toast.LENGTH_SHORT).show();
//                        showMain();
//                        /*잠금화면 상태를 체크한다.*/
//                    } else if(!keyguardManager.isKeyguardSecure()){
//                        //Toast.makeText(MainActivity.this, "잠금화면을 설정해 주세요.", Toast.LENGTH_SHORT).show();
//                        showMain();
//                    } else if(!fingerprintManager.hasEnrolledFingerprints()){
//                        finger_print_state.setText("등록된 지문이 없습니다.");
//                        showMain();
//                    } else {//모든 관문을 성공적으로 통과(지문인식을 지원하고 지문 사용이 허용되어 있고 잠금화면이 설정되었고 지문이 등록되어 있을때)
//                        showFingerAuth();
//                        finger_print_state.setText("등록된 손가락을 지문인식 위치에 올려주세요.");
//                        generateKey();
//                        if(cipherInit()){
//                            cryptoObject = new FingerprintManager.CryptoObject(cipher);
//                            //핸들러실행
//                            FingerprintHandler fingerprintHandler = new FingerprintHandler(MainActivity.this);
//                            fingerprintHandler.startAutho(fingerprintManager, cryptoObject);
//                        }
//                    }
//                } else {
//                    showMain();
//                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (index == 2) {
            cartFragment.getCartList();
        } else if (index == 3) {
            mineFragment.loadData();
        }
    }

    @OnClick({R.id.page1, R.id.page2, R.id.page3, R.id.page4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.page1:    switchPage(0);  break;
            case R.id.page2:    switchPage(1);  break;
            case R.id.page3:    switchPage(2);  break;
            case R.id.page4:    switchPage(3);  break;
        }
    }

    public void switchPage(int selected) {

        tab = getIntent().getIntExtra("tab", -1);

        if (tab > -1) {
            selected = tab;
        } else {
            viewName = getIntent().getStringExtra("viewName");
            if (viewName != null && !viewName.equals("")) {
                if (viewName.equals("productDetail")) {
                    //상품상세
                    selected = 0;
                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this, GoodsDetailActivity.class);
                    intent.putExtra("itemCode", getIntent().getStringExtra("itemCode"));
                    intent.putExtra("supplyCode", getIntent().getStringExtra("supplyCode"));
                    startActivity(intent);
                }
            }
        }

        if (selected == 2 || selected == 3) {
            if (!"1".equals(SPUtils.getInstance().getString(AppConstants.KEY.LOGIN_STATUS))) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                i.putExtra("from", "main");
                i.putExtra("tab", selected);
                startActivityForResult(i, 2001);
                return;
            }
        }
        for (int i = 0; i < pageImages.length; i++) {
            if (i == selected) {
                pageImages[i].setSelected(true);
                pageTexts[i].setSelected(true);
                pageTexts[i].getPaint().setFakeBoldText(true);
            } else {
                pageImages[i].setSelected(false);
                pageTexts[i].setSelected(false);
                pageTexts[i].getPaint().setFakeBoldText(false);
            }
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragments[index].isAdded()) {
            transaction.hide(fragments[index]);
        }
        if (fragments[selected].isAdded()) {
            transaction.show(fragments[selected]);
            if (selected == 2) {
                cartFragment.getCartList();
            } else if (selected == 3) {
                mineFragment.loadData();
            }
        } else {
            transaction.add(R.id.content, fragments[selected]);
        }
        transaction.commit();
        index = selected;

        if (selected == 0) {
            if (HomePageFragment.isSuccessLoad == false) {
                ((HomePageFragment) fragments[index]).initLoad();
                return;
            }
        }
    }

    private void parseCategoryData() {

        categoryList = new JSONArray();

        JSONObject level2List = new JSONObject();
        JSONObject level3List = new JSONObject();

        List<DataMain.ItemlevelBean> itemLevelList = dataMain.getItemlevel();

        int count = itemLevelList.size();
        int position = 0;

        //전채 리스트 parsing, 정렬은 대메뉴만 정렬된다
        while (count > position) {
            try {
                DataMain.ItemlevelBean b = itemLevelList.get(position);
                //아이템 레벨 확인
                int itemDepth = getDepth(b);
                JSONObject item = makeCategoryItem(b, itemDepth);
                if (itemDepth == 3) {
                    if (!level3List.has(b.getLevel2())) {
                        level3List.put(b.getLevel2(), new JSONArray().put(Integer.parseInt(item.getString("rank")) - 1, item));
                    } else {
                        level3List.getJSONArray(b.getLevel2()).put(Integer.parseInt(item.getString("rank")) - 1, item);
                    }
                } else if (itemDepth == 2) {
                    if (!level2List.has(b.getLevel1())) {
                        level2List.put(b.getLevel1(), new JSONArray().put(Integer.parseInt(item.getString("rank")) - 1, item));
                    } else {
                        level2List.getJSONArray(b.getLevel1()).put(Integer.parseInt(item.getString("rank")) - 1, item);
                    }
                } else {
                    categoryList.put(Integer.parseInt(b.getRank()) - 1, item);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            position++;
        }

        //정렬된 데이터를 리스트로 만든다.
        for (int i = 0; i < categoryList.length(); i++) {
            try {
                JSONObject item = categoryList.getJSONObject(i);
                JSONArray list = level2List.getJSONArray(item.getString("level1"));
                //중메뉴의 갯수
                int c = list.length();
                for (int j = 0; j < c; j++) {
                    JSONObject sItem = list.getJSONObject(j);
                    sItem.put("list", level3List.getJSONArray(sItem.getString("level2")));
                }
               item.put("list", list);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.d("test", "categoryList : " + categoryList.toString());

    }

    /**
     * check category depth
     *
     * @param bean
     * @return depth 1 : 대, 2 : 중, 3 : 소
     */
    private int getDepth(DataMain.ItemlevelBean bean) {
        if (bean.getLevel3() != null && !bean.getLevel3().equals("") && !bean.getLevel3().equals("*")) {
            return 3;
        } else if (bean.getLevel2() != null && !bean.getLevel2().equals("") && !bean.getLevel2().equals("*")) {
            return 2;
        }
        return 1;

    }

    /**
     * create category item
     *
     * @param beam
     * @param depth
     * @return
     */
    public JSONObject makeCategoryItem(DataMain.ItemlevelBean beam, int depth) {

        try {
            /**
             * level1 : A201
             * level2 : *
             * level3 : *
             * level_name : 패션의류
             * image_url : http://fileserver.gigawon.cn:8588/GigaShopMall/ca02.png
             * rank : 1
             */
            JSONObject item = new JSONObject();
            item.put("level1", beam.getLevel1());
            item.put("level2", beam.getLevel2());
            item.put("level3", beam.getLevel3());
            item.put("level_name", beam.getLevel_name());
            item.put("image_url", beam.getImage_url());
            item.put("rank", beam.getRank());
            if (depth < 3)
                item.put("list", new JSONArray());

            return item;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        }
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH)) {
            index = intent.getIntExtra("index", 0);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    switchPage(index);
                }
            }, 500);
        }
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("종료");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("어플리케이션을 종료 합니다");
        builder.setPositiveButton("종료",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        builder.setNegativeButton("취소",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 2001) {
                //로그인 요청
                int tab = data.getIntExtra("tab", 0);
                switchPage(tab);
            }
        }
    }

    private static final String KEY_NAME = "example_key";
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private Cipher cipher;
    private FingerprintManager.CryptoObject cryptoObject;

    //Cipher Init()
    public boolean cipherInit(){
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }
        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        } catch (Exception e) {
            return false;
        }
    }

    //Key Generator
    @TargetApi(Build.VERSION_CODES.M)//이녀석은 또 처음 보는군.
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | IOException e){
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)//이녀석은 또 처음 보는군.
    public class FingerprintHandler extends FingerprintManager.AuthenticationCallback{
        CancellationSignal cancellationSignal;
        private Context context;

        public FingerprintHandler(Context context){
            this.context = context;
        }

        //메소드들 정의
        public void startAutho(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject){
            cancellationSignal = new CancellationSignal();
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            this.update("인증 에러 발생(" + errString + ")", false);
        }

        @Override
        public void onAuthenticationFailed() {
            this.update("지문인증이 실패되었습니다", false);
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            this.update(helpString.toString(), false);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            this.update("지문인증이 성공되었습니다", true);
        }

        public void stopFingerAuth(){
            if(cancellationSignal != null && !cancellationSignal.isCanceled()){
                cancellationSignal.cancel();
            }
        }

        private void update(String s, boolean b) {
            finger_print_state.setText(s);
            if(b == false){
            } else {//지문인증 성공
                finger_print_state.setTextColor(Color.parseColor("#FF0000"));
                finger_print.setBackgroundResource(R.drawable.ic_fingerprint_success);
                //sound effect
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone((Activity)context, notification);
                r.play();
                showMain();
            }
        }
    }

    public void showMain() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadingArea.setVisibility(View.GONE);
                    Animation anim = AnimationUtils.loadAnimation
                            (getApplicationContext(), // 현재화면 제어권자
                                    R.anim.hide_anim);      // 에니메이션 설정한 파일
                    loadingArea.startAnimation(anim);
                }
            }, 500);
        } catch (Exception e) {
            L.e(e);
        }

    }

    public void showFingerAuth() {
        finger_print_state.setVisibility(View.VISIBLE);
        finger_print.setVisibility(View.VISIBLE);
    }

    public void updateToken(String token) {

        try {
            String custom_code = SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE);

            L.d("updateToken : " + token + " / " + custom_code);

            if (custom_code != null && !custom_code.equals("") && token != null && !token.equals("")) {
                SPUtils.getInstance().put(AppConstants.KEY.PUSH_TOKEN, token);
                String deviceId = custom_code;
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager telephonyManager;
                    telephonyManager = (TelephonyManager) getSystemService(Context.
                            TELEPHONY_SERVICE);
                    deviceId = telephonyManager.getDeviceId();
                }

                HashMap<String, String> params = new HashMap<>();
                params.put("custom_code", custom_code);
                params.put("appl_gubun", "11");
                params.put("device_id", deviceId);
                params.put("token", token);
                params.put("os_gubun", "aos");

                OkHttpHelper okHttpHelper = new OkHttpHelper(this);
                okHttpHelper.addPostRequest(new OkHttpHelper.CallbackLogic() {
                    @Override
                    public void onBizSuccess(String responseDescription, JSONObject data, String flag) {
                        initMainData();
                    }

                    @Override
                    public void onBizFailure(String responseDescription, JSONObject data, String flag) {
                        initMainData();
                    }

                    @Override
                    public void onNetworkError(Request request, IOException e) {
                        initMainData();
                    }
                }, "http://samanager.gigawon.co.kr:8824/api/PushInfo/requestPushInfo", GsonUtils.createGsonString(params));
            } else {
                initMainData();
            }
        } catch (Exception e) {
            L.e(e);
            initMainData();
        }
    }
}
