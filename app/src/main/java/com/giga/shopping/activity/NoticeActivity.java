package com.giga.shopping.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.L;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeActivity extends AppBaseActivity {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.list)
    RecyclerView list;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_notice);
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(linearLayoutManager);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();

        try {

            NoticeListAdapter adapter = new NoticeListAdapter(this, new JSONArray(getIntent().getStringExtra("list")));
            list.setAdapter(adapter);

        } catch (Exception e) {
            L.e(e);
        }
    }

    public class NoticeListAdapter extends RecyclerView.Adapter<NoticeListAdapter.NoticeListViewHolder> {
        private DecimalFormat decimalFormat = new DecimalFormat("#,###");
        private Context mContext;
        private JSONArray list;

        public NoticeListAdapter(Context mContext, JSONArray list) {
            this.mContext = mContext;
            this.list = list;
        }

        public JSONArray getListItem() {
            return this.list;
        }

        @Override
        public NoticeActivity.NoticeListAdapter.NoticeListViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notice, viewGroup, false);
            NoticeListViewHolder viewHolder = new NoticeListViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull NoticeActivity.NoticeListAdapter.NoticeListViewHolder viewHolder, final int position) {

            try {

                JSONObject item = list.getJSONObject(position);
                viewHolder.seq_no.setText(item.getString("seq_no"));
                viewHolder.title.setText(item.getString("title"));
                viewHolder.date.setText(getString(R.string.notice_date) + " : " + item.getString("work_date"));
                viewHolder.content.setText(item.getString("msg"));
                viewHolder.ll_parenttView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            JSONObject item = list.getJSONObject(position);

                            alertShow(item.getString("title"), item.getString("msg"));

                        } catch (Exception e) {
                            Log.e("test", e.getMessage());
                        }
                    }
                });

            } catch (Exception e) {
                L.e(e);
            }

        }

        @Override
        public int getItemCount() {
            return list.length();
        }

        public class NoticeListViewHolder extends RecyclerView.ViewHolder {

            private TextView seq_no, title, date, content;

            private RelativeLayout ll_parenttView;

            public NoticeListViewHolder(View itemView) {
                super(itemView);

                ll_parenttView  = itemView.findViewById(R.id.ll_parenttView);
                seq_no = itemView.findViewById(R.id.seq_no);
                title = itemView.findViewById(R.id.title);
                date = itemView.findViewById(R.id.date);
                content = itemView.findViewById(R.id.content);
            }

        }

        private void alertShow(String title, String msg)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(NoticeActivity.this);
            builder.setTitle(title);
            builder.setMessage(msg);
            builder.setPositiveButton("확인",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        }

    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }
}
