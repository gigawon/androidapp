
package com.giga.shopping.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.giga.library.base.IBaseActivity;
import com.giga.library.util.DeviceUtil;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.http.OkHttpHelper;
import com.giga.shopping.listener.SonicRuntimeImpl;
import com.tencent.sonic.sdk.SonicConfig;
import com.tencent.sonic.sdk.SonicEngine;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Request;
import qiu.niorgai.StatusBarCompat;

public class ConfirmActivity extends AppBaseActivity {

    private DecimalFormat decimalFormat = new DecimalFormat("#,###");

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.address_area)
    RelativeLayout address_area;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView addressTextView;
    @BindView(R.id.mobile_number)
    TextView mobile_number;
    @BindView(R.id.payment_name)
    TextView payment_name;
    @BindView(R.id.product_list)
    TextView product_list;
    @BindView(R.id.payment_info_1)
    TextView payment_info_1;
    @BindView(R.id.payment_btn)
    TextView payment_btn;

    private JSONArray produceList;

    private int deliveryAmount;

    private int totalAmount;

    private String addr_seq_num;

    @Override
    protected void onBefore() {
        super.onBefore();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        if (!SonicEngine.isGetInstanceAllowed()) {
            SonicEngine.createInstance(new SonicRuntimeImpl(getApplication()), new SonicConfig.Builder().build());
        }
    }

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_confirm);
        StatusBarCompat.translucentStatusBar(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        address_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addressIntent = new Intent(ConfirmActivity.this, AllAddressActivity.class);
                addressIntent.putExtra("from","payment");
                startActivityForResult(addressIntent, 1000);
            }
        });

        payment_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                createOrder();
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();

        try {
            produceList = new JSONArray(getIntent().getStringExtra("itemList"));
            String orderName = produceList.getJSONObject(0).getString("item_name");
            if (produceList.length() > 1) {
                orderName = orderName + " 외 " + (produceList.length()-1) + "건";
            }
            product_list.setText(orderName);
            totalAmount = getIntent().getIntExtra("totalAmount", 0);
            deliveryAmount = getIntent().getIntExtra("deliveryAmount", 0);
            payment_info_1.setText("총 결제 금액 " + decimalFormat.format(Double.parseDouble(""+totalAmount)) + getString(R.string.price_unit) + "\n(" +
                    "상품가격 " + decimalFormat.format(Double.parseDouble(""+(totalAmount - deliveryAmount))) + "원 + " +
                    "배송비 " + decimalFormat.format(Double.parseDouble(""+deliveryAmount)) + "원)");
            loadAddress();
        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1000) {
                //주소변경
                name.setText("배송지 " + data.getStringExtra("delivery_name"));
                addressTextView.setText(data.getStringExtra("zip_name") + " " +
                        data.getStringExtra("to_address")
                );
                mobile_number.setText(data.getStringExtra("mobilepho"));
                addr_seq_num = data.getStringExtra("addr_seq_num");
            } else if (requestCode == 1001) {
                //결제완료
                Intent i = new Intent(ConfirmActivity.this, AllOrderActivity.class);
                startActivity(i);
                finish();
            }
        } else {
            if (requestCode == 1001) {
                //결제완료
                finish();
                Toast.makeText(ConfirmActivity.this, "결제가 정상적으로 취소되었습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void loadAddress() {

        try {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_GET_ORDER_ADDRESS)
                    .addParam("myData", "{lang_type:'kor',isnew:'1',pagesize:'10',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "',item_code:'1',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',Isdefault:''}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                JSONArray list = new JSONArray(main.getString("bcm100ts"));
                                JSONObject addressData = new JSONObject();
                                for (int i = 0; i < list.length(); i++) {
                                    addressData = list.getJSONObject(i);
                                    if (addressData.getString("default_add").equals("True")) {
                                        break;
                                    }
                                }
                                name.setText("배송지 " + addressData.getString("delivery_name"));
                                addressTextView.setText(addressData.getString("zip_name") + " " +
                                                addressData.getString("to_address")
                                        );
                                mobile_number.setText(addressData.getString("mobilepho"));
                                addr_seq_num = addressData.getString("seq_num");

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public void createOrder() {

        try {

            JSONArray itemList = new JSONArray();

            for (int i = 0; i < produceList.length(); i++) {
                JSONObject pItem = produceList.getJSONObject(i);
                JSONObject item = new JSONObject();
                item.put("item_code", pItem.getString("item_code"));
                item.put("supply_code", pItem.getString("supply_code"));
                item.put("mall_custom_code", SPUtils.getInstance().getString(AppConstants.KEY.MALL_HOME_ID));
                item.put("sale_q", pItem.getString("sale_q"));
                itemList.put(item);
            }

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_CREATE_ORDER)
                    .addParam("myData", "{lang_type:'kor',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) +
                            "',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) +
                            "',addr_seq_num:'" + addr_seq_num + "',CartMall:" + itemList.toString() + "}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }
                            try {
                                JSONObject main = new JSONObject(response.getD());
                                if (("1").equals(main.getString("status"))) {
                                    String order_num = main.getJSONObject("data").getString("order_num");
                                    checkOrderNumber(order_num);
                                } else {
                                    ToastUtils.showShortToast(main.getString("msg"));
                                }
                            } catch (Exception e) {
                                L.e(e);
                            }

                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }

    public void checkOrderNumber(final String orderNumber) {

        try {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_CHECK_ORDER)
                    .addParam("myData", "{lang_type:'kor',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) +
                            "',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) +
                            "',order_num:'" + orderNumber + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject data = new JSONObject(response.getD());

                                if (("1").equals(data.getString("status"))) {

                                    String total_amt = data.getJSONObject("data").getString("order_o");
                                    String customCode = SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE);

                                    Intent intent = new Intent(ConfirmActivity.this, LGUPlusPayActivity.class);
                                    intent.putExtra("url", "http://211.43.220.140:8080/LgMPay/payment.jsp");
                                    String postdata="CST_PLATFORM=" + AppConstants.PAYMENT.LG_PAY + "&LGD_BUYER=" + customCode + "&LGD_PRODUCTINFO=" + data.getJSONObject("data").getString("item_name") + "&CUSTOM_CODE="+customCode+"&LGD_OID="+orderNumber+"&LGD_AMOUNT="+total_amt + "&LGD_BUYEREMAIL=";
                                    intent.putExtra("postData",postdata);
                                    startActivityForResult(intent, 1001);

                                } else {
                                    ToastUtils.showShortToast(data.getString("msg"));
                                }

                            } catch (Exception e) {
                                L.e(e);
                            }

                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }

    public class ItemView extends LinearLayout {

        public ImageView image;
        public TextView title, price;

        public ItemView(Context context) {
            super(context);

            initView(context);
        }

        public void initView(Context context) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_order, null);

            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);

        }

    }

    public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder> {
        private DecimalFormat decimalFormat = new DecimalFormat("#,###");
        private Context mContext;
        private JSONArray list;

        public OrderListAdapter(Context mContext, JSONArray list) {
            this.mContext = mContext;
            this.list = list;
        }

        public JSONArray getListItem() {
            return this.list;
        }

        @Override
        public OrderListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order, viewGroup, false);
            OrderListViewHolder viewHolder = new OrderListViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull OrderListViewHolder viewHolder, int position) {

            try {

                JSONObject item = list.getJSONObject(position);

                viewHolder.title.setText(item.getString("item_name"));
                viewHolder.price.setText(decimalFormat.format(Double.parseDouble(item.getString("item_p"))) + mContext.getString(R.string.price_unit));
                Glide.with(mContext).load(item.getString("image_url")).into(viewHolder.image);

            } catch (Exception e) {
                L.e(e);
            }

        }

        @Override
        public int getItemCount() {
            return list.length();
        }

        public class OrderListViewHolder extends RecyclerView.ViewHolder {

            private ImageView image;
            private TextView title, price;

            public OrderListViewHolder(View itemView) {
                super(itemView);
                image = itemView.findViewById(R.id.image);
                title = itemView.findViewById(R.id.title);
                price = itemView.findViewById(R.id.price);
            }
        }

    }

}
