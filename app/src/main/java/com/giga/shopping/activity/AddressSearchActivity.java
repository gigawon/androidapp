package com.giga.shopping.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.giga.library.util.L;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.AddressSearchAdapter;
import com.giga.shopping.adapter.SearchAdapter;
import com.giga.shopping.bean.AddressSearchDetail;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ProductDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.EndlessRecyclerOnScrollListener;
import com.giga.shopping.widget.LoadMoreWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;


public class AddressSearchActivity extends AppBaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.address_search)
    EditText address_search;
    @BindView(R.id.search_btn)
    TextView search_btn;

    private LoadMoreWrapper loadMoreWrapper;
    private List<AddressSearchDetail> addressList = new ArrayList<>();
    private AddressSearchAdapter loadMoreWrapperAdapter;
    private int pg          = 1;
    private int tot_page    = 1;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_address_search);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();

        setSwipeColor(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        loadMoreWrapperAdapter = new AddressSearchAdapter(this, addressList);
        loadMoreWrapperAdapter.setItemClickListener(new AddressSearchAdapter.SearchListener() {
            @Override
            public void onItemClick(int position) {

                AddressSearchDetail item = addressList.get(position);

                Intent intent = getIntent();
                intent.putExtra("zipName", item.getNewZipName());
                intent.putExtra("zipCode", item.getZipCode());

                setResult(RESULT_OK, intent);
                finish();
            }
        });
        loadMoreWrapper = new LoadMoreWrapper(loadMoreWrapperAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(loadMoreWrapper);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING);

                if (pg < tot_page) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadList();
                                }
                            });
                        }
                    }, 1000);
                } else {
                    loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_END);
                }
            }
        });
    }

    private void addData(JSONArray list) {

        for (int i = 0; i < list.length(); i++) {
            try {
                JSONObject item = list.getJSONObject(i);
                AddressSearchDetail address = new AddressSearchDetail();
                address.setZipCode(item.getJSONObject("postcd").getString("cdatasection"));
                address.setNewZipName(item.getJSONObject("address").getString("cdatasection"));
                address.setLastZipName(item.getJSONObject("addrjibun").getString("cdatasection"));
                addressList.add(address);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    private void loadList() {

        try {

            String key = address_search.getText().toString();
            if (key == null || key.equals("")) {
                Toast.makeText(AddressSearchActivity.this, "검색어를 입력하세요!", Toast.LENGTH_SHORT).show();
                return;
            }

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_ADDRESS_LIST)
                    .addParam("myData", "{key:'" + key + "',pg:'" + pg + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_COMPLETE);
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                JSONArray list = main.getJSONObject("post").getJSONObject("itemlist").getJSONArray("item");
                                pg = pg + 1;
                                tot_page = Integer.parseInt(main.getJSONObject("post").getJSONObject("pageinfo").getJSONObject("totalCount").getString("cdatasection"));

                                addData(list);
                                loadMoreWrapperAdapter.notifyDataSetChanged();
                                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_COMPLETE);

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    protected void initData() {
        super.initData();
        address_search.setText("오성빌딩");
    }


    @OnClick({R.id.back_btn, R.id.search_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.search_btn:
                pg = 1;
                addressList.clear();
                loadList();
                break;

        }
    }

    @Override
    public void onRefresh() {
        initData();
        stopRefreshing();
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

}
