package com.giga.shopping.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.L;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.SearchAdapter;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ProductDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.listener.EndlessRecyclerOnScrollListener;
import com.giga.shopping.widget.LoadMoreWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;


public class SearchGoodsActivity extends AppBaseActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.arrow_back)
    RelativeLayout arrowBack;
    @BindView(R.id.iv_gradient_search)
    ImageView ivGradientSearch;
    @BindView(R.id.tv_gradient_search)
    TextView tvGradientSearch;
    @BindView(R.id.ll_gradient_search_bg)
    LinearLayout llGradientSearchBg;
    @BindView(R.id.type_recy)
    ImageView typeRecy;
    @BindView(R.id.tab_text_1)
    TextView tabText1;
    @BindView(R.id.tab_1)
    RelativeLayout tab1;
    @BindView(R.id.tab_text_2)
    TextView tabText2;
    @BindView(R.id.tab_2)
    RelativeLayout tab2;
    @BindView(R.id.tab_text_3)
    TextView tabText3;
    @BindView(R.id.tab_3)
    RelativeLayout tab3;
    @BindView(R.id.tab_text_4)
    TextView tabText4;
    @BindView(R.id.tab_4)
    RelativeLayout tab4;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    public int itemCount = 1;
    @BindView(R.id.tab_img_1)
    ImageView tabImg1;
    @BindView(R.id.tab_img_2)
    ImageView tabImg2;
    @BindView(R.id.tab_img_3)
    ImageView tabImg3;
    @BindView(R.id.tab_img_4)
    ImageView tabImg4;

    View selectView;

    private LoadMoreWrapper loadMoreWrapper;
    private List<ProductDetail> productList = new ArrayList<>();
    private SearchAdapter loadMoreWrapperAdapter;
    private boolean isHigh = false;
    private PopupWindow window;

    private String itemCode;
    private int pg          = 1;
    private int tot_page    = 1;

    private boolean isSearch = false;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_search_goods);
        ButterKnife.bind(this);
    }

    private void switchIcon() {
        if (itemCount == 1) {
            typeRecy.setImageResource(R.mipmap.icon_list);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            itemCount = 2;
        } else {
            typeRecy.setImageResource(R.mipmap.icon_pic);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            itemCount = 1;
        }
        recyclerView.setAdapter(loadMoreWrapper);
    }

    @Override
    protected void initView() {
        super.initView();

        typeRecy.setImageResource(R.mipmap.icon_pic);
        setSwipeColor(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        tabText1.setTextColor(getResources().getColor(R.color.colorPrimary));
        tabImg1.setImageResource(R.mipmap.icon_pulldown_s);
        tabImg2.setImageResource(R.mipmap.icon_pulldown_n);
        tabImg3.setImageResource(R.mipmap.icon_price_arrow);
        tabImg4.setImageResource(R.mipmap.icon_pulldown_n);
        selectView = findViewById(R.id.root_pop);
        loadMoreWrapperAdapter = new SearchAdapter(this, productList);
        loadMoreWrapperAdapter.setItemClickListener(new SearchAdapter.SearchListener() {
            @Override
            public void onItemClick(int position) {

                ProductDetail item = productList.get(position);

                Intent intent = new Intent();
                intent.setClass(SearchGoodsActivity.this,GoodsDetailActivity.class);
                intent.putExtra("itemCode", item.getItem_code());
                intent.putExtra("supplyCode", item.getCustom_code());
                startActivity(intent);
            }
        });
        loadMoreWrapper = new LoadMoreWrapper(loadMoreWrapperAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(loadMoreWrapper);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING);

                if (pg < tot_page) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadList();
                                }
                            });
                        }
                    }, 1000);
                } else {
                    loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_END);
                }
            }
        });

        itemCode = getIntent().getStringExtra("itemCode");

        if ("search".equals(itemCode)) {

        }

    }

    private void addData(JSONArray list) {

        for (int i = 0; i < list.length(); i++) {

            try {

                JSONObject item = list.getJSONObject(i);

                ProductDetail product = new ProductDetail();
                if (item.has("ad_image"))
                    product.setAd_image(item.getString("ad_image"));
                if (item.has("ad_title"))
                    product.setAd_title(item.getString("ad_title"));
                if (item.has("custom_code"))
                    product.setCustom_code(item.getString("custom_code"));
                if (item.has("item_code"))
                    product.setItem_code(item.getString("item_code"));
                if (item.has("item_img_url"))
                    product.setItem_img_url(item.getString("item_img_url"));
                if (item.has("item_name"))
                    product.setItem_name(item.getString("item_name"));
                if (item.has("item_p"))
                    product.setItem_p(item.getString("item_p"));
                if (item.has("link_url"))
                    product.setLink_url(item.getString("link_url"));
                if (item.has("market_p"))
                    product.setMarket_p(item.getString("market_p"));
                if (item.has("sub_title"))
                    product.setSub_title(item.getString("sub_title"));
                productList.add(product);

            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    private void loadList() {

        if (isSearch == false) {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_GET_CATEGORY_LIST)
                    .addParam("myData", "{lang_type:'kor',div_code:'2',user_id:'',token:'',cateevent1:'" + itemCode + "',pg:'" + pg + "',pagesize:'20'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                JSONArray list = main.getJSONArray("eventtype21");
                                pg = Integer.parseInt(main.getString("pg")) + 1;
                                tot_page = Integer.parseInt(main.getString("tot_page"));

                                addData(list);

                                loadMoreWrapperAdapter.notifyDataSetChanged();

                                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_COMPLETE);

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } else {
            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_GET_SEARCH_LIST)
                    .addParam("myData", "{lang_type:'kor',orderBy:'',priceOrderBy:'',searchType:'1',searchWordSub:'',searchPriceFr:'',searchPriceTo:'',div_code:'2',user_id:'',token:'',pg:'" + pg + "',pagesize:'20',searchWord:'" + tvGradientSearch.getText().toString() + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                JSONArray list = main.getJSONArray("itemsSearch");
                                pg = Integer.parseInt(main.getString("pg")) + 1;
                                tot_page = Integer.parseInt(main.getString("tot_page"));

                                addData(list);

                                loadMoreWrapperAdapter.notifyDataSetChanged();

                                loadMoreWrapper.setLoadState(loadMoreWrapper.LOADING_COMPLETE);

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });
        }
    }

    @Override
    protected void initData() {
        super.initData();
        if ("search".equals(itemCode)) {
            Intent intent = new Intent();
            intent.setClass(SearchGoodsActivity.this, SeachLevelActivity.class);
            startActivityForResult(intent, 1001, true);
        } else
            loadList();
    }


    @OnClick({R.id.arrow_back, R.id.ll_gradient_search_bg, R.id.type_recy, R.id.tab_1, R.id.tab_2, R.id.tab_3, R.id.tab_4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.arrow_back:
                finish();
                break;
            case R.id.ll_gradient_search_bg:
                Intent intent = new Intent();
                intent.setClass(SearchGoodsActivity.this, SeachLevelActivity.class);
                startActivityForResult(intent, 1000, true);
                break;
            case R.id.type_recy:
                switchIcon();
                break;
            case R.id.tab_1:
                tabText1.setTextColor(getResources().getColor(R.color.colorPrimary));
                tabText2.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabText3.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabText4.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabImg1.setImageResource(R.mipmap.icon_pulldown_s);
                tabImg2.setImageResource(R.mipmap.icon_pulldown_n);
                tabImg3.setImageResource(R.mipmap.icon_price_arrow);
                tabImg4.setImageResource(R.mipmap.icon_pulldown_n);
                break;
            case R.id.tab_2:
                tabText1.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabText2.setTextColor(getResources().getColor(R.color.colorPrimary));
                tabText3.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabText4.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabImg1.setImageResource(R.mipmap.icon_pulldown_n);
                tabImg2.setImageResource(R.mipmap.icon_pulldown_s);
                tabImg3.setImageResource(R.mipmap.icon_price_arrow);
                tabImg4.setImageResource(R.mipmap.icon_pulldown_n);
                break;
            case R.id.tab_3:
                if (isHigh) {
                    tabImg3.setImageResource(R.mipmap.icon_price_high);
                    isHigh = false;
                } else {
                    tabImg3.setImageResource(R.mipmap.icon_price_low);
                    isHigh = true;
                }
                tabText1.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabText2.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabText3.setTextColor(getResources().getColor(R.color.colorPrimary));
                tabText4.setTextColor(getResources().getColor(R.color.textcolor_black));
                tabImg1.setImageResource(R.mipmap.icon_pulldown_n);
                tabImg2.setImageResource(R.mipmap.icon_pulldown_n);
                tabImg4.setImageResource(R.mipmap.icon_price_arrow);
                break;
            case R.id.tab_4:
                showPricePopup();
                break;
        }
    }

    private void showPricePopup() {
        // 用于PopupWindow的View
        View contentView = LayoutInflater.from(this).inflate(R.layout.popup_search_price, null, false);
        // 创建PopupWindow对象，其中：
        // 第一个参数是用于PopupWindow中的View，第二个参数是PopupWindow的宽度，
        // 第三个参数是PopupWindow的高度，第四个参数指定PopupWindow能否获得焦点
        window = new PopupWindow(contentView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
        // 设置PopupWindow的背景
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // 设置PopupWindow是否能响应外部点击事件
        window.setOutsideTouchable(true);
        // 设置PopupWindow是否能响应点击事件
        window.setTouchable(true);
        // 显示PopupWindow，其中：
        // 第一个参数是PopupWindow的锚点，第二和第三个参数分别是PopupWindow相对锚点的x、y偏移

        EditText low = contentView.findViewById(R.id.low_price);
        EditText high = contentView.findViewById(R.id.high_price);
        TextView done = contentView.findViewById(R.id.price_choose);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                window.dismiss();
            }
        });

        window.showAsDropDown(selectView, 0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isSearch = false;

        if (resultCode == RESULT_OK) {
            if (requestCode == 1000 || requestCode == 1001) {
                if (data == null) {
                    if (requestCode == 1001)
                        finish();
                    else
                        return;
                }
                doSearchResult(data);
                String result = data.getStringExtra("searchkey");
                if (EmptyUtils.isNotEmpty(result)) {
                    productList.clear();
                    loadMoreWrapperAdapter.notifyDataSetChanged();
                    tvGradientSearch.setText(result);
                    isSearch = true;
                    pg = 1;
                    loadList();
                }
            }
        } else {
            if (requestCode == 1001) {
                finish();
            }
        }
    }

    @Override
    public void onRefresh() {
        initData();
        stopRefreshing();
    }

    public int getType() {
        return itemCount;
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH))
            finish();
    }

    public void doSearchResult(Intent data) {
        String result = data.getStringExtra("searchkey");
        if (EmptyUtils.isNotEmpty(result)) {
            productList.clear();
            loadMoreWrapperAdapter.notifyDataSetChanged();
            tvGradientSearch.setText(result);
            isSearch = true;
            pg = 1;
            loadList();
        }
    }

}
