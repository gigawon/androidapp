package com.giga.shopping.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

public class AllAddressActivity extends IBaseActivity {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.list)
    RecyclerView list;

    private String from;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_address);
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(linearLayoutManager);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();

        from = getIntent().getStringExtra("from");
    }

    public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.OrderListViewHolder> {
        private DecimalFormat decimalFormat = new DecimalFormat("#,###");
        private Context mContext;
        private JSONArray list;

        public AddressListAdapter(Context mContext, JSONArray list) {
            this.mContext = mContext;
            this.list = list;
        }

        public void setListItem(JSONArray list) {this.list = list;}

        public JSONArray getListItem() {
            return this.list;
        }

        @Override
        public AllAddressActivity.AddressListAdapter.OrderListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address, viewGroup, false);
            OrderListViewHolder viewHolder = new OrderListViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull AllAddressActivity.AddressListAdapter.OrderListViewHolder viewHolder, int position) {

            try {

//                {
//                    "d": "{\"bcm100ts\":[
//                      {\"seq_num\":\"569\",\"delivery_name\":\"기가코리아\",\"to_address\":\"우성빌딩 8층\",\"zip_code\":\"07344 \",\"zip_name\":\" 서울특별시 영등포구 63로 7 (여의도동, 은하아파트)\",\"mobilepho\":\"13371169420\",\"latitude\":\"37.4346680\",\"longitude\":\"122.1607420\",\"default_add\":\"True\",\"rum\":\"1\",\"num\":\"1\"}],\"status\":\"1\",\"flag\":\"1501\",\"msg\":\"작업완료\",\"start_page\":\"1\",\"end_page\":\"1\",\"pg\":\"1\",\"tot_page\":\"1\",\"tot_cnt\":\"1\"}"
//                }

                JSONObject item = list.getJSONObject(position);

                String default_add = item.getString("default_add");

                viewHolder.name.setText(item.getString("delivery_name"));
                viewHolder.state.setText(default_add.equals("True")? "기본배송지":"");
                viewHolder.address.setText(item.getString("zip_name") + " " + item.getString("to_address"));
                viewHolder.mobile_number.setText("휴대전화번호 " + item.getString("mobilepho"));
                viewHolder.itemView.setTag(position);

                viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(AllAddressActivity.this);
                        builder.setTitle("배송지 삭제");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("배송지를 삭제합니다");
                        builder.setPositiveButton("삭제",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        try {

                                            JSONObject item = list.getJSONObject(position);

                                            new HttpHelper.Create(AllAddressActivity.this)
                                                    .setUrl(AppConstants.URL.MALL_BASEURL)
                                                    .service(AppConstants.SERVICE.REQUEST_ADDRESS_DELETE)
                                                    .addParam("lang_type", "kor")
                                                    .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                                                    .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                                                    .addParam("del_seq_no", item.getString("seq_num"))
                                                    .execute(new ICallBack<Object>() {

                                                        @Override
                                                        public void onBefore(Request request, int id) {
                                                            super.onBefore(request, id);
                                                            showProgressDialog();
                                                        }

                                                        @Override
                                                        public void onAfter(int id) {
                                                            super.onAfter(id);
                                                            hideProgressDialog();
                                                        }

                                                        @Override
                                                        public void onError(Call call, Exception e, int id) {
                                                            ToastUtils.showShortToast(e.getMessage());
                                                        }

                                                        @Override
                                                        public void onResponse(Object response, int id) {
                                                            try {
                                                                if (response == null) {
                                                                    return;
                                                                }
                                                                JSONObject result = new JSONObject(response.toString());
                                                                Toast.makeText(AllAddressActivity.this, result.getString("msg"), Toast.LENGTH_SHORT).show();
                                                                loadData();
                                                            } catch (Exception e) {
                                                                L.e(e);
                                                            }
                                                        }
                                                    });

                                        } catch (Exception e) {
                                            L.e(e);
                                        }

                                    }
                                });
                        builder.setNegativeButton("취소",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();

                        return false;
                    }
                });

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            JSONObject item = list.getJSONObject(position);

                            if (from != null && from.equals("payment")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(AllAddressActivity.this);
                                builder.setTitle("선택");
                                builder.setIcon(R.mipmap.ic_launcher);
                                builder.setMessage("선텍하신 주소를 사용하시겠습니까?");
                                builder.setPositiveButton("사용",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                try {

                                                    Intent resultIntent = new Intent();
                                                    resultIntent.putExtra("delivery_name", item.getString("delivery_name"));
                                                    resultIntent.putExtra("zip_name", item.getString("zip_name"));
                                                    resultIntent.putExtra("to_address", item.getString("to_address"));
                                                    resultIntent.putExtra("mobilepho", item.getString("mobilepho"));
                                                    resultIntent.putExtra("addr_seq_num", item.getString("seq_num"));
                                                    setResult(RESULT_OK, resultIntent);

                                                } catch (Exception e) {
                                                    L.e(e);
                                                }

                                                finish();
                                            }
                                        });
                                builder.setNegativeButton("편집",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                try {

                                                    Intent i = new Intent(AllAddressActivity.this, AddressEditActivity.class);
                                                    i.putExtra("addressName", item.getString("delivery_name"));
                                                    i.putExtra("addressZipName", item.getString("zip_name"));
                                                    i.putExtra("addressZipDetail", item.getString("to_address"));
                                                    i.putExtra("addressMobile", item.getString("mobilepho"));
                                                    i.putExtra("addressMsg", item.getString("remark"));
                                                    i.putExtra("seq_num", item.getString("seq_num"));
                                                    i.putExtra("zip_code", item.getString("zip_code"));
                                                    i.putExtra("default_add", item.getString("default_add").equals("True"));
                                                    startActivity(i);

                                                } catch (Exception e) {
                                                    L.e(e);
                                                }
                                            }
                                        });
                                builder.show();

                            } else {

                                Intent i = new Intent(AllAddressActivity.this, AddressEditActivity.class);
                                i.putExtra("addressName", item.getString("delivery_name"));
                                i.putExtra("addressZipName", item.getString("zip_name"));
                                i.putExtra("addressZipDetail", item.getString("to_address"));
                                i.putExtra("addressMobile", item.getString("mobilepho"));
                                i.putExtra("addressMsg", item.getString("remark"));
                                i.putExtra("seq_num", item.getString("seq_num"));
                                i.putExtra("zip_code", item.getString("zip_code"));
                                i.putExtra("default_add", item.getString("default_add").equals("True"));
                                startActivity(i);

                            }

                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                });

            } catch (Exception e) {
                L.e(e);
            }

        }

        @Override
        public int getItemCount() {
            return list.length();
        }

        public class OrderListViewHolder extends RecyclerView.ViewHolder {

            private TextView name, state, address, mobile_number;

            public OrderListViewHolder(View itemView) {
                super(itemView);

                name = itemView.findViewById(R.id.name);
                state = itemView.findViewById(R.id.state);
                address = itemView.findViewById(R.id.address);
                mobile_number = itemView.findViewById(R.id.mobile_number);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    public void loadData() {
        try {

//            {
//                'myData': "{\"lang_type\": \"kor\",\"custom_code\": \"0102109209162c04\",\"token\": \"0102109209162c040729f189-39c4-47a0-b5ff-c5613beb1666\",\"item_code\": \"1\",\"pagesize\": \"10\",\"isnew\":\"\",\"Isdefault\": \"\",}"
//            }

            new HttpHelper.Create(AllAddressActivity.this)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_GET_ORDER_ADDRESS)
                    .addParam("myData", "{lang_type:'kor',item_code:'1',pagesize:'10',isnew:'',Isdefault:'',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                //"d": "{\"total_Count\":0,\"total_Page\":0,\"order_c\":0,\"order_pay\":0,\"order_Distribution\":0,\"order_Express\":0,\"order_cancel\":0,\"status\":2,\"msg\":\"用户不存在\",\"data\":null}"

                                JSONObject main = new JSONObject(response.getD());


                                AddressListAdapter adapter = new AddressListAdapter(AllAddressActivity.this, main.getJSONArray("bcm100ts"));
                                list.setAdapter(adapter);

                            } catch (Exception e) {
                                L.e(e);
                            }

                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }
    }

    @OnClick({R.id.add_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_btn:
                startActivity(new Intent(AllAddressActivity.this, AddressEditActivity.class));
                break;

        }
    }
}
