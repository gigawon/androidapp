package com.giga.shopping.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.SPUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.fragment.ClassifyFragment;
import com.giga.shopping.fragment.HomePageFragment;
import com.giga.shopping.fragment.MineFragment;
import com.giga.shopping.fragment.ShoppingCartFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GSMainActivity extends AppBaseActivity {

    @BindView(R.id.pageImage1)
    ImageView pageImage1;
    @BindView(R.id.pageText1)
    TextView pageText1;
    @BindView(R.id.page1)
    RelativeLayout page1;
    @BindView(R.id.pageImage2)
    ImageView pageImage2;
    @BindView(R.id.pageText2)
    TextView pageText2;
    @BindView(R.id.page2)
    RelativeLayout page2;
    @BindView(R.id.pageImage3)
    ImageView pageImage3;
    @BindView(R.id.pageText3)
    TextView pageText3;
    @BindView(R.id.page3)
    RelativeLayout page3;
    @BindView(R.id.pageImage4)
    ImageView pageImage4;
    @BindView(R.id.pageText4)
    TextView pageText4;
    @BindView(R.id.page4) RelativeLayout page4;

    @BindView(R.id.menuBar) LinearLayout menuBar;
    private int index = 0;
    private ImageView[] pageImages;
    private TextView[] pageTexts;
    private Fragment[] fragments;
    private HomePageFragment homePageFragment;
    private ClassifyFragment classifyFragment;
    private ShoppingCartFragment cartFragment;
    private MineFragment mineFragment;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_gsmain);
        ButterKnife.bind(this);
    }

    @Override
    protected void initView() {
        super.initView();
        pageImages = new ImageView[]{pageImage1, pageImage2, pageImage3,pageImage4};
        pageTexts = new TextView[]{pageText1, pageText2, pageText3,pageText4};
        homePageFragment = new HomePageFragment();
        classifyFragment = new ClassifyFragment();
        cartFragment = new ShoppingCartFragment();
        mineFragment = new MineFragment();
        fragments = new Fragment[]{homePageFragment,classifyFragment, cartFragment,mineFragment};
        switchPage(index);
    }

    @OnClick({R.id.page1, R.id.page2, R.id.page3, R.id.page4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.page1:
                switchPage(0);
                break;
            case R.id.page2:
                switchPage(1);
                break;
            case R.id.page3:
                switchPage(2);
                break;
            case R.id.page4:
                if ("1".equals(SPUtils.getInstance().getString(AppConstants.KEY.LOGIN_STATUS)))
                    switchPage(3);
                else
                    startActivity(new Intent(GSMainActivity.this, LoginActivity.class));
                break;
        }
    }

    private void switchPage(int selected) {
        for (int i = 0; i < pageImages.length; i++) {
            if (i == selected) {
                pageImages[i].setSelected(true);
                pageTexts[i].setSelected(true);
                pageTexts[i].getPaint().setFakeBoldText(true);
            } else {
                pageImages[i].setSelected(false);
                pageTexts[i].setSelected(false);
                pageTexts[i].getPaint().setFakeBoldText(false);
            }
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragments[index].isAdded()) {
            transaction.hide(fragments[index]);
        }
        if (fragments[selected].isAdded()) {
            transaction.show(fragments[selected]);
        } else {
            transaction.add(R.id.content, fragments[selected]);
        }
        transaction.commit();
        index = selected;
    }

    @Override
    public void onBriadcastReceive(Intent intent) {
        if (intent.getAction().equals(AppConstants.INTENT_FILTER.INTENT_FINISH)) {
            index = intent.getIntExtra("index", 0);
            switchPage(index);
        }
    }
}
