package com.giga.shopping.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.giga.library.base.IBaseActivity;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.bean.ReturnBean;
import com.giga.shopping.bean.ReturnInfoBean;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Request;

public class ReturnActivity extends IBaseActivity {

    @BindView(R.id.back_btn)
    RelativeLayout back_btn;
    @BindView(R.id.list)
    RecyclerView recyclerView;

    private String order_num;

    private String ReturnReason = "";

    private String ReturnReasonRemark = "";

    private ReturnInfoListAdapter adapter;

    @Override
    protected void initialize(Bundle savedInstanceState) {
        setContentView(R.layout.activity_return);
    }

    @Override
    protected void initView() {
        super.initView();
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    protected void initData() {
        super.initData();

        Intent getIntent = getIntent();

        order_num         = getIntent.getStringExtra("order_num");

        getReturnCode();

    }

    @OnClick({R.id.return_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.return_btn:
                if (ReturnReason == null || ReturnReason.equals("")) {
                    Toast.makeText(ReturnActivity.this, "반품사유를 선택해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (ReturnReason.equals("99") && ReturnReasonRemark.equals("")) {
                    Toast.makeText(ReturnActivity.this, "기타 반품사유를 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }

                doReturn(ReturnReason, ReturnReasonRemark);
                break;
        }
    }

    public void doReturn(String ReturnReason, String ReturnReasonRemark) {

        try {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.MALL_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_RETURN_ORDER)
                    .addParam("lang_type", "kor")
                    .addParam("token", SPUtils.getInstance().getString(AppConstants.KEY.TOKEN))
                    .addParam("custom_code", SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE))
                    .addParam("order_num", order_num)
                    .addParam("ReturnReason", ReturnReason)
                    .addParam("ReturnReasonRemark", ReturnReasonRemark)
                    .execute(new ICallBack<ReturnBean>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(ReturnBean response, int id) {

                            try {
                                if (response == null) {
                                    return;
                                }

                                Toast.makeText(ReturnActivity.this, response.getMsg(), Toast.LENGTH_SHORT).show();
                                if (response.getStatus().equals("1")) {
                                    setResult(RESULT_OK);
                                    finish();
                                }

                            } catch (Exception e) {
                                L.e(e);
                            }


                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public void getReturnCode() {

        try {

            new HttpHelper.Create(this)
                    .setUrl(AppConstants.URL.SAM_URL)
                    .service(AppConstants.SERVICE.REQUEST_GET_RETURN_CODE)
                    .addParam("lang_type", "kor")
                    .addParam("main_code", "B055")
                    .execute(new ICallBack<ReturnInfoBean>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(ReturnInfoBean response, int id) {

                            try {
                                if (response == null) {
                                    return;
                                }

                                if (response.getStatus().equals("1")) {
                                    List<LinkedTreeMap<Object,Object>> list = response.getData();
                                    JSONArray jArr = new JSONArray();
                                    for (int i = 0; i < list.size(); i++) {
                                        LinkedTreeMap<Object,Object> l = list.get(i);
                                        JSONObject j = new JSONObject();
                                        j.put("sub_code", l.get("sub_code").toString());
                                        j.put("code_name", l.get("code_name").toString());
                                        jArr.put(j);
                                    }

                                    adapter = new ReturnInfoListAdapter(ReturnActivity.this, jArr);
                                    recyclerView.setAdapter(adapter);

                                }

                            } catch (Exception e) {
                                L.e(e);
                            }


                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public class ReturnInfoListAdapter extends RecyclerView.Adapter<ReturnInfoListAdapter.ReturnInfoListViewHolder> {
        private Context mContext;
        private JSONArray list;

        public ReturnInfoListAdapter(Context mContext, JSONArray list) {
            this.mContext = mContext;
            this.list = list;
        }

        public JSONArray getListItem() {
            return this.list;
        }

        @Override
        public ReturnInfoListViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_return, viewGroup, false);
            ReturnInfoListViewHolder viewHolder = new ReturnInfoListViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ReturnInfoListViewHolder viewHolder, final int position) {

            try {

                JSONObject item = list.getJSONObject(position);
                String sub_code = item.getString("sub_code");
                viewHolder.title.setText(item.getString("code_name"));
                viewHolder.ll_parenttView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            ReturnReason = sub_code;
                            adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            Log.e("test", e.getMessage());
                        }
                    }
                });

                if (sub_code.equals(ReturnReason)) {
                    viewHolder.check_box_img.setImageResource(R.mipmap.icon_cart_selected);
                    if (sub_code.equals("99")) {
                        viewHolder.remark.setVisibility(View.VISIBLE);
                        if (ReturnReasonRemark != null && !ReturnReasonRemark.equals(""))
                            viewHolder.remark.setText(ReturnReasonRemark);
                        viewHolder.remark.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                ReturnReasonRemark = s.toString();
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });
                    } else {
                        viewHolder.remark.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.check_box_img.setImageResource(R.mipmap.icon_cart_unselected);
                    viewHolder.remark.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                L.e(e);
            }

        }

        @Override
        public int getItemCount() {
            return list.length();
        }

        public class ReturnInfoListViewHolder extends RecyclerView.ViewHolder {

            private ImageView check_box_img;

            private TextView title, remark;

            private LinearLayout ll_parenttView;

            public ReturnInfoListViewHolder(View itemView) {
                super(itemView);

                ll_parenttView  = itemView.findViewById(R.id.ll_parenttView);
                title = itemView.findViewById(R.id.title);
                remark = itemView.findViewById(R.id.remark);
                check_box_img = itemView.findViewById(R.id.check_box_img);
            }

        }

    }

}
