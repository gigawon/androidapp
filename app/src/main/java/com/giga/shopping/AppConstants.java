package com.giga.shopping;

import java.util.HashMap;

/**
 * Created by LaMitad on 2017/1/19.
 */

public class AppConstants {

    public interface PAYMENT {
        String LG_PAY = "service";
    }

    public interface URL {

        String MALL_BASEURL            = "http://mall.gigawon.co.kr:8800/";
        String WWW_BASEURL             = "http://www.gigawon.co.kr:8800/";
        String REGIST_URL              = "http://join.gigaroom.com/10_Member/join_main";
        String MYSHOP_URL              = "http://myshop.gigawon.co.kr:522/";
        String SAM_URL                 = "http://samanager.gigawon.co.kr:8824/";

    }

    public interface SERVICE {

        String REQUEST_GET_MAIN                 = "api/Main/requestGetMain";

        String REQUEST_GET_CATEGORY_LIST        = "00_Init/RequestApi.aspx/request1DepthSubMain";

        String REQUEST_GET_SEARCH_LIST          = "00_Init/RequestApi.aspx/requestItemsSearch";

        String REQUEST_GOODS_DETAIL             = "00_Init/RequestApi.aspx/requestGoodsdetail";

        String REQUEST_CART_INFO                = "00_Init/RequestApi.aspx/GetTopInfo";

        String REQUEST_ADD_CART                 = "00_Init/RequestApi.aspx/CarOrderAdd";

        String REQUEST_ADD_CART_MALL_ORDER      = "00_Init/RequestAPI.aspx/CarMallOrderAdd";

        String REQUEST_LOGIN                    = "00_Init/RequestApi.aspx/requestLoginCheck";

        String REQUEST_LOGOUT                   = "00_Init/RequestApi.aspx/requestLogout";

        String REQUEST_CART_LIST                = "00_Init/RequestApi.aspx/GetcarMallOrderList";

        String REQUEST_DELETE_CART              = "00_init/RequestAPI.aspx/CarMallOrderDel";

        String REQUEST_UPDATE_CART              = "00_Init/RequestAPI.aspx/CarMallOrderEdit";

        String REQUEST_MY_ORDER_COUNT           = "00_Init/httpWebReuqestAPI.aspx/QueryMyShopMain";

        String REQUEST_CHECK_ORDER              = "00_Init/RequestAPI.aspx/requestPaymentBeforeOrderNumInfo";

        String REQUEST_GET_ORDER_ADDRESS        = "00_Init/RequestApi.aspx/GetReceivingAddressList";

        String REQUEST_GET_ORDER_LIST           = "00_Init/httpWebReuqestAPI.aspx/requestMyOrderList";

        String REQUEST_CREATE_ORDER             = "00_Init/RequestAPI.aspx/requestCartMallCreateOrder";

        String REQUEST_NOTICE_LIST              = "00_Init/httpWebReuqestAPI.aspx/requestNoticeList";

        String REQUEST_ADDRESS_LIST             = "00_Init/RequestAPI.aspx/GetKorAddressList";

        String REQUEST_ADDRESS_ADD              = "api/ReceivingAddress/RecAddressAdd";

        String REQUEST_ADDRESS_EDIT             = "api/ReceivingAddress/RecAddresEdit";

        String REQUEST_ADDRESS_DELETE           = "api/ReceivingAddress/RecAddressDel";

        String REQUEST_COMMENT_LIST             = "api/Assess/requestItemAssessList";

        String REQUEST_COMMENT_CREATE           = "api/Assess/requestAssessAdd";

        String REQUEST_COMMENT_EDIT             = "api/Assess/requestAssessEdit";

        String REQUEST_COMMENT_GET              = "api/Assess/requestAppMyShopAssess";

        String REQUEST_RETURN_ORDER             = "api/Order/requestSetMallReturnReceipt";

        String REQUEST_CANCEL_ORDER             = "api/Order/requestSetMallCancel";

        String REQUEST_GET_RETURN_CODE          = "api/apiSaCommon/requestBaseInfoSearch";

        String REQUEST_CONFIRM_ORDER            = "api/MyShop/requestReceiptConfirmation";

        String getmodels            = "getmodels";//初始化模块
        String loginMember          = "api/Login/requestLoginCheck";//登录
    }

    public interface KEY {

        HashMap<String, String> INTERFACE_PARAMS = new HashMap<String, String>();

        String LOGIN_STATUS             = "loginStatus";

        String ID                       = "ID";
        String TAG                      = "TAG";
        String KEY_JSON_FM_ORDERSTATUS = "orderStatus";
        String KEY_JSON_FM_STATUS       = "status";
        String MESSAGE                  = "MESSAGE";
        String KEY_SHARED_KNICK_NAME    = "shared_knick_name";
        String USERNAME                 = "USERNAME";
        String KEY_REQUEST_MEMBER_ID    = "memid";
        String TOKEN                    = "token";
        String VALUE_RESPONSE_SUCCESS   = "success";
        String KEY_RESPONSE_FLAG        = "flag";
        String KEY_SHARED_ICON_PATH     = "shared_icon_path";
        String LANG_TYPE                = "kor";
        String KEY_SP_HISTORYKEY        = "historyKey";
        String KEY_SP_HISTORYKEY_level1 = "historyKey_level1";
        String CUSTOM_CODE              = "custom_code";
        String CUSTOM_ID                = "custom_id";
        String CUSTOM_NAME              = "custom_name";
        String NICK_NAME                = "nick_name";
        String PROFILE_IMG              = "profile_img";
        String DIV_CODE                 = "div_code";
        String SSOID                    = "sso_regiKey";
        String SSO_REGIKEY              = "sso_regiKey";
        String MALL_HOME_ID             = "mall_home_id";
        String POINT_CART_NO            = "point_card_no";
        String PARENT_ID                = "parent_id";
        String BUSINESS_TYPE            = "business_type";
        String MINFO                    = "minfo";
        String ITEM_CODE                = "item_code";
        String COUNT                    = "count";
        String UNIT_PRICE               = "unit_price";
        String APP_DATA                 = "app_data";
        String CUSTOMER_DATA            = "customer_data";
        String PUSH_TOKEN           = "push_token";

        //카테고리 레밸
        String KEY_LEVEL1               = "level1";
        String KEY_LEVEL2               = "level2";
        String KEY_LEVEL3               = "level3";
        String KEY_LEVEL_NAME           = "level_name";
        String KEY_IMG_URL              = "image_url";
        String KEY_RANK                 = "rank";
    }

    public interface VALUES {
        String mobile           = "mobile";
        String password         = "password";
        String type             = "type";

    }

    public interface ENCRYPT_TYPE {
        String MD5 = "md5";
        String DES = "des";
        String NOCRYPT = "nocrypt";
    }

    public interface ACTION {
        String RECEIVE_CLIENT_ID = "RECEIVE_CLIENT_ID";
        String HOME_BACK = "HOME_BACK";
        String CHANGE_PHONE_SUCCESS = "CHANGE_PHONE_SUCCESS";
        String PAY_SUCCESS = "PAY_SUCCESS";
    }

    public interface CODE_TYPE {
        String LOGIN = "1";
        String REGISTER = "2";
    }

    public interface MODEL_ID {

    }

    public interface INTENT_FILTER {
        String INTENT_FINISH = "intent_finish";
    }

    public interface WEB {

        //
        String SCHEME = "giga";

        //detail of product
        String HOST_CALL_PRODUCT_DETAIL = "showProductDetail";

        //show category
        String HOST_CALL_CATEGORY = "showCategory";

        //call search
        String HOST_CALL_SEARCH = "showSearch";

        //call search
        String HOST_CALL_JAVA_SCRIPT = "callScript";

        //page load
        String HOST_LOAD_URL = "loadUrl";

        //call login
        String HOST_DO_LOGIN = "doLogin";

        //call logout
        String HOST_DO_LOGOUT = "doLogout";

        //call search
        String HOST_FINISH = "close";

        //call logout
        String HOST_SET_RESOURCE = "setResource";

        //category no
        String CATEGORY_NO = "categoryNo";

        //product no
        String PRODUCT_NO = "productNo";

        //product no
        String SUPPLY_CODE = "supplyCode";

        //script name
        String NAME = "name";

        //script value
        String VALUES = "values";

        //script value
        String VALUE = "value";

        //url
        String URL = "url";

    }

}
