package com.giga.shopping.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.base.IBaseFragment;
import com.giga.library.util.DeviceUtil;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.activity.GoodsDetailActivity;
import com.giga.shopping.activity.LoginActivity;
import com.giga.shopping.activity.MainActivity;
import com.giga.shopping.activity.SearchGoodsActivity;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;

import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Request;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.giga.shopping.AppConstants.WEB.CATEGORY_NO;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_CATEGORY;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_JAVA_SCRIPT;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_PRODUCT_DETAIL;
import static com.giga.shopping.AppConstants.WEB.HOST_CALL_SEARCH;
import static com.giga.shopping.AppConstants.WEB.HOST_DO_LOGIN;
import static com.giga.shopping.AppConstants.WEB.HOST_DO_LOGOUT;
import static com.giga.shopping.AppConstants.WEB.HOST_LOAD_URL;
import static com.giga.shopping.AppConstants.WEB.HOST_SET_RESOURCE;
import static com.giga.shopping.AppConstants.WEB.NAME;
import static com.giga.shopping.AppConstants.WEB.PRODUCT_NO;
import static com.giga.shopping.AppConstants.WEB.SCHEME;
import static com.giga.shopping.AppConstants.WEB.SUPPLY_CODE;
import static com.giga.shopping.AppConstants.WEB.URL;
import static com.giga.shopping.AppConstants.WEB.VALUE;
import static com.giga.shopping.AppConstants.WEB.VALUES;

public class HomePageFragment extends IBaseFragment {

    public static boolean isSuccessLoad = true;

    private MainActivity mainActivity;

    @BindView(R.id.web_view)
    WebView webView;

    @BindView(R.id.error_view)
    LinearLayout error_view;

    @BindView(R.id.reload_btn)
    TextView reload_btn;

    @BindView(R.id.home_circle_btn)
    LinearLayout home_circle_btn;

    @BindView(R.id.reload_circle_btn)
    LinearLayout reload_circle_btn;

    @BindView(R.id.back_circle_btn)
    LinearLayout back_circle_btn;

    @BindView(R.id.forward_circle_btn)
    LinearLayout forward_circle_btn;

    @BindView(R.id.search_circle_btn)
    LinearLayout search_circle_btn;

    @BindView(R.id.touch_area)
    RelativeLayout touch_area;

    Unbinder unbinder;

    @Override
    protected void initialize() {
        setContentView(R.layout.fragment_home_page);

    }

    @Override
    protected void initView() {
        super.initView();
        unbinder = ButterKnife.bind(this, layout);

        try {

            mainActivity = (MainActivity)getActivity();
            //webView.clearCache(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.getSettings().setDisplayZoomControls(false);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webView.getSettings().setSupportMultipleWindows(true);
            webView.setEnabled(false);
            touch_area.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    timerCount = 5;
                    if (reload_circle_btn.getVisibility() == View.GONE) {
                        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.show_anim);
                        home_circle_btn.setVisibility(View.VISIBLE);
                        home_circle_btn.startAnimation(anim);
                        reload_circle_btn.setVisibility(View.VISIBLE);
                        reload_circle_btn.startAnimation(anim);
                        back_circle_btn.setVisibility(View.VISIBLE);
                        back_circle_btn.startAnimation(anim);
                        forward_circle_btn.setVisibility(View.VISIBLE);
                        forward_circle_btn.startAnimation(anim);
                        search_circle_btn.setVisibility(View.VISIBLE);
                        search_circle_btn.startAnimation(anim);
                        timerHandler.postDelayed(timerRunnable, 1000);
                    }
                    return false;
                }
            });

            home_circle_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goHome();
                }
            });

            reload_circle_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    webView.reload();
                }
            });

            back_circle_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (webView.canGoBack())
                        webView.goBack();
                }
            });

            forward_circle_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (webView.canGoForward())
                        webView.goForward();
                }
            });

            search_circle_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(activity, SearchGoodsActivity.class);
                    i.putExtra("itemCode", "search");
                    startActivity(i);
                }
            });

            // 이전 버전에서 적용되지 않는다
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
                webView.getSettings().setMixedContentMode(
                        WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                    return super.onJsAlert(view, url, message, result);
                }


            });

            webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    // TODO Auto-generated method stub

                    try {

                        L.d("shouldOverrideUrlLoading : " + url);

                        Uri uri = Uri.parse(url);
                        String scheme = uri.getScheme();
                        String host = uri.getHost();

                        L.d("shouldOverrideUrlLoading scheme : " + scheme);
                        L.d("shouldOverrideUrlLoading host : " + host);

                        if (scheme.equals(SCHEME)) {

                            if (host.equals(HOST_CALL_PRODUCT_DETAIL)) {

                                //giga://showProductDetail?supplyCode=0&productNo=0

                                String supplyCode = uri.getQueryParameter(SUPPLY_CODE);
                                String productNo = uri.getQueryParameter(PRODUCT_NO);

                                Intent intent = new Intent();
                                intent.setClass(activity, GoodsDetailActivity.class);
                                intent.putExtra("itemCode", productNo);
                                intent.putExtra("supplyCode", supplyCode);
                                startActivity(intent);

                            } else if (host.equals(HOST_CALL_CATEGORY)) {

                                //giga://showCategory?categoryNo=0

                                String categoryNo = uri.getQueryParameter(CATEGORY_NO);

                                Intent intent = new Intent();
                                intent.setClass(activity,SearchGoodsActivity.class);
                                intent.putExtra("itemCode", categoryNo);
                                startActivity(intent);

                            } else if (host.equals(HOST_CALL_SEARCH)) {

                                //giga://showSearch
                                Intent intent = new Intent();
                                intent.setClass(activity,SearchGoodsActivity.class);
                                intent.putExtra("itemCode", "search");
                                startActivity(intent);

                            } else if (host.equals(HOST_CALL_JAVA_SCRIPT)) {

                                //giga://callScript

                                String scriptName   = uri.getQueryParameter(NAME);
                                String values       = uri.getQueryParameter(VALUES);
                                String[] params = values.split("\\|");
                                String javaScript = "javascript:" + scriptName + "(";

                                String strData = SPUtils.getInstance().getString(AppConstants.KEY.APP_DATA);
                                if (strData == null || strData.equals(""))
                                    strData = "{}";
                                JSONObject appData = new JSONObject(strData);

                                for (int i = 0; i < params.length; i++) {
                                    if (i > 0) javaScript = javaScript + ",";
                                    javaScript = javaScript + "'" + (appData.has(params[i])?appData.getString(params[i]):"") + "'";
                                }
                                javaScript = javaScript + ")";
                                view.loadUrl(javaScript);

                            } else if (host.equals(HOST_LOAD_URL)) {

                                //giga://loadUrl
                                String tagetUrl   = uri.getQueryParameter(URL);
                                view.loadUrl(tagetUrl);

                            } else if (host.equals(HOST_DO_LOGIN)) {

                                //giga://doLogin?name=name&values=value|value

                                String scriptName   = uri.getQueryParameter(NAME);
                                String values       = uri.getQueryParameter(VALUES);

                                Intent i = new Intent(activity, LoginActivity.class);
                                i.putExtra("from", "web");
                                i.putExtra("scriptName", scriptName);
                                i.putExtra("values", values);
                                startActivityForResult(i, 2000);

                            } else if (host.equals(HOST_DO_LOGOUT)) {

                                //giga://doLOgout
                                String targetUrl = uri.getQueryParameter(URL);
                                doLogout(targetUrl);

                            } else if (host.equals(HOST_SET_RESOURCE)) {

                                //giga://setResource
                                String name = uri.getQueryParameter(NAME);
                                String value = uri.getQueryParameter(VALUE);
                                SPUtils.getInstance().put(name, value);

                                String strData = SPUtils.getInstance().getString(AppConstants.KEY.APP_DATA);
                                if (strData == null || strData.equals(""))
                                    strData = "{}";
                                JSONObject appData = new JSONObject(strData);
                                appData.put(name, value);
                                SPUtils.getInstance().put(AppConstants.KEY.APP_DATA, appData.toString());

                            }
                            return true;
                        }
                    } catch (Exception e) {
                        L.e(e);
                        return true;
                    }
                    return super.shouldOverrideUrlLoading(view, url);
                }

                @Override
                public void onPageStarted(final WebView view, String url, Bitmap favicon) {
                    // TODO Auto-generated method stub
                    showProgressDialog();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    // TODO Auto-generated method stub
                    hideProgressDialog();
                    isSuccessLoad = true;
                }

                @Override
                public void onReceivedError(final WebView view, int errorCode, String description, String failingUrl) {
                    super.onReceivedError(view, errorCode, description, failingUrl);
                    isSuccessLoad = false;
//                    error_view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onReceivedSslError(final WebView view, SslErrorHandler handler, SslError error) {
                    super.onReceivedSslError(view, handler, error);
                    isSuccessLoad = false;
                }

                @Override
                public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                    super.onReceivedHttpError(view, request, errorResponse);
//                    final int statusCode;
//
//                    if (Build.VERSION.SDK_INT < 21) {
//                        statusCode = 1000;
//                    } else {
//                        statusCode = errorResponse.getStatusCode();
//                    }
//
//                    L.d("[onReceivedHttpError]" + statusCode);
//
//                    if (statusCode == 404) {
//                        isSuccessLoad = false;
//                        error_view.setVisibility(View.VISIBLE);
//                    }
                }
            });

            initLoad();

        } catch (Exception e) {
            L.e(e);
        }

    }

    @Override
    protected void initData() {
        super.initData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.show_anim);
        home_circle_btn.setVisibility(View.VISIBLE);
        home_circle_btn.startAnimation(anim);
        reload_circle_btn.setVisibility(View.VISIBLE);
        reload_circle_btn.startAnimation(anim);
        back_circle_btn.setVisibility(View.VISIBLE);
        back_circle_btn.startAnimation(anim);
        forward_circle_btn.setVisibility(View.VISIBLE);
        forward_circle_btn.startAnimation(anim);
        search_circle_btn.setVisibility(View.VISIBLE);
        search_circle_btn.startAnimation(anim);
        timerHandler.postDelayed(timerRunnable, 1000);
    }

    @Override
    public void onPause() {
        super.onPause();
        reload_circle_btn.setVisibility(View.GONE);
        timerHandler.removeCallbacks(timerRunnable);
    }

    public void initLoad() {
        error_view.setVisibility(View.GONE);
        goHome();
    }

    @OnClick({R.id.reload_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.reload_btn:
                initLoad();
                break;
        }
    }

    public void doLogout(String url) {

        try {

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_LOGOUT)
                    .addParam("myData", "{lang_type:'kor',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',deviceNo:'" + DeviceUtil.getDeviceId(activity) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));
                                SPUtils.getInstance().put(AppConstants.KEY.LOGIN_STATUS, "");
                                SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_CODE, "");
                                SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_ID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_NAME, "");
                                SPUtils.getInstance().put(AppConstants.KEY.NICK_NAME, "");
                                SPUtils.getInstance().put(AppConstants.KEY.PROFILE_IMG, "");
                                SPUtils.getInstance().put(AppConstants.KEY.TOKEN, "");
                                SPUtils.getInstance().put(AppConstants.KEY.DIV_CODE, "");
                                SPUtils.getInstance().put(AppConstants.KEY.SSOID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.SSO_REGIKEY, "");
                                SPUtils.getInstance().put(AppConstants.KEY.MALL_HOME_ID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.POINT_CART_NO, "");
                                SPUtils.getInstance().put(AppConstants.KEY.PARENT_ID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.BUSINESS_TYPE, "");
                                SPUtils.getInstance().put(AppConstants.KEY.MINFO, "");

                            } catch (Exception e) {
                                L.e(e);
                                SPUtils.getInstance().put(AppConstants.KEY.LOGIN_STATUS, "0");
                            }

                            if (url != null && !url.equals("") && url.startsWith("http"))
                                webView.loadUrl(url);
                            else
                                goHome();
                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 2000) {
                    //로그인 요청
                    String scriptName = data.getStringExtra("scriptName");
                    String values = data.getStringExtra("values");
                    if (scriptName == null || scriptName.equals(""))
                        goHome();
                    else {
                        String[] params = values.split("\\|");
                        String javaScript = "javascript:" + scriptName + "(";
                        String strData = SPUtils.getInstance().getString(AppConstants.KEY.APP_DATA);
                        if (strData == null || strData.equals(""))
                            strData = "{}";
                        JSONObject appData = new JSONObject(strData);

                        for (int i = 0; i < params.length; i++) {
                            if (i > 0) javaScript = javaScript + ",";
                            javaScript = javaScript + "'" + (appData.has(params[i])?appData.getString(params[i]):"") + "'";
                        }
                        javaScript = javaScript + ")";
                        webView.loadUrl(javaScript);
                    }
                }
            }

            if (resultCode == RESULT_CANCELED) {
                if (requestCode == 2000) {
                    //로그인 요청
                    goHome();
                }
            }
        } catch (Exception e) {
            L.e(e);
        }
    }

    private int timerCount = 5;
    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {

            if (timerCount < 1) {
                Animation anim = AnimationUtils.loadAnimation(activity, R.anim.hide_anim);
                home_circle_btn.setVisibility(View.GONE);
                home_circle_btn.startAnimation(anim);
                reload_circle_btn.setVisibility(View.GONE);
                reload_circle_btn.startAnimation(anim);
                back_circle_btn.setVisibility(View.GONE);
                back_circle_btn.startAnimation(anim);
                forward_circle_btn.setVisibility(View.GONE);
                forward_circle_btn.startAnimation(anim);
                search_circle_btn.setVisibility(View.GONE);
                search_circle_btn.startAnimation(anim);
                timerHandler.removeCallbacks(timerRunnable);
                return;
            }
            timerCount--;
            timerHandler.postDelayed(timerRunnable, 1000);
        }
    };

    public void goHome() {

        if (MineFragment.isClearWebCache == true) {
            clearApplicationData();
        }

        String url = "http://m.gigawon.co.kr/gateToMain.aspx?lang_type=kor&custom_code=" +
                SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "&mall_home_id=" +
                SPUtils.getInstance().getString(AppConstants.KEY.MALL_HOME_ID) + "&token=" +
                SPUtils.getInstance().getString(AppConstants.KEY.TOKEN);
        webView.loadUrl(url);
    }

    public void clearApplicationData() {

        webView.clearCache(true);
        webView.clearHistory();
        MineFragment.isClearWebCache = false;

        File cache = activity.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    L.d("**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            L.d("Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else
        {
            L.d("Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(activity);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }

    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
}
