package com.giga.shopping.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.library.base.IBaseFragment;
import com.giga.library.util.BarUtils;
import com.giga.library.util.DeviceUtil;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.activity.AllAddressActivity;
import com.giga.shopping.activity.AllOrderActivity;
import com.giga.shopping.activity.MainActivity;
import com.giga.shopping.activity.NoticeActivity;
import com.giga.shopping.activity.ReturnActivity;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Request;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

public class MineFragment extends IBaseFragment {

    public static boolean isClearWebCache = false;

    @BindView(R.id.head_img)
    CircleImageView headImg;
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.phone_num)
    TextView phoneNum;
    @BindView(R.id.setting_info)
    TextView settingInfo;
    @BindView(R.id.look_all)
    LinearLayout lookAll;
    @BindView(R.id.order_1)
    LinearLayout order1;
    @BindView(R.id.order_2)
    LinearLayout order2;
    @BindView(R.id.order_3)
    LinearLayout order3;
    @BindView(R.id.order_4)
    LinearLayout order4;
    @BindView(R.id.order_5)
    LinearLayout order5;
    @BindView(R.id.service_1)
    LinearLayout service1;
    @BindView(R.id.service_2)
    LinearLayout service2;
    @BindView(R.id.service_3)
    LinearLayout service3;
    @BindView(R.id.service_4)
    LinearLayout service4;
    Unbinder unbinder;

    private Badge badge1, badge2, badge3, badge4, badge5;

    private String allOrderList;

    private String noticeList;

    @Override
    protected void initialize() {
        setContentView(R.layout.fragment_mine);
    }

    @Override
    protected void initView() {
        super.initView();
        unbinder = ButterKnife.bind(this, layout);
        BarUtils.setColor(activity, getResColor(com.giga.library.R.color.colorPrimary), 50);

        badge1 = new QBadgeView(activity).bindTarget(order1);
        badge1.setBadgeGravity(Gravity.END | Gravity.TOP);
        badge2 = new QBadgeView(activity).bindTarget(order2);
        badge2.setBadgeGravity(Gravity.END | Gravity.TOP);
        badge3 = new QBadgeView(activity).bindTarget(order3);
        badge3.setBadgeGravity(Gravity.END | Gravity.TOP);
        badge4 = new QBadgeView(activity).bindTarget(order4);
        badge4.setBadgeGravity(Gravity.END | Gravity.TOP);
        badge5 = new QBadgeView(activity).bindTarget(order5);
        badge5.setBadgeGravity(Gravity.END | Gravity.TOP);
    }

    @Override
    protected void initData() {
        super.initData();

        loadData();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.setting_info, R.id.look_all, R.id.order_1, R.id.order_2, R.id.order_3, R.id.order_4, R.id.order_5, R.id.service_1, R.id.service_2, R.id.service_3, R.id.service_4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting_info:
                AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
                builder1.setTitle("로그아웃");
                builder1.setIcon(R.mipmap.ic_launcher);
                builder1.setMessage("로그아웃 하시겠습니까?");
                builder1.setPositiveButton("로그아웃",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                doLogout();
                            }
                        });
                builder1.setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder1.show();
                break;
            case R.id.look_all:
                Intent i = new Intent(activity, AllOrderActivity.class);
                i.putExtra("list", allOrderList);
                startActivity(i);
                break;
            case R.id.order_1:
                break;
            case R.id.order_2:
                break;
            case R.id.order_3:
                break;
            case R.id.order_4:
                break;
            case R.id.order_5:
                break;
            case R.id.service_1:
                Intent n = new Intent(activity, NoticeActivity.class);
                n.putExtra("list", noticeList);
                startActivity(n);
                break;
            case R.id.service_2:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(activity);
                builder2.setTitle("고객센터");
                builder2.setIcon(R.mipmap.ic_launcher);
                builder2.setMessage("고객센터에 연결됩니다.\n" +
                        "1644-0411");
                builder2.setPositiveButton("연결",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                String tel = "tel:16440411";
                                startActivity(new Intent("android.intent.action.CALL", Uri.parse(tel)));
                            }
                        });
                builder2.setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder2.show();

                break;
            case R.id.service_3:
                Intent a = new Intent(activity, AllAddressActivity.class);
                startActivity(a);
                break;
            case R.id.service_4:
//                boolean isExist = false;
//
//                PackageManager pkgMgr = activity.getPackageManager();
//                List<ResolveInfo> mApps;
//                Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
//                mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//                mApps = pkgMgr.queryIntentActivities(mainIntent, 0);
//
//                try {
//                    for (int m = 0; m < mApps.size(); m++) {
//                        if(mApps.get(m).activityInfo.packageName.startsWith("com.giga.messenger")){
//                            isExist = true;
//                            break;
//                        }
//                    }
//                }
//                catch (Exception e) {
//                    isExist = false;
//                }
//                if(isExist) {
//                    Intent intent = activity.getPackageManager().getLaunchIntentForPackage("com.giga.messenger");
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                } else {
//                    String url = "market://details?id=" + "com.giga.messenger";
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                    startActivity(intent);
//                }
                break;
        }
    }

    public void doLogout() {

        try {

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_LOGOUT)
                    .addParam("myData", "{lang_type:'kor',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',deviceNo:'" + DeviceUtil.getDeviceId(activity) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));
                                SPUtils.getInstance().put(AppConstants.KEY.LOGIN_STATUS, "");
                                SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_CODE, "");
                                SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_ID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.CUSTOM_NAME, "");
                                SPUtils.getInstance().put(AppConstants.KEY.NICK_NAME, "");
                                SPUtils.getInstance().put(AppConstants.KEY.PROFILE_IMG, "");
                                SPUtils.getInstance().put(AppConstants.KEY.TOKEN, "");
                                SPUtils.getInstance().put(AppConstants.KEY.DIV_CODE, "");
                                SPUtils.getInstance().put(AppConstants.KEY.SSOID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.SSO_REGIKEY, "");
                                SPUtils.getInstance().put(AppConstants.KEY.MALL_HOME_ID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.POINT_CART_NO, "");
                                SPUtils.getInstance().put(AppConstants.KEY.PARENT_ID, "");
                                SPUtils.getInstance().put(AppConstants.KEY.BUSINESS_TYPE, "");
                                SPUtils.getInstance().put(AppConstants.KEY.MINFO, "");
                                SPUtils.getInstance().put(AppConstants.KEY.APP_DATA, "{}");

                            } catch (Exception e) {
                                L.e(e);
                                SPUtils.getInstance().put(AppConstants.KEY.LOGIN_STATUS, "0");
                            }

                            //캐쉬 삭제
                            isClearWebCache = true;

                            //회면 다시 갱신 요청
                            HomePageFragment.isSuccessLoad = false;
                            ((MainActivity)activity).switchPage(0);

                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }

    public void loadData() {
        getAccountInfo();
        getOrderCount();
    }

    public void getAccountInfo() {
        textName.setText(SPUtils.getInstance().getString(AppConstants.KEY.NICK_NAME));
        phoneNum.setText(" " + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_ID));
        Glide.with(activity).load(SPUtils.getInstance().getString(AppConstants.KEY.PROFILE_IMG)).into(headImg);
    }

    public void getOrderCount() {

        try {

            //myData	String	{custom_code:'0102109209162c04',lang_type:'kor',sDate:'',eDate:'',token:'0102109209162c04341e4057-a539-44a9-80c2-b0e97d4d9b6e',page:'1',pageSize:'4'}

            //"d": "{\"total_Count\":5,\"total_Page\":1,\"order_c\":5,\"order_pay\":0,\"order_Distribution\":0,\"order_Express\":0,\"order_cancel\":0,\"status\":1,\"msg\":\"Success\",\"data\":[{\"ORDER_DATE\":\"20190119\",\"ORDER_NUM\":\"1021901190000000003W\",\"ITEM_NAME\":\"[Dr.EM]닥터이엠 헤어토닉...2个/总共2个\",\"ORDER_O\":\"61,830\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"61,830\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190119\",\"ORDER_NUM\":\"1021901190000000001W\",\"ITEM_NAME\":\"[카이젤]3in1 여성전용 멀...1个/总共1个\",\"ORDER_O\":\"35,100\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"35,100\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190118\",\"ORDER_NUM\":\"1021901180000000003W\",\"ITEM_NAME\":\"[Dr.EM]닥터이엠 헤어토닉...2个/总共2个\",\"ORDER_O\":\"61,830\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"61,830\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190118\",\"ORDER_NUM\":\"1021901180000000002W\",\"ITEM_NAME\":\"[Dr.EM]닥터이엠 헤어토닉...2个/总共2个\",\"ORDER_O\":\"61,830\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"61,830\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190118\",\"ORDER_NUM\":\"1021901180000000001W\",\"ITEM_NAME\":\"[CANZ]  차량용 공기청정...1个/总共1个\",\"ORDER_O\":\"53,100\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"53,100\",\"ORDER_STATUS\":\"상품주문\"}]}"

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.MYSHOP_URL)
                    .service(AppConstants.SERVICE.REQUEST_MY_ORDER_COUNT)
                    .addParam("myData", "{sDate:'',eDate:'',page:'1',pageSize:'1',lang_type:'kor',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                //"d": "{\"total_Count\":0,\"total_Page\":0,\"order_c\":0,\"order_pay\":0,\"order_Distribution\":0,\"order_Express\":0,\"order_cancel\":0,\"status\":2,\"msg\":\"用户不存在\",\"data\":null}"

                                JSONObject main = new JSONObject(response.getD());

                                allOrderList = main.getString("data");

                                int order_c = Integer.parseInt(main.getString("order_c"));
                                int order_pay = Integer.parseInt(main.getString("order_pay"));
                                int order_Distribution = Integer.parseInt(main.getString("order_Distribution"));
                                int order_Express = Integer.parseInt(main.getString("order_Express"));
                                int order_cancel = Integer.parseInt(main.getString("order_cancel"));

                                badge1.setBadgeNumber(order_c);
                                badge2.setBadgeNumber(order_pay);
                                badge3.setBadgeNumber(order_Distribution);
                                badge4.setBadgeNumber(order_Express);
                                badge5.setBadgeNumber(order_cancel);

                                getNotice();

                            } catch (Exception e) {
                                L.e(e);
                            }

                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }

    public void getNotice() {

        try {

            //myData	String	{custom_code:'0102109209162c04',lang_type:'kor',sDate:'',eDate:'',token:'0102109209162c04341e4057-a539-44a9-80c2-b0e97d4d9b6e',page:'1',pageSize:'4'}

            //"d": "{\"total_Count\":5,\"total_Page\":1,\"order_c\":5,\"order_pay\":0,\"order_Distribution\":0,\"order_Express\":0,\"order_cancel\":0,\"status\":1,\"msg\":\"Success\",\"data\":[{\"ORDER_DATE\":\"20190119\",\"ORDER_NUM\":\"1021901190000000003W\",\"ITEM_NAME\":\"[Dr.EM]닥터이엠 헤어토닉...2个/总共2个\",\"ORDER_O\":\"61,830\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"61,830\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190119\",\"ORDER_NUM\":\"1021901190000000001W\",\"ITEM_NAME\":\"[카이젤]3in1 여성전용 멀...1个/总共1个\",\"ORDER_O\":\"35,100\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"35,100\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190118\",\"ORDER_NUM\":\"1021901180000000003W\",\"ITEM_NAME\":\"[Dr.EM]닥터이엠 헤어토닉...2个/总共2个\",\"ORDER_O\":\"61,830\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"61,830\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190118\",\"ORDER_NUM\":\"1021901180000000002W\",\"ITEM_NAME\":\"[Dr.EM]닥터이엠 헤어토닉...2个/总共2个\",\"ORDER_O\":\"61,830\",\"ORDER_Q\":\"2\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"61,830\",\"ORDER_STATUS\":\"상품주문\"},{\"ORDER_DATE\":\"20190118\",\"ORDER_NUM\":\"1021901180000000001W\",\"ITEM_NAME\":\"[CANZ]  차량용 공기청정...1个/总共1个\",\"ORDER_O\":\"53,100\",\"ORDER_Q\":\"1\",\"EXPRESS_O\":\"0\",\"PAYMENT_O\":\"53,100\",\"ORDER_STATUS\":\"상품주문\"}]}"

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.MYSHOP_URL)
                    .service(AppConstants.SERVICE.REQUEST_NOTICE_LIST)
                    .addParam("myData", "{page:'1',pageSize:'5',lang_type:'kor',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());

                                noticeList = main.getString("data");

                            } catch (Exception e) {
                                L.e(e);
                            }

                        }
                    });

        } catch (Exception e) {
            L.e(e);
            ToastUtils.showShortToast(e.getMessage());
        }

    }

}
