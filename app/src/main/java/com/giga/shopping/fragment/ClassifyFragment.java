package com.giga.shopping.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.giga.library.base.IBaseFragment;
import com.giga.shopping.MyApplication;
import com.giga.shopping.R;
import com.giga.shopping.activity.MainActivity;
import com.giga.shopping.activity.SearchGoodsActivity;
import com.giga.shopping.adapter.LefterAdapter;
import com.giga.shopping.adapter.RightAdapter;
import com.giga.shopping.bean.Classify;
import com.giga.shopping.widget.DividerItemDecorationNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ClassifyFragment extends IBaseFragment {

    private MainActivity mainActivity;

    @BindView(R.id.recycler_left)
    RecyclerView leftRecyclerView;
    @BindView(R.id.recycler_right)
    RecyclerView rightRecyclerView;
    @BindView(R.id.gridview)
    GridView gridview;
    Unbinder unbinder;
    private LefterAdapter lefterAdapter;
    private RightAdapter rightAdapter;
    private RecyclerView.LayoutManager leftLayoutManager;
    private RecyclerView.LayoutManager rightLayoutManager;
    List<Classify> classifyList = new ArrayList<>();
    List<Classify> classifyList2 = new ArrayList<>();
    List<Classify.ListBeanX> listBeanXList = new ArrayList<>();
    List<Classify.ListBeanX> listBeanXListAll = new ArrayList<>();
    private List<String> bigSortList = new ArrayList<>();
    private List<String> smallSortList = new ArrayList<>();
    private List<String> ssList = new ArrayList<>();

    private HashMap<Integer, Integer> iconList = new HashMap<Integer, Integer>();

    @Override
    protected void initialize() {

        setContentView(R.layout.fragment_classify);
    }

    @Override
    protected void initView() {
        super.initView();
        unbinder = ButterKnife.bind(this, layout);

        iconList.put(0, R.mipmap.icon_category_clothing);
        iconList.put(1, R.mipmap.icon_category_bags);
        iconList.put(2, R.mipmap.icon_category_baby);
        iconList.put(3, R.mipmap.icon_category_fruits);
        iconList.put(4, R.mipmap.icon_category_commodity);
        iconList.put(5, R.mipmap.icon_category_furniture);
        iconList.put(6, R.mipmap.icon_category_commodity);
        iconList.put(7, R.mipmap.icon_category_more);
        iconList.put(8, R.mipmap.icon_category_sports);
        iconList.put(9, R.mipmap.icon_category_more);
        iconList.put(10, R.mipmap.icon_category_house_e);
        iconList.put(11, R.mipmap.icon_category_electronics);
        iconList.put(12, R.mipmap.icon_category_electronics);

        mainActivity = (MainActivity) getActivity();

        leftLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        leftRecyclerView.setLayoutManager(leftLayoutManager);
        lefterAdapter = new LefterAdapter(activity, classifyList);

        rightLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rightRecyclerView.setLayoutManager(rightLayoutManager);
        rightRecyclerView.addItemDecoration(new DividerItemDecorationNew(activity,
                LinearLayoutManager.HORIZONTAL, R.drawable.divide_bg_10));
        rightAdapter = new RightAdapter(activity, listBeanXListAll);
        //左侧列表的点击事件
        lefterAdapter.setItemClickListener(new LefterAdapter.LeftListener() {
            @Override
            public void onItemClick(int position) {
                //向适配器中返回点击的位置，在适配器中进行操作
                selectCategory(position);
            }
        });
        leftRecyclerView.setAdapter(lefterAdapter);
        rightRecyclerView.setAdapter(rightAdapter);

        gridview.setAdapter(new MenuGridAdapter(classifyList));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(activity,SearchGoodsActivity.class);
                intent.putExtra("itemCode", classifyList.get(position).getLevel1());
                startActivity(intent);
            }
        });

    }

    @Override
    protected void initData() {
        super.initData();
        JSONArray json = ((MainActivity) activity).getCategoryList();
        for (int i = 0; i < json.length(); i++) {
            try {
                JSONObject myjObject = json.getJSONObject(i);
                Classify classify = MyApplication.getInstance().gson.fromJson(myjObject.toString(), Classify.class);
                classifyList.add(classify);
                classifyList2.add(classify);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (classifyList2.size() > 0) {
            listBeanXList = classifyList2.get(0).getList();
        }
        listBeanXListAll.addAll(listBeanXList);

    }

    public void selectCategory(int id) {
        lefterAdapter.getSelectedPosition(id);
        listBeanXList = classifyList2.get(id).getList();
        if (listBeanXListAll.size() > 0) {
            listBeanXListAll.clear();
        }
        listBeanXListAll.addAll(listBeanXList);
        rightAdapter.getSelectedPosition(id);
        rightAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public class MenuGridAdapter extends BaseAdapter {

        public int selectedPosition = 0;

        List<Classify> listItems;

        public MenuGridAdapter(List<Classify> listItems) {
            this.listItems = listItems;
        }

        @Override
        public int getCount() {
            return listItems.size();
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_list_item, parent, false);

            ImageView icon = view.findViewById(R.id.icon);
            View line = view.findViewById(R.id.line);
            TextView title = view.findViewById(R.id.title);
            title.setText(listItems.get(position).getLevel_name());
            Glide.with(activity).load(listItems.get(position).getImage_url()).into(icon);
            //icon.setBackgroundResource(iconList.get(position));
            if (selectedPosition == position) {
                title.setTextColor(Color.parseColor("#000000"));
//                line.setVisibility(View.VISIBLE);
            } else {
                title.setTextColor(Color.parseColor("#696969"));
//                line.setVisibility(View.GONE);
            }
            return view;
        }

    }

}
