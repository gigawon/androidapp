package com.giga.shopping.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.giga.library.base.IBaseFragment;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.activity.ConfirmActivity;
import com.giga.shopping.activity.MainActivity;
import com.giga.shopping.adapter.MyCartAdapter;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Request;

public class CartFragment extends IBaseFragment {

    private DecimalFormat decimalFormat = new DecimalFormat("#,###");

    Unbinder unbinder;

    @BindView(R.id.recyclerView_cart)
    RecyclerView recyclerViewCart;

    @BindView(R.id.total_amount)
    TextView totalAmount;

    @BindView(R.id.all_select)
    CheckBox allSelect;

    @BindView(R.id.all_select_txt)
    TextView all_select_txt;

    @BindView(R.id.go_settlement)
    TextView goSettlement;

    private MyCartAdapter myCartAdapter;
    private int pg = 1;
    private int totalPage = 2;

    @Override
    protected void initialize() {
        setContentView(R.layout.fragment_shopping_cart);
    }

    @Override
    protected void initView() {
        super.initView();

        unbinder = ButterKnife.bind(this, layout);

        try {
            L.d("initView");

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
            recyclerViewCart.setLayoutManager(linearLayoutManager);
            myCartAdapter = new MyCartAdapter(activity, new ArrayList<JSONObject>());
            myCartAdapter.setItemClickListener(new MyCartAdapter.ListControllListener() {
                @Override
                public void onCheckBoxClick(int position) {
                    try {
                        String checked = (myCartAdapter.getListItem().get(position)).getString("checked").equals("1") ? "0" : "1";
                        (myCartAdapter.getListItem().get(position)).put("checked", checked);
                        myCartAdapter.notifyDataSetChanged();

                        if ("0".equals(checked)) {
                            allSelect.setChecked(false);
                        } else {
                            boolean check = true;
                            for (int i = 0; i < myCartAdapter.getItemCount(); i++) {
                                try {
                                    if ("0".equals(myCartAdapter.getListItem().get(i).getString("checked"))) {
                                        check = false;
                                        break;
                                    }
                                } catch (Exception e) {
                                    L.e(e);
                                }
                            }
                            allSelect.setChecked(check);
                        }

                        setTotalAmount();

                    } catch (Exception e) {
                        L.e(e);
                    }
                }

                @Override
                public void onPlusClick(int position) {
                    try {
                        showProgressDialog();
                        int count = Integer.parseInt((myCartAdapter.getListItem().get(position)).getString("sale_q"));
                        updateCart(position, count + 1);
                    } catch (Exception e) {
                        L.e(e);
                        hideProgressDialog();
                    }
                }

                @Override
                public void onMinusClick(int position) {
                    try {
                        showProgressDialog();
                        int count = Integer.parseInt((myCartAdapter.getListItem().get(position)).getString("sale_q"));
                        if (count > 1)
                        updateCart(position, count - 1);

                    } catch (Exception e) {
                        L.e(e);
                        hideProgressDialog();
                    }
                }

                @Override
                public void onDeleteClick(int position) {
                    deleteCart(position);
                }
            });
            recyclerViewCart.setAdapter(myCartAdapter);
            allSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String isChecked = allSelect.isChecked()?"1":"0";
                    for (int i = 0; i < myCartAdapter.getItemCount(); i++) {
                        try {
                            myCartAdapter.getListItem().get(i).put("checked", isChecked);
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                    myCartAdapter.notifyDataSetChanged();

                    setTotalAmount();
                }
            });
            all_select_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String isChecked = allSelect.isChecked()?"1":"0";
                    for (int i = 0; i < myCartAdapter.getItemCount(); i++) {
                        try {
                            myCartAdapter.getListItem().get(i).put("checked", isChecked);
                        } catch (Exception e) {
                            L.e(e);
                        }
                    }
                    myCartAdapter.notifyDataSetChanged();

                    setTotalAmount();
                }
            });
            goSettlement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        JSONArray orderList = new JSONArray();

                        for (int i = 0; i < myCartAdapter.getListItem().size(); i++) {
                            JSONObject j = myCartAdapter.getListItem().get(i);
                            if ("1".equals(j.getString("checked"))) {
                                orderList.put(j);
                            }
                        }

                        if (orderList.length() > 0) {
                            Intent intent = new Intent(activity, ConfirmActivity.class);
                            intent.putExtra("itemList", orderList.toString());
                            intent.putExtra("totalAmount", getTotalAmount());
                            intent.putExtra("deliveryAmount", getTotalDeliveryAmount());
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        L.e(e);
                    }
                }
            });
        } catch (Exception e) {
            L.d("initView e");
            L.e(e);
        }

    }

    @Override
    protected void initData() {
        super.initData();
        L.d("initData");
        getCartList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getCartList() {

        try {

            L.d("getCartList");

            //	'myData': "{ \"lang_type\": \"kor\", \"div_code\": \"2\", \"custom_code\": \"0102109209162c04\", \"token\": \"0102109209162c042e382760-26d7-4ca6-a017-c7e3364df787\", \"pg\":\"1\", \"pagesize\":\"50\"}"

            //"d": "{\"carorders\":[{\"plan_yyyymm\":\"\",\"money_unit\":\"RMB\",\"plan_type\":\"1\",\"item_code\":\"180311020703\",\"sale_q\":\"1\",\"item_p\":\"45000\",\"discount_p\":\"5000\",\"delivery_p\":\"1500\",\"sale_amount_o\":\"46500\",\"rum\":\"1\",\"num\":\"1\",\"supply_name\":\"온리더 \",\"supply_code\":\"010530117822fbe4\",\"item_name\":\"[바디이펙트] BRA 컴포트브라 스포츠(110590) 이태리 직수입\",\"image_url\":\"http://fileserver.gigawon.co.kr:8588/Item/sub_0013.png\"}],\"status\":\"1\",\"flag\":\"1501\",\"msg\":\"작업완료\",\"start_page\":\"1\",\"end_page\":\"1\",\"pg\":\"1\",\"tot_page\":\"1\",\"tot_cnt\":\"1\"}"

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_CART_LIST)
                    .addParam("myData", "{lang_type:'kor',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) +
                            "',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "',pg:'" + pg + "',pagesize:'50'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();

                            myCartAdapter.getListItem().clear();
                            myCartAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {
                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));
                                pg = Integer.parseInt(main.getString("pg")) + 1;
                                totalPage = Integer.parseInt(main.getString("tot_page"));

                                JSONArray cartList = main.getJSONArray("carorders");

                                for (int i = 0; i < cartList.length() ; i++) {
                                    JSONObject item = cartList.getJSONObject(i);
                                    item.put("checked", "0");
                                    myCartAdapter.getListItem().add(item);
                                }

                                myCartAdapter.notifyDataSetChanged();

                                setTotalAmount();

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public void deleteCart(int position) {

        try {

            String item_code = myCartAdapter.getListItem().get(position).getString("item_code");
            String supply_code = myCartAdapter.getListItem().get(position).getString("supply_code");
            String mall_custom_code = myCartAdapter.getListItem().get(position).getString("mall_custom_code");
            //	'myData': "{\"lang_type\": \"kor\",\"div_code\": \"2\",\"custom_code\": \"0102109209162c04\",\"token\": \"0102109209162c042e382760-26d7-4ca6-a017-c7e3364df787\", \"item_code\": \"1803000000600\", \"supply_code\": \"01071390102abcde\",}"

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_DELETE_CART)
                    .addParam("myData",
                            "{lang_type:'kor'," +
                            "div_code:'2'," +
                            "custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "'," +
                            "token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'," +
                            "item_code:'" + item_code + "'," +
                            "mall_custom_code:'" + mall_custom_code + "'," +
                            "supply_code:'" + supply_code + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));

                                if ("1".equals(main.getString("status"))) {
                                    myCartAdapter.getListItem().remove(position);
                                    myCartAdapter.notifyDataSetChanged();
                                }

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public void updateCart(int position, final int count) {

        try {

            String item_code = myCartAdapter.getListItem().get(position).getString("item_code");
            //String sale_q = myCartAdapter.getListItem().get(position).getString("sale_q");
            String supply_code = myCartAdapter.getListItem().get(position).getString("supply_code");
            String mall_custom_code = myCartAdapter.getListItem().get(position).getString("mall_custom_code");
//            {
//                'myData': "{\"lang_type\": \"kor\",\"custom_code\": \"0102109209162c04\",\"token\": \"0102109209162c042b92f0de-5aa0-4420-9029-980d34c9ee4e\",\"mall_custom_code\": \"gigakorea\",\"supply_code\": \"0102367238949434\", \"item_code\": \"1903110000003630\", \"sale_q\": \"3\"}"
//            }

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_UPDATE_CART)
                    .addParam("myData",
                            "{lang_type:'kor'," +
                            "div_code:'2'," +
                            "custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) + "'," +
                            "token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "'," +
                            "item_code:'" + item_code + "'," +
                            "sale_q:'" + count + "'," +
                            "mall_custom_code:'" + mall_custom_code + "'," +
                            "supply_code:'" + supply_code + "'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {

                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));

                                if ("1".equals(main.getString("status"))) {
                                    (myCartAdapter.getListItem().get(position)).put("sale_q", "" + count);
                                    myCartAdapter.notifyDataSetChanged();
                                    setTotalAmount();
                                }

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

    public void setTotalAmount() {

        int amount = 0;

        for (int i = 0; i < myCartAdapter.getItemCount(); i++) {
            try {
                if ("1".equals(myCartAdapter.getListItem().get(i).getString("checked")))
                    amount = amount + (Integer.parseInt(myCartAdapter.getListItem().get(i).getString("sale_amount_o")));
            } catch (Exception e) {
                L.e(e);
            }
        }

        totalAmount.setText("" + decimalFormat.format(amount) + getString(R.string.price_unit));

    }

    public int getTotalAmount() {

        int amount = 0;

        for (int i = 0; i < myCartAdapter.getItemCount(); i++) {
            try {
                if ("1".equals(myCartAdapter.getListItem().get(i).getString("checked")))
                    amount = amount + (Integer.parseInt(myCartAdapter.getListItem().get(i).getString("sale_amount_o")));
            } catch (Exception e) {
                L.e(e);
            }
        }

        return amount;

    }

    public int getTotalDeliveryAmount() {

        int amount = 0;

        for (int i = 0; i < myCartAdapter.getItemCount(); i++) {
            try {
                if ("1".equals(myCartAdapter.getListItem().get(i).getString("checked")))
                    amount = amount + (Integer.parseInt(myCartAdapter.getListItem().get(i).getString("delivery_p")));
            } catch (Exception e) {
                L.e(e);
            }
        }

        return amount;

    }

}
