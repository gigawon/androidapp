package com.giga.shopping.fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.giga.library.base.IBaseFragment;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.MyCartAdapter;
import com.giga.shopping.bean.DataDetail;
import com.giga.shopping.bean.ShopCart;
import com.giga.shopping.http.HttpHelper;
import com.giga.shopping.http.ICallBack;
import com.giga.shopping.utils.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Request;


public class ShoppingCartFragment extends IBaseFragment {

    Unbinder unbinder;

    @BindView(R.id.recyclerView_cart)
    RecyclerView recyclerViewCart;

    @BindView(R.id.total_amount)
    TextView totalAmount;

    @BindView(R.id.all_select_txt)
    CheckBox allSelect;

    @BindView(R.id.go_settlement)
    TextView goSettlement;

    boolean isselect = false;
    private MyCartAdapter myCartAdapter;
    private int pg = 1;
    private int totalPage = 2;

    @Override
    protected void initialize() {
        setContentView(R.layout.fragment_shopping_cart);
    }

    @Override
    protected void initView() {
        super.initView();

        unbinder = ButterKnife.bind(this, layout);

        try {
            L.d("initView");

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
            recyclerViewCart.setLayoutManager(linearLayoutManager);
            myCartAdapter = new MyCartAdapter(activity, new ArrayList<JSONObject>());
            myCartAdapter.setItemClickListener(new MyCartAdapter.ListControllListener() {
                @Override
                public void onCheckBoxClick(int position) {
                    try {
                        String checked = (myCartAdapter.getListItem().get(position)).getString("checked").equals("1") ? "0" : "1";
                        (myCartAdapter.getListItem().get(position)).put("checked", checked);
                        myCartAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        L.e(e);
                    }
                }

                @Override
                public void onPlusClick(int position) {
                    try {
                        int count = Integer.parseInt((myCartAdapter.getListItem().get(position)).getString("sale_q"));
                        (myCartAdapter.getListItem().get(position)).put("sale_q", "" + (count + 1));
                        myCartAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        L.e(e);
                    }
                }

                @Override
                public void onMinusClick(int position) {
                    try {
                        int count = Integer.parseInt((myCartAdapter.getListItem().get(position)).getString("sale_q"));
                        (myCartAdapter.getListItem().get(position)).put("sale_q", "" + (count - 1));
                        myCartAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        L.e(e);
                    }
                }

                @Override
                public void onDeleteClick(int position) {

                }
            });
            recyclerViewCart.setAdapter(myCartAdapter);

        } catch (Exception e) {
            L.d("initView e");
            L.e(e);
        }

    }

    @Override
    protected void initData() {
        super.initData();
        L.d("initData");
        getCartList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getCartList() {

        try {

            L.d("getCartList");

            //	'myData': "{ \"lang_type\": \"kor\", \"div_code\": \"2\", \"custom_code\": \"0102109209162c04\", \"token\": \"0102109209162c042e382760-26d7-4ca6-a017-c7e3364df787\", \"pg\":\"1\", \"pagesize\":\"50\"}"

            new HttpHelper.Create(activity)
                    .setUrl(AppConstants.URL.WWW_BASEURL)
                    .service(AppConstants.SERVICE.REQUEST_CART_LIST)
                    .addParam("myData", "{lang_type:'kor',div_code:'2',custom_code:'" + SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE) +
                            "',token:'" + SPUtils.getInstance().getString(AppConstants.KEY.TOKEN) + "',pg:'" + pg + "',pagesize:'50'}")
                    .execute(new ICallBack<DataDetail>() {

                        @Override
                        public void onBefore(Request request, int id) {
                            super.onBefore(request, id);
                            showProgressDialog();
                        }

                        @Override
                        public void onAfter(int id) {
                            super.onAfter(id);
                            hideProgressDialog();
                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            ToastUtils.showShortToast(e.getMessage());
                        }

                        @Override
                        public void onResponse(DataDetail response, int id) {
                            if (response == null) {
                                return;
                            }

                            try {
                                myCartAdapter.getListItem().clear();
                                JSONObject main = new JSONObject(response.getD());
                                ToastUtils.showShortToast(main.getString("msg"));
                                pg = Integer.parseInt(main.getString("pg")) + 1;
                                totalPage = Integer.parseInt(main.getString("tot_page"));

                                JSONArray cartList = main.getJSONArray("carorders");

                                for (int i = 0; i < cartList.length() ; i++) {
                                    JSONObject item = cartList.getJSONObject(i);
                                    item.put("checked", "0");
                                    myCartAdapter.getListItem().add(item);
                                }

                                myCartAdapter.notifyDataSetChanged();

                            } catch (Exception e) {
                                L.e(e);
                            }
                        }
                    });

        } catch (Exception e) {
            L.e(e);
        }

    }

}
