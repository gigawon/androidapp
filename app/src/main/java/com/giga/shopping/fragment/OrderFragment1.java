package com.giga.shopping.fragment;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.giga.library.base.IBaseFragment;
import com.giga.shopping.AppConstants;
import com.giga.shopping.R;
import com.giga.shopping.adapter.MyOrderAdapter;
import com.giga.shopping.utils.XListView;

import org.json.JSONArray;

public class OrderFragment1 extends ListFragment implements XListView.IXListViewListener {

    private String orderCode;
    private RelativeLayout id_ad;
    private int currentPage = 1;
    private int TotalCount = 0;
    private MyOrderAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        orderCode = bundle.getString(AppConstants.KEY.KEY_JSON_FM_ORDERSTATUS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_order, container, false);
        id_ad = (RelativeLayout) v.findViewById(R.id.id_ad);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {


            ((XListView) getListView()).setXListViewListener(this);

            ((XListView) getListView()).setPullLoadEnable(true);

        } catch (Exception e) {

        }
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        getUserOrderList();

        ((XListView) getListView()).stopLoadMore();
        ((XListView) getListView()).stopRefresh();
    }



    @Override
    public void onLoadMore() {
        currentPage++;
        getUserOrderList();

        ((XListView) getListView()).stopLoadMore();
        ((XListView) getListView()).stopRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserOrderList();
    }

    private void getUserOrderList() {
        getListView().setVisibility(View.VISIBLE);
        id_ad.setVisibility(View.GONE);
        adapter = new MyOrderAdapter(getActivity(),OrderFragment1.this);
        setListAdapter(adapter);
    }
}
