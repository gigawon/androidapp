package com.giga.shopping;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;

import com.giga.library.util.GsonUtils;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.shopping.activity.MainActivity;
import com.giga.shopping.http.OkHttpHelper;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Request;

public class FCMService extends com.google.firebase.messaging.FirebaseMessagingService {

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        L.d("onNewToken token: " + token);

        updateToken(token);

    }

    // 메시지 수신
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        L.d("onMessageReceived");

        Map<String, String> data = remoteMessage.getData();
        String title = data.get("title");
        String msg = data.get("msg");
        sendNotification(title, msg);
    }

    private void sendNotification(String title, String message) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "giga channel";
            String description = "giga channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("giga_korea", name, importance);
            channel.setDescription(description);
            channel.enableLights(true);
            channel.setLightColor(Color.GREEN);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 100, 200});
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "giga_korea")
                .setSmallIcon(R.mipmap.img_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setCategory(Notification.CATEGORY_MESSAGE)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        mBuilder.setDefaults(NotificationCompat.DEFAULT_SOUND);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, mBuilder.build());

    }

    public void updateToken(String token) {

        String custom_code = SPUtils.getInstance().getString(AppConstants.KEY.CUSTOM_CODE);

        if (custom_code != null && !custom_code.equals("")) {

            String deviceId = custom_code;

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                TelephonyManager telephonyManager;
                telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                deviceId = telephonyManager.getDeviceId();

            }

            HashMap<String, String> params = new HashMap<>();
            params.put("custom_code", custom_code);
            params.put("appl_gubun", "11");
            params.put("device_id", deviceId);
            params.put("token", token);
            params.put("os_gubun", "aos");

            OkHttpHelper okHttpHelper = new OkHttpHelper(this, null);
            okHttpHelper.addPostRequest(new OkHttpHelper.CallbackLogic() {
                @Override
                public void onBizSuccess(String responseDescription, JSONObject data, String flag) {
                }

                @Override
                public void onBizFailure(String responseDescription, JSONObject data, String flag) {
                }

                @Override
                public void onNetworkError(Request request, IOException e) {
                }
            }, "http://samanager.gigawon.co.kr:8824/api/PushInfo/requestPushInfo", GsonUtils.createGsonString(params));

        }
    }

}
