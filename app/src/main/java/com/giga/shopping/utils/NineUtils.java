package com.giga.shopping.utils;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Paint;
import android.util.TypedValue;
import android.view.View;
import com.giga.shopping.MyApplication;

/**
 * @author KCrason
 * @date 2018/5/6
 */
public class NineUtils {

    public static int dp2px(float dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, MyApplication.getInstance().getResources().getDisplayMetrics());
    }

    public static int getScreenWidth() {
        return  MyApplication.getInstance().getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight(){
        return  MyApplication.getInstance().getResources().getDisplayMetrics().heightPixels;
    }

    public static int calcStatusBarHeight(Context context) {
        int statusHeight = -1;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height").get(object).toString());
            statusHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }

    public static boolean calculateShowCheckAllText(String content) {
        Paint textPaint = new Paint();
        textPaint.setTextSize(NineUtils.dp2px(16f));
        float textWidth = textPaint.measureText(content);
        float maxContentViewWidth = NineUtils.getScreenWidth() - NineUtils.dp2px(74f);
        float maxLines = textWidth / maxContentViewWidth;
        return maxLines > 4;
    }

//
//    public static void showSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout, OnStartSwipeRefreshListener onStartSwipeRefreshListener) {
//        if (swipeRefreshLayout != null) {
//            swipeRefreshLayout.post(() -> {
//                swipeRefreshLayout.setRefreshing(true);
//                Single.timer(200, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
//                        .subscribe(aLong -> {
//                            if (onStartSwipeRefreshListener != null) {
//                                onStartSwipeRefreshListener.onStartRefresh();
//                            }
//                        });
//            });
//        }
//    }

//    public static void hideSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
//        if (swipeRefreshLayout != null) {
//            swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(false));
//        }
//    }

    public static void startAlphaAnimation(View view, boolean isShowTranslation) {
        if (isShowTranslation && view != null) {
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0.5f, 1f);
            valueAnimator.addUpdateListener(animation -> view.setAlpha((Float) animation.getAnimatedValue()));
            valueAnimator.setDuration(500).start();
        }
    }
    public final static class FriendCircleType {
        //纯文字
        public final static int FRIEND_CIRCLE_TYPE_ONLY_WORD = 0;
        //文字和图片
        public final static int FRIEND_CIRCLE_TYPE_WORD_AND_IMAGES = 1;
        //分享链接
        public final static int FRIEND_CIRCLE_TYPE_WORD_AND_URL = 2;
    }
}
