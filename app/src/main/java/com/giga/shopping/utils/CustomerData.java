package com.giga.shopping.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.giga.library.util.L;
import com.giga.library.util.SPUtils;
import com.giga.shopping.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import ch.ielse.view.imagewatcher.ImageWatcher;

public class CustomerData {

    public static void initData() {

        try {

            JSONObject customerData = new JSONObject(SPUtils.getInstance().getString(AppConstants.KEY.CUSTOMER_DATA, "{}"));
            if (!customerData.has("searchHistory")) {
                customerData.put("searchHistory", new JSONObject());
            }

            if (!customerData.has("clickHistory")) {
                customerData.put("clickHistory", new JSONObject());
            }

            SPUtils.getInstance().put(AppConstants.KEY.CUSTOMER_DATA, customerData.toString());
        } catch (Exception e) {
            L.e(e);
        }

    }

    public static void putSearchHistory(String history) {
        try {

            JSONObject customerData = new JSONObject(SPUtils.getInstance().getString(AppConstants.KEY.CUSTOMER_DATA, "{}"));
            if (!customerData.has("searchHistory")) {
                initData();
            }

            JSONObject data = new JSONObject();
            data.put("time", "" + System.currentTimeMillis());
            data.put("data", history);

            if (customerData.getJSONArray("searchHistory").length() > 1000) {
                customerData.getJSONArray("searchHistory").remove(1000);
            }
            customerData.getJSONArray("searchHistory").put(data);

            L.d("customerData : " + customerData.toString());
            SPUtils.getInstance().put(AppConstants.KEY.CUSTOMER_DATA, customerData.toString());
        } catch (Exception e) {
            L.e(e);
        }
    }

}