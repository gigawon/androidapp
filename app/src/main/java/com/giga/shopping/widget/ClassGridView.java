package com.giga.shopping.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

public class ClassGridView extends GridView {
    public ClassGridView(Context context) {
        super(context);
    }

    public ClassGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClassGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);

    }
}
