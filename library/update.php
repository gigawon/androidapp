<?php
header("Content-Type: application/json; charset=UTF-8");

class UpdateInfo{
    public $versionCode;
    public $minVersionCode;
    public $versionName;
    public $fileSize;
    public $updateContent;
    public $downloadUrl;
}

$updateInfo=new UpdateInfo();
$updateInfo->versionCode="170630";
$updateInfo->minVersionCode="170630";
$updateInfo->versionName="1.1.170630";
$updateInfo->fileSize="8.2M";
$updateInfo->updateContent="修复部分Bug,运行更流畅.";
$updateInfo->downloadUrl="http://gdown.baidu.com/data/wisegame/a0f5d56c174899f7/gaodeditu_6081.apk";

echo json_encode($updateInfo,JSON_UNESCAPED_UNICODE);
?>