package com.giga.library.code;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.DiskLruCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.module.GlideModule;

/**
 * Created by LaMitad on 2017/4/21.
 */

public class IGlideModule  extends AppGlideModule {

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
   // implements GlideModule
//
//    @Override
//    public void applyOptions(Context context, GlideBuilder builder) {
//        //设置内存缓存大小
//        int maxMemory = (int) Runtime.getRuntime().maxMemory();//获取系统分配给应用的总内存大小
//        int memoryCacheSize = maxMemory / 5;//设置图片内存缓存占用五分之一
//        builder.setMemoryCache(new LruResourceCache(memoryCacheSize));
//        //设置BitmapPool缓存内存大小
//        builder.setBitmapPool(new LruBitmapPool(memoryCacheSize));
//        //设置磁盘缓存大小
//        int diskCacheSize = GlideConfig.diskCacheSize;
//        if (diskCacheSize < 0) {
//            diskCacheSize = 1024 * 1024 * 64;
//        }
//        builder.setDiskCache(new DiskLruCacheFactory(GlideConfig.cacheDir, "glide", diskCacheSize));
//    }
//
//
//
//    @Override
//    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
//
//    }
}
