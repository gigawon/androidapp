package com.giga.library.code;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;

/**
 * Created by LaMitad on 2016/12/19.
 */

public class FloatingActionButtonAutoHide extends RecyclerView.OnScrollListener {

    private FloatingActionButton floatingActionButton;
    private int offset;

    public FloatingActionButtonAutoHide(FloatingActionButton floatingActionButton, int offset) {
        this.floatingActionButton = floatingActionButton;
        this.offset = offset;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
            if (Math.abs(dy) > offset) {
                floatingActionButton.hide();
            }
        } else {
            floatingActionButton.show();
        }
    }
}