package com.giga.library.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.giga.library.R;
import com.giga.library.util.LogUtils;

/**
 * Created by LaMitad on 16/1/5.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private final String className = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogUtils.v(">>> " + className + " onCreate");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        onBefore();
        super.onCreate(savedInstanceState);
        initialize(savedInstanceState);
        initView();
        initData();
        onAfter();
    }

    @Override
    protected void onResume() {
        LogUtils.v(">>> " + className + " onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        LogUtils.v(">>> " + className + " onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        LogUtils.v(">>> " + className + " onDestroy");
        super.onDestroy();
    }

    /**
     * onCreate 执行之前的操作
     */
    protected void onBefore() {
    }

    /**
     * 用于初始化对象,获取Intent数据等操作
     */
    protected abstract void initialize(Bundle savedInstanceState);

    /**
     * 用于初始化视图,获取控件实例
     */
    protected abstract void initView();

    /**
     * 用于初始化数据,填充视图
     */
    protected void initData() {
    }

    /**
     * 用于执行数据填充完后的操作
     */
    protected void onAfter() {
    }


    /**
     * 监听返回键
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (onPressBack()) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 复写返回键操作,返回true则不继续下发
     *
     * @return
     */
    protected boolean onPressBack() {
        return false;
    }

    /**
     * 增加默认的界面切换动画
     */
    @Override
    public void startActivity(Intent intent) {
        startActivity(intent, true);
    }

    public void startActivity(Intent intent, boolean anim) {
        super.startActivity(intent);
        if (anim) overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode, true);
    }

    public void startActivityForResult(Intent intent, int requestCode, boolean anim) {
        super.startActivityForResult(intent, requestCode);
        if (anim) overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    @Override
    public void finish() {
        finish(true);
    }

    public void finish(boolean anim) {
        super.finish();
        if (anim) overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

}
