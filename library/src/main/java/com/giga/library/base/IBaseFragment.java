package com.giga.library.base;

import android.support.v4.widget.SwipeRefreshLayout;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.zhy.http.okhttp.OkHttpUtils;

import com.giga.library.R;
import com.giga.library.util.EmptyUtils;
import com.giga.library.viewinject.ViewInjectorImpl;


/**
 * Created by LaMitad on 2016/12/16.
 */

public abstract class IBaseFragment extends BaseFragment {

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void initView() {
        //ViewInjector初始化
        ViewInjectorImpl.getInstance().inject(this, layout);
        initProgressDialog();
    }

    @Override
    protected void initData() {
        super.initData();
        OkHttpUtils.getInstance().cancelTag(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        OkHttpUtils.getInstance().cancelTag(this);
    }

    /**
     * SwipeRefreshLayout 相关
     */
    protected void setSwipeColor(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
        swipeRefreshLayout.setColorSchemeResources(R.color.teal, R.color.green, R.color.amber, R.color.blue);
    }

    protected void startRefreshing() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    protected void stopRefreshing() {
        stopRefreshing(800);
    }

    protected void stopRefreshing(int time) {
        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }, time);
    }

    /**
     * ProgressDialog
     */
    private KProgressHUD hud;

    private void initProgressDialog() {
        if (hud == null) {
            hud = KProgressHUD.create(activity)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCancellable(false);
        }
    }

    public void showProgressDialog() {
        if (hud != null) {
            hud.show();
        }
    }

    public void hideProgressDialog() {
        if (hud != null) {
            hud.dismiss();
        }
    }

    /**
     * 获取颜色
     *
     * @param colorRes 资源ID
     * @return
     */
    protected int getResColor(int colorRes) {
        return getResources().getColor(colorRes);
    }

    public void refresh() {
        if (EmptyUtils.isNotEmpty(activity)) {
            initData();
        }
    }
}
