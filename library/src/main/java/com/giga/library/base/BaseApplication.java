package com.giga.library.base;

import android.app.Application;
import android.content.Context;

import com.giga.library.util.C;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;
import com.zhy.http.okhttp.OkHttpUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import com.giga.library.code.GlideConfig;
import com.giga.library.http.ILogInterceptor;
import com.giga.library.thread.CleanTempThread;
import com.giga.library.update.AppUpdate;
import com.giga.library.util.AppUtils;
import com.giga.library.util.CrashUtils;
import com.giga.library.util.DirectoryUtils;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.LogUtils;
import com.giga.library.util.ScreenUtils;
import com.giga.library.util.Utils;
import okhttp3.OkHttpClient;

/**
 * Created by LaMitad on 2016/12/12.
 */

public abstract class BaseApplication extends Application {


    private Context context;

    private int screenWidth;
    private int screenHeight;

    //默认存储目录
    private String basePath;
    //APP默认路径
    private String appPath;
    //缓存路径
    private String cachePath;
    //临时文件目录
    private String tempPath;
    //下载文件夹目录
    private String downloadPath;
    //保存log的目录
    private String logPath;
    public static Gson gson;
    private static BaseApplication instance;

    public static BaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context = getApplicationContext();
        Utils.init(context);
        screenWidth = ScreenUtils.getScreenWidth();
        screenHeight = ScreenUtils.getScreenHeight();
    }

    public static Config getConfig(BaseApplication application) {
        return new Config(application);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getAppPath() {
        return appPath;
    }

    public void setAppPath(String appPath) {
        this.appPath = appPath;
    }

    public String getCachePath() {
        return cachePath;
    }

    public void setCachePath(String cachePath) {
        this.cachePath = cachePath;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    /**
     * 初始化App
     *
     * @param config
     */
    protected void init(Config config) {
        //初始化文件目录
        initDir(config.getDirName());
        //初始化Log
        new LogUtils.Builder()
                .setLogSwitch(config.isDebug())
                .setGlobalTag(config.getTag())
                .setLog2FileSwitch(config.isDebug(), logPath)
                .setVersion(AppUtils.getAppVersionName() + "(" + AppUtils.getAppVersionCode() + ")");
        LogUtils.i("========== Application init ==========");
        //打印App最大申请内存
        long maxMemory = Runtime.getRuntime().maxMemory();
        LogUtils.v("Application max memory: " + Long.toString(maxMemory / (1024 * 1024)) + "MB");
        //初始化Crash模块
        CrashUtils.getInstance().init(config.isDebug(), getLogPath());
        LogUtils.v("Crash model initial success");
        //初始化App更新
        if (EmptyUtils.isNotEmpty(config.getAppUpdateUrl())) {
            AppUpdate.getInstance().init(context, config.getAppUpdateUrl(), downloadPath, false);
        }
        LogUtils.v("App update model initial success");
        //初始化友盟统计模块
        if (EmptyUtils.isNotEmpty(config.getUmengKey())) {
            if (EmptyUtils.isEmpty(config.getUmengChannel())) {
                initUmeng(config.getUmengKey(), "Default");
            } else {
                initUmeng(config.getUmengKey(), config.getUmengChannel());
            }
        }
        LogUtils.v("Umeng model initial success");
        //Okhttp
        if (EmptyUtils.isNotEmpty(config.getTag())) {
            initOkHttp(config.getTag());
        } else {
            initOkHttp();
        }
        LogUtils.v("OkHttp model initial success");
        //Glide
        GlideConfig.diskCacheSize = config.getMaxCacheSize();
        GlideConfig.cacheDir = getCachePath();
        LogUtils.v("Glide model initial success");
        //清理临时文件夹
        new CleanTempThread(this).start();
        LogUtils.i("Application initial complete");
        if (EmptyUtils.isNotEmpty(config.getFileProvider())) {
            C.FILE_CONTENT_FILEPROVIDER = config.getFileProvider();
        }
        if (EmptyUtils.isNotEmpty(config.getAppName())) {
            C.APP_NAME = config.getAppName();
        }
    }

    /**
     * 初始化目录模块
     *
     * @param dirName 文件夹名
     */
    protected void initDir(String dirName) {
        if (EmptyUtils.isEmpty(dirName)) {
            dirName = getPackageName();
        }
        basePath = DirectoryUtils.getDefaultStoragePath();
        appPath = basePath + File.separator + dirName;
        cachePath = appPath + File.separator + "cache";
        tempPath = appPath + File.separator + "temp";
        downloadPath = appPath + File.separator + "download";
        logPath = appPath + File.separator + "log";
        //判断目录是否存在,如不存在则创建目录
        File dir = new File(cachePath);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
        dir = new File(tempPath);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
        dir = new File(downloadPath);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
        dir = new File(logPath);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    /**
     * 初始化友盟统计
     *
     * @param umengKey 友盟KEY
     * @param channel  渠道
     */
    protected void initUmeng(String umengKey, String channel) {
        MobclickAgent.UMAnalyticsConfig umAnalyticsConfig =
                new MobclickAgent.UMAnalyticsConfig(context, umengKey, channel, MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.startWithConfigure(umAnalyticsConfig);
    }

    /**
     * 初始化Okhttp
     *
     * @param tag            网络请求Tag
     * @param connectTimeout 连接超时
     * @param readTimeout    读取超时
     * @param writeTimeout   写入超时
     */
    protected void initOkHttp(String tag, long connectTimeout, long readTimeout, long writeTimeout) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new ILogInterceptor(tag, true))
                .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(writeTimeout, TimeUnit.MILLISECONDS)
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }

    protected void initOkHttp() {
        initOkHttp("OkHttp", 10000L, 10000L, 10000L);
    }

    protected void initOkHttp(String tag) {
        initOkHttp(tag, 10000L, 10000L, 10000L);
    }


    /**
     * 配置构造器
     */
    public static class Config {

        private BaseApplication application;

        private boolean debug;
        private String dirName;
        private String tag;
        private String umengKey;
        private String umengChannel;
        private String appUpdateUrl;
        private int maxCacheSize;
        private String fileProvider;
        private String appName;

        public Config(BaseApplication application) {
            this.application = application;
            debug = true;
            dirName = application.getContext().getPackageName();
            tag = application.getContext().getPackageName();
            maxCacheSize = -1;
            fileProvider = application.getContext().getPackageName()+".fileprovider";

        }

        public String getAppName() {
            return appName;
        }

        public Config setAppName(String appName) {
            this.appName = appName;
            return this;
        }

        public Config setDebug(boolean debug) {
            this.debug = debug;
            return this;
        }

        public Config setDirName(String dirName) {
            this.dirName = dirName;
            return this;
        }

        public Config setFileProvider(String fileProvider) {
            this.fileProvider = fileProvider;
            return this;
        }

        public Config setTag(String tag) {
            this.tag = tag;
            return this;
        }

        public Config setUmengKey(String umengKey) {
            this.umengKey = umengKey;
            return this;
        }

        public Config setUmengChannel(String umengChannel) {
            this.umengChannel = umengChannel;
            return this;
        }

        public Config setAppUpdateUrl(String appUpdateUrl) {
            this.appUpdateUrl = appUpdateUrl;
            return this;
        }

        public Config setMaxCacheSize(int maxCacheSize) {
            this.maxCacheSize = maxCacheSize;
            return this;
        }

        public void init() {
            application.init(this);
        }

        public boolean isDebug() {
            return debug;
        }

        public String getDirName() {
            return dirName;
        }

        public String getTag() {
            return tag;
        }

        public String getUmengKey() {
            return umengKey;
        }

        public String getUmengChannel() {
            return umengChannel;
        }

        public String getAppUpdateUrl() {
            return appUpdateUrl;
        }

        public String getFileProvider() {
            return fileProvider;
        }

        public int getMaxCacheSize() {
            return maxCacheSize;
        }
    }

}
