package com.giga.library.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.umeng.analytics.MobclickAgent;
import com.zhy.http.okhttp.OkHttpUtils;

import com.giga.library.R;
import com.giga.library.util.ActivityUtils;
import com.giga.library.util.BarUtils;
import com.giga.library.viewinject.ViewInjectorImpl;

import java.text.DecimalFormat;


/**
 * Created by Wxcily on 16/1/5.
 */
public abstract class IBaseActivity extends BaseActivity {

    public DecimalFormat decimalFormat = new DecimalFormat("#,###");

    private SwipeRefreshLayout swipeRefreshLayout;

    static {
        //矢量图支持
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    protected Context context;

    protected boolean addTask = true;

    protected void addTask(boolean addTask) {
        this.addTask = addTask;
    }

    @Override
    protected void onBefore() {
        super.onBefore();
        this.context = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (addTask)
            ActivityUtils.getInstance().addActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OkHttpUtils.getInstance().cancelTag(this);
        if (addTask)
            ActivityUtils.getInstance().delActivity(this);
    }

    @Override
    protected void initView() {
        //ViewInjector初始化
        ViewInjectorImpl.getInstance().inject(this);
        //沉浸式状态栏
        setStatusBar();
        initProgressDialog();
    }

    @Override
    protected void initData() {
        super.initData();
        OkHttpUtils.getInstance().cancelTag(this);
    }

    protected void setStatusBar() {
        BarUtils.setColor(this, getResColor(R.color.white), 50);
    }

    /**
     * 获取颜色
     *
     * @param colorRes 资源ID
     * @return
     */
    protected int getResColor(int colorRes) {
        return getResources().getColor(colorRes);
    }

    /**
     * 隐藏输入法
     */
    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * 获取焦点
     *
     * @param editText
     */
    public void requestFocus(final EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) context
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, 0);
            }
        }, 500);
    }

    /**
     * ProgressDialog
     */
    private KProgressHUD hud;

    private void initProgressDialog() {
        if (hud == null) {
            hud = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCancellable(false);
        }
    }

    public void showProgressDialog() {
        if (hud != null) {
            hud.show();
        }
    }

    public void hideProgressDialog() {
        if (hud != null) {
            hud.dismiss();
        }
    }

    public boolean progressDialogIsShow() {
        if (hud == null) {
            return false;
        } else {
            return hud.isShowing();
        }
    }

    /**
     * SwipeRefreshLayout 相关
     */
    protected void setSwipeColor(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
        swipeRefreshLayout.setColorSchemeResources(R.color.teal, R.color.green, R.color.amber, R.color.blue);
    }

    protected void startRefreshing() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    protected void stopRefreshing() {
        stopRefreshing(800);
    }

    protected void stopRefreshing(int time) {
        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }, time);
    }

    protected void setToolBar(Toolbar toolBar) {
        setSupportActionBar(toolBar);
        toolBar.setNavigationIcon(AppCompatResources.getDrawable(this, R.drawable.ic_action_arrow_back));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!onPressBack()) {
                    onBackPressed();
                }
            }
        });
    }


}
