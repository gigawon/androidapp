package com.giga.library.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.giga.library.R;
import com.giga.library.util.LogUtils;


/**
 * Created by Wxcily on 15/10/29.
 * 简洁构造Fragment
 */
public abstract class BaseFragment extends Fragment {

    private final String className = this.getClass().getSimpleName();

    protected FragmentActivity activity;
    protected int layoutId;
    protected View layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtils.v(">>> " + className + " onCreate");
        activity = getActivity();
        onBefore();
        super.onCreate(savedInstanceState);
        initialize();
    }

    @Override
    public void onResume() {
        LogUtils.v(">>> " + className + " onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        LogUtils.v(">>> " + className + " onPause");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LogUtils.v(">>> " + className + " onDestroy");
        super.onDestroy();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        LogUtils.v(">>> " + className + " onHiddenChanged: " + String.valueOf(hidden));
        super.onHiddenChanged(hidden);
    }

    /**
     * onCreate 执行之前的操作
     */
    protected void onBefore() {
    }

    /**
     * 用于初始化对象,获取Intent数据等操作
     */
    protected abstract void initialize();

    /**
     * 用于初始化视图,获取控件实例
     */
    protected abstract void initView();

    /**
     * 用于初始化数据,填充视图
     */
    protected void initData() {
    }

    protected void onAfter() {
    }

    protected void initLayout(int layoutId) {
        this.layoutId = layoutId;
    }

    protected void setContentView(int layoutId) {
        this.layoutId = layoutId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(layoutId, container, false);
        initView();
        initData();
        onAfter();
        return layout;
    }

    protected View findView(int viewid) {
        return layout.findViewById(viewid);
    }

    /**
     * 增加默认的界面切换动画
     */
    @Override
    public void startActivity(Intent intent) {
        startActivity(intent, true);
    }

    public void startActivity(Intent intent, boolean anim) {
        super.startActivity(intent);
        if (anim) activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode, true);
    }

    public void startActivityForResult(Intent intent, int requestCode, boolean anim) {
        super.startActivityForResult(intent, requestCode);
        if (anim) activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

}
