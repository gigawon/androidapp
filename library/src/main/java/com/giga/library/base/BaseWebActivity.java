package com.giga.library.base;

import android.app.Activity;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import java.util.HashMap;
import java.util.List;

import com.giga.library.R;
import com.giga.library.util.L;


public class BaseWebActivity extends Activity {

	//interface scheme
	private static final String SCHEME = "giga";

	//interface history back
	private static final String HOST_HISTORY_BACK = "historyBack";

	//interface call script
	private static final String HOST_CALL_SCRIPT = "callScript";

	//interface script name
	private static final String SCRIPT_NAME = "scriptName";

	//interface script key
	private static final String KEY = "key";

	//interface script value
	private static final String VALUE = "value";

	//interface javascript
	private static final String JAVA_SCRIPT = "javascript:";

	//interface script params list
	public static HashMap<String, String> INTERFACE_PARAMS = new HashMap<String, String>();

	//webView
	public WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_web);

		webView = (WebView) findViewById(R.id.web_view);

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
		webView.getSettings().setRenderPriority(RenderPriority.HIGH);
		webView.getSettings().setDisplayZoomControls(false);
		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webView.getSettings().setPluginState(PluginState.ON);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		webView.getSettings().setSupportMultipleWindows(true);
		webView.setEnabled(false);
		webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

		webView.setWebChromeClient(new WebChromeClient() {
		});

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub

				try {

					L.d("shouldOverrideUrlLoading : " + url);

					Uri uri = Uri.parse(url);
					String scheme = uri.getScheme();
					String host = uri.getHost();

					if (scheme.equals(SCHEME)) {

						if (host.equals(HOST_HISTORY_BACK)) {
							if (webView.canGoBack() == true)
								webView.goBack();
							else finish();
							return true;
						} else if (host.equals(HOST_CALL_SCRIPT)) {
							String scriptName = uri.getQueryParameter(SCRIPT_NAME);
							String script = JAVA_SCRIPT + scriptName + "(";

							List<String> keys = uri.getQueryParameters(KEY);
							if (keys != null) {
								for (int i = 0; i < keys.size(); i++) {
									String key = keys.get(i);
									String value = INTERFACE_PARAMS.get(key);
									if (value == null) {
										value = "";
									}

									if (i == 0) {
										script = script + "'" + value + "'";
									} else {
										script = script + ",'" + value + "'";
									}
								}
							}

							List<String> values = uri.getQueryParameters(VALUE);

							if (values != null) {
								for (int i = 0; i < values.size(); i++) {
									String value = values.get(i);
									if ((keys == null || keys.size() == 0) && i == 0) {
										script = script + "'" + value + "'";
									} else {
										script = script + ",'" + value + "'";
									}
								}
							}

							script = script + ")";
							L.d("shouldOverrideUrlLoading HOST_CALL_SCRIPT : " + script);
							webView.loadUrl(script);
						}

					}

				} catch (Exception e) {
					L.e(e);
				}

				return super.shouldOverrideUrlLoading(view, url);
			}

		});

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		webView.saveState(outState);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}