package com.giga.library.widget;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import com.giga.library.R;

/**
 * Created by Wxcily on 15/10/29.
 * 可无限循环的轮播控件
 */

public class ImagePageView extends RelativeLayout {
    // 上下文
    private Context context;

    // 轮播控件
    private AdViewPager viewPager;

    // 指示器容器
    private LinearLayout linearLayout;

    // 指示器列表
    private ImageView[] imageViews;

    // 轮播时间
    private int time = 5000;

    public void setTime(int time) {
        this.time = time * 1000;
    }

    // 消息处理
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 0) {
                if (viewPager.getCurrentItem() != Integer.MAX_VALUE) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    handler.sendEmptyMessageDelayed(0, time);
                } else {
                    viewPager.setCurrentItem(Integer.MAX_VALUE);
                }
            }
        }

        ;
    };

    public ImagePageView(Context context) {
        super(context);
        initView(context);
    }

    public ImagePageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(context);
    }

    // 初始化视图
    private void initView(Context context) {
        removeAllViews();
        // 初始化轮播控件
        viewPager = new AdViewPager(context);
        viewPager.setOnPageChangeListener(new AdPageChangeListener());
        viewPager.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        // 开始图片滚动
                        startImageTimerTask();
                        break;
                    default:
                        // 停止图片滚动
                        stopImageTimerTask();
                        break;
                }
                return false;
            }
        });
        // 设置轮播指示器
        linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams layoutParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, dp2px(context, 10));
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        linearLayout.setLayoutParams(layoutParams);
        // 添加到布局
        addView(viewPager);
        addView(linearLayout);
    }

    public void setData(ArrayList<String> urls,
                        OnImagePageListener onImagePageListener) {
        if (urls.size() == 0)
            return;
        initView(context);
        imageViews = new ImageView[urls.size()];
        for (int i = 0; i < imageViews.length; i++) {
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            imageView.setScaleType(ScaleType.CENTER_CROP);
            if (i == 0) {
                imageView.setImageResource(R.drawable.framework_indicators_now);
            } else {
                layoutParams.leftMargin = dp2px(context, 7);
                imageView
                        .setImageResource(R.drawable.framework_indicators_default);
            }
            imageView.setLayoutParams(layoutParams);
            linearLayout.addView(imageView);
            imageViews[i] = imageView;
        }
        viewPager.setAdapter(new AdPagerAdapter(urls, onImagePageListener));
        if (urls.size() > 1) {
            int max = (Integer.MAX_VALUE - 1) / 2;
            viewPager.setCurrentItem(max - (max % urls.size()));
        } else {
            viewPager.setCurrentItem(0);
        }
    }

    // 开始轮播
    public void startImageTimerTask() {
        stopImageTimerTask();
        if (imageViews != null && imageViews.length > 1) {
            handler.sendEmptyMessageDelayed(0, time);
        }
    }

    // 停止轮播
    public void stopImageTimerTask() {
        handler.removeMessages(0);
    }

    private class AdPagerAdapter extends PagerAdapter {
        /**
         * 图片资源列表
         */
        private ArrayList<String> urls = new ArrayList<String>();

        /**
         * 广告图片点击监听
         */
        private OnImagePageListener onImagePageListener;

        public AdPagerAdapter(ArrayList<String> urls,
                              OnImagePageListener onImagePageListener) {
            this.urls = urls;
            this.onImagePageListener = onImagePageListener;
        }

        // 控件个数
        @Override
        public int getCount() {
            return urls.size() > 1 ? Integer.MAX_VALUE : 1;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        // 销毁缓存
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ImageView imageView = (ImageView) object;
            viewPager.removeView(imageView);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            String imageUrl = urls.get(position % urls.size());
            ImageView imageView = new ImageView(context);
            imageView.setLayoutParams(new LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ScaleType.CENTER_CROP);
            // 设置图片点击监听
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImageTimerTask();
                    onImagePageListener.onViewClick(position % urls.size(), v);
                }
            });
            container.addView(imageView);
            onImagePageListener.displayImage(imageUrl, imageView);
            return imageView;
        }

    }

    private class AdPageChangeListener implements OnPageChangeListener {

        // 监听viewpage 状态改变 0:什么都没做 1.正在滑动 2.滑动完毕

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        // 正在滑动时调用 arg0: 当前滑动的veiw arg1:滑动偏移的百分比 arg2:偏移的像素位置
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        // 滑动完成后调用 arg0:当前显示的view
        @Override
        public void onPageSelected(int index) {
            index = index % imageViews.length;
            for (int i = 0; i < imageViews.length; i++) {
                if (i != index) {
                    imageViews[i]
                            .setImageResource(R.drawable.framework_indicators_default);
                } else {
                    imageViews[index]
                            .setImageResource(R.drawable.framework_indicators_now);
                }
            }
        }

    }

    public interface OnImagePageListener {

        public void onViewClick(int position, View imageView);

        public void displayImage(String imageURL, ImageView imageView);
    }

    // 自定义viewpage 解决滑动冲突
    private class AdViewPager extends ViewPager {

        public AdViewPager(Context context) {
            super(context);
        }

        public AdViewPager(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean onTouchEvent(MotionEvent arg0) {
            getParent().requestDisallowInterceptTouchEvent(true);
            return super.onTouchEvent(arg0);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            getParent().requestDisallowInterceptTouchEvent(true);
            super.dispatchTouchEvent(ev);
            return true;
        }

    }

    public static int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
