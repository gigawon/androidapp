package com.giga.library.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.giga.library.R;

/**
 * Created by LaMitad on 2017/1/11.
 */

public class IndexBar extends View {

    public static String[] INDEX_STRING = {"A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "#"};

    private Context context;
    private int backgroundColor;
    private int pressBackgroundColor;
    private int textSize;
    private int textColor;
    private boolean isBold;

    //所有字母索引 用于计算宽度
    private List<String> baseIndex;
    //索引数据源
    private List<IndexDataBean> indexTextList;
    //画笔
    private Paint paint;
    //View的宽高
    private int width, height;
    //每个index区域的高度
    private int itemHeight;


    public IndexBar(Context context) {
        super(context);
        init(context, null);
    }

    public IndexBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        this.context = context;
        //设置默认参数
        backgroundColor = Color.parseColor("#00000000");
        pressBackgroundColor = Color.parseColor("#64000000");
        textColor = Color.parseColor("#C8FFFFFF");
        textSize = dp2px(context, 13);
        isBold = true;
        //获取xml布局参数
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.IndexBar);
            for (int i = 0; i < typedArray.getIndexCount(); i++) {
                int attr = typedArray.getIndex(i);
                if (attr == R.styleable.IndexBar_backgroundColor) {
                    backgroundColor = typedArray.getColor(attr, backgroundColor);
                    setBackgroundColor(backgroundColor);

                } else if (attr == R.styleable.IndexBar_pressBackgroundColor) {
                    pressBackgroundColor = typedArray.getColor(attr, pressBackgroundColor);

                } else if (attr == R.styleable.IndexBar_indexTextColor) {
                    textColor = typedArray.getColor(attr, textColor);

                } else if (attr == R.styleable.IndexBar_indexTextSize) {
                    textSize = typedArray.getDimensionPixelSize(attr, textSize);
                }
            }
            typedArray.recycle();
        }
        //初始化数据源
        indexTextList = new ArrayList<>();
        baseIndex = Arrays.asList(INDEX_STRING);
        //初始化画笔
        paint = new Paint();
        paint.setAntiAlias(true);//抗锯齿
        if (isBold) {
            paint.setTypeface(Typeface.DEFAULT_BOLD);//加粗
        }
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        //初始化背景
        setBackgroundColor(backgroundColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //取出宽高的MeasureSpec  Mode 和Size
        int wMode = MeasureSpec.getMode(widthMeasureSpec);
        int wSize = MeasureSpec.getSize(widthMeasureSpec);
        int hMode = MeasureSpec.getMode(heightMeasureSpec);
        int hSize = MeasureSpec.getSize(heightMeasureSpec);
        int measureWidth = 0, measureHeight = 0;//最终测量出来的宽高

        //得到合适宽度：
        Rect indexBounds = new Rect();//存放每个绘制的index的Rect区域
        String index;//每个要绘制的index内容
        for (int i = 0; i < baseIndex.size(); i++) {
            index = baseIndex.get(i);
            paint.getTextBounds(index, 0, index.length(), indexBounds);//测量计算文字所在矩形，可以得到宽高
            measureWidth = Math.max(indexBounds.width(), measureWidth);//循环结束后，得到index的最大宽度
            measureHeight = Math.max(indexBounds.height(), measureHeight);//循环结束后，得到index的最大高度，然后*size
        }
        measureHeight *= indexTextList.size();
        switch (wMode) {
            case MeasureSpec.EXACTLY:
                measureWidth = wSize;
                break;
            case MeasureSpec.AT_MOST:
                measureWidth = new BigDecimal(measureWidth).multiply(new BigDecimal(2)).intValue();
                break;
            case MeasureSpec.UNSPECIFIED:
                measureWidth = wSize;
                break;
        }

        //得到合适的高度：
        switch (hMode) {
            case MeasureSpec.EXACTLY:
                measureHeight = hSize;
                break;
            case MeasureSpec.AT_MOST:
                measureHeight = Math.min(measureHeight, hSize);//wSize此时是父控件能给子View分配的最大空间
                break;
            case MeasureSpec.UNSPECIFIED:
                break;
        }

        setMeasuredDimension(measureWidth, measureHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int t = getPaddingTop();//top的基准点(支持padding)
        IndexDataBean index;//每个要绘制的index内容
        for (int i = 0; i < indexTextList.size(); i++) {
            index = indexTextList.get(i);
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();//获得画笔的FontMetrics，用来计算baseLine。因为drawText的y坐标，代表的是绘制的文字的baseLine的位置
            int baseline = (int) ((itemHeight - fontMetrics.bottom - fontMetrics.top) / 2);//计算出在每格index区域，竖直居中的baseLine值
            canvas.drawText(index.getLettel(), width / 2 - paint.measureText(index.getLettel()) / 2, t + itemHeight * i + baseline, paint);//调用drawText，居中显示绘制index
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setBackgroundColor(pressBackgroundColor);//手指按下时背景变色
                //注意这里没有break，因为down时，也要计算落点 回调监听器
            case MotionEvent.ACTION_MOVE:
                float y = event.getY();
                //通过计算判断落点在哪个区域：
                int pressI = (int) ((y - getPaddingTop()) / itemHeight);
                //边界处理（在手指move时，有可能已经移出边界，防止越界）
                if (pressI < 0) {
                    pressI = 0;
                } else if (pressI >= indexTextList.size()) {
                    pressI = indexTextList.size() - 1;
                }
                //回调监听器
                if (null != onIndexPressedListener && pressI > -1 && pressI < indexTextList.size()) {
                    onIndexPressedListener.onIndexPressed(indexTextList.get(pressI).getPosition(), indexTextList.get(pressI).getLettel());
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            default:
                setBackgroundColor(backgroundColor);//手指抬起时背景恢复
                //回调监听器
                if (null != onIndexPressedListener) {
                    onIndexPressedListener.onMotionEventEnd();
                }
                break;
        }
        return true;
    }


    public void setData(List<? extends IndexDataBean> data) {
        if (data == null || data.isEmpty()) {
            return;
        }
        this.indexTextList = new ArrayList<>();
        this.indexTextList.addAll(data);
        Iterator<? extends IndexDataBean> iterator = indexTextList.iterator();
        while (iterator.hasNext()) {
            IndexDataBean dataBean = iterator.next();
            if (!dataBean.isIndex()) {
                iterator.remove();
            }
        }
        this.indexTextList.add(0, new HeadIndex());
        invalidate();
    }

    class HeadIndex implements IndexDataBean {

        @Override
        public boolean isIndex() {
            return true;
        }

        @Override
        public String getLettel() {
            return "↑";
        }

        @Override
        public int getPosition() {
            return -1;
        }
    }


    /**
     * 以下情况调用：
     * 1 在数据源改变
     * 2 控件size改变时
     * 计算itemHeight
     */
    private void computeItemHeight() {
        itemHeight = (height - getPaddingTop() - getPaddingBottom()) / indexTextList.size();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = w;
        height = h;
        //add by zhangxutong 2016 09 08 :解决源数据为空 或者size为0的情况,
        if (null == indexTextList || indexTextList.isEmpty()) {
            return;
        }
        computeItemHeight();
    }

    /**
     * 当前被按下的index的监听器
     */
    public interface OnIndexPressedListener {
        void onIndexPressed(int position, String text);//当某个Index被按下

        void onMotionEventEnd();//当触摸事件结束（UP CANCEL）
    }

    private OnIndexPressedListener onIndexPressedListener;

    public void setOnIndexPressedListener(OnIndexPressedListener onIndexPressedListener) {
        this.onIndexPressedListener = onIndexPressedListener;
    }

    /**
     * dp转px
     *
     * @param context 上下文
     * @param dpValue dp值
     * @return px值
     */
    public static int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public void setIndexBarBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        setBackgroundColor(backgroundColor);
    }

    public void setPressBackgroundColor(int pressBackgroundColor) {
        this.pressBackgroundColor = pressBackgroundColor;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
        paint.setTextSize(textSize);
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        paint.setColor(textColor);
    }

    public void setBold(boolean bold) {
        isBold = bold;
        if (isBold) {
            paint.setTypeface(Typeface.DEFAULT_BOLD);//加粗
        } else {
            paint.setTypeface(Typeface.DEFAULT);
        }
    }

    /**
     * 索引数据源实现接口
     */
    public interface IndexDataBean {

        //是否为Index
        boolean isIndex();

        //索引字符
        String getLettel();

        //索引位置
        int getPosition();

    }
}
