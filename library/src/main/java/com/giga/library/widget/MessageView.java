package com.giga.library.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.R;
import com.giga.library.util.SizeUtils;
import com.giga.library.util.StringUtils;

/**
 * Created by Wxcily on 16/3/27.
 */
public class MessageView extends RelativeLayout implements View.OnClickListener {
    private Context context;
    private TextView textView;
    private int textSize = 14;
    private int padding = 9;
    private int time = 3000;
    private int backgroundColor = R.color.green;
    private int wBackgroundColor = R.color.violet;
    private boolean isshow = false;

    private Animation showAnim, hideAnim;

    public MessageView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public MessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    private void initView() {
        showAnim = AnimationUtils.loadAnimation(context, R.anim.anim_message_show);
        hideAnim = AnimationUtils.loadAnimation(context, R.anim.anim_message_hide);

        this.setClickable(true);
        this.setOnClickListener(this);
        textView = new TextView(context);
        textView.setTextColor(context.getResources().getColor(R.color.whiteT230));
        textView.setTextSize(SizeUtils.dp2sp(textSize));
        textView.setPadding(SizeUtils.dp2px(padding), SizeUtils.dp2px(padding), SizeUtils.dp2px(padding), SizeUtils.dp2px(padding));
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, TRUE);
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER);
        TextPaint textPaint = textView.getPaint();
        textPaint.setFakeBoldText(true);
        addView(textView);
        this.setVisibility(View.INVISIBLE);
    }

    public void showMessage(String message) {
        showMessage(message, false);
    }

    public void showWarningMessage(String message) {
        showMessage(message, true);
    }

    /**
     * 显示警告信息
     *
     * @param message   显示的文本
     * @param isWarning 是否为警告
     */
    public void showMessage(String message, boolean isWarning) {
        if (isshow) {
            return;
        }
        if (StringUtils.isEmpty(message)) {
            return;
        }
        isshow = true;
        if (isWarning) {
            setBackgroundColor(context.getResources().getColor(wBackgroundColor));
        } else {
            setBackgroundColor(context.getResources().getColor(backgroundColor));
        }
        textView.setText(message);
        this.setVisibility(View.VISIBLE);
        this.startAnimation(showAnim);
        handler.sendEmptyMessageDelayed(HIDE, time);
    }

    private void hide() {
        this.startAnimation(hideAnim);
        MessageView.this.setVisibility(View.INVISIBLE);
        isshow = false;
    }

    private static final int HIDE = 1;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HIDE:
                    hide();
                    break;
            }
        }
    };

    public void setTextSize(int textSize) {
        textView.setTextSize(SizeUtils.dp2sp(textSize));
    }

    public void setPadding(int padding) {
        textView.setPadding(SizeUtils.dp2px(padding), SizeUtils.dp2px(padding), SizeUtils.dp2px(padding), SizeUtils.dp2px(padding));
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public void onClick(View v) {
        if (isshow) {
            handler.removeMessages(HIDE);
            hide();
        }
    }

    public void showNetWorkError() {
        this.showMessage("网络错误,请重试..", true);
    }

    public void showRequestFailure() {
        this.showMessage("无法连接服务器,请重试..", true);
    }

    public void showEmptyData() {
        this.showMessage("数据为空", true);
    }

}

