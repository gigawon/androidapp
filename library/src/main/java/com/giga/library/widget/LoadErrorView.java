package com.giga.library.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.giga.library.R;
import com.giga.library.util.SizeUtils;


/**
 * Created by LaMitad on 2016/12/20.
 */

public class LoadErrorView extends RelativeLayout implements View.OnClickListener {

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener;
    private TextView textView;

    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
    }

    public LoadErrorView(Context context) {
        super(context);
        initView(context);
    }

    public LoadErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LoadErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        this.setVisibility(GONE);
        this.setClickable(true);
        this.setOnClickListener(this);
        this.setBackgroundColor(context.getResources().getColor(R.color.gray2));
        textView = new TextView(context);
        LayoutParams layoutParam = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParam.addRule(RelativeLayout.CENTER_IN_PARENT);
        textView.setLayoutParams(layoutParam);
        textView.setText("加载失败,点击重试");
        textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        textView.setTextSize(SizeUtils.dp2sp(14));
        TextPaint textPaint = textView.getPaint();
        textPaint.setFakeBoldText(true);
        this.addView(textView);
    }

    @Override
    public void onClick(View view) {
        if (onRefreshListener != null) {
            onRefreshListener.onRefresh();
        }
    }

    public void show() {
        this.setVisibility(VISIBLE);
    }

    public void showEmpty() {
        setText("数据为空,点击重试");
        show();
    }

    public void hide() {
        textView.setText("加载失败,点击重试");
        setDisable(false);
        this.setVisibility(GONE);
    }

    public void setText(String text) {
        textView.setText(text);
    }

    public void setDisable(boolean disable) {
        if (disable) {
            this.setOnClickListener(null);
        } else {
            this.setOnClickListener(this);
        }
    }

}
