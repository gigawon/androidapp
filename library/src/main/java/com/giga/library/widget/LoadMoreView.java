package com.giga.library.widget;


import com.giga.library.R;

/**
 * Created by LaMitad on 2016/12/19.
 * 下拉刷新View
 */

public final class LoadMoreView extends com.chad.library.adapter.base.loadmore.LoadMoreView {

    @Override
    public int getLayoutId() {
        return R.layout.layout_load_more_view;
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.loadingView;
    }

    @Override
    protected int getLoadFailViewId() {
        return R.id.failView;
    }

    @Override
    protected int getLoadEndViewId() {
        return R.id.endView;
    }
}