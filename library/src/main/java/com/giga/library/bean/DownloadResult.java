package com.giga.library.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LaMitad on 2016/12/12.
 */

public class DownloadResult implements Parcelable {

    private boolean success;
    private String url;
    private String path;
    private String data;
    private String packageName;

    public DownloadResult() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.url);
        dest.writeString(this.path);
        dest.writeString(this.data);
        dest.writeString(this.packageName);
    }

    protected DownloadResult(Parcel in) {
        this.success = in.readByte() != 0;
        this.url = in.readString();
        this.path = in.readString();
        this.data = in.readString();
        this.packageName = in.readString();
    }

    public static final Parcelable.Creator<DownloadResult> CREATOR = new Parcelable.Creator<DownloadResult>() {
        @Override
        public DownloadResult createFromParcel(Parcel source) {
            return new DownloadResult(source);
        }

        @Override
        public DownloadResult[] newArray(int size) {
            return new DownloadResult[size];
        }
    };
}

