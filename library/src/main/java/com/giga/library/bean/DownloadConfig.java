package com.giga.library.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LaMitad on 2016/12/12.
 */

public class DownloadConfig implements Parcelable {

    private String url;
    private String path;
    private String fileName;
    private String ticker;
    private String title;
    private int iconResourceId = 0;

    private String action;
    private String data;
    private String packageName;

    public DownloadConfig() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconResourceId() {
        return iconResourceId;
    }

    public void setIconResourceId(int iconResourceId) {
        this.iconResourceId = iconResourceId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.path);
        dest.writeString(this.fileName);
        dest.writeString(this.ticker);
        dest.writeString(this.title);
        dest.writeInt(this.iconResourceId);
        dest.writeString(this.action);
        dest.writeString(this.data);
        dest.writeString(this.packageName);
    }

    protected DownloadConfig(Parcel in) {
        this.url = in.readString();
        this.path = in.readString();
        this.fileName = in.readString();
        this.ticker = in.readString();
        this.title = in.readString();
        this.iconResourceId = in.readInt();
        this.action = in.readString();
        this.data = in.readString();
        this.packageName = in.readString();
    }

    public static final Parcelable.Creator<DownloadConfig> CREATOR = new Parcelable.Creator<DownloadConfig>() {
        @Override
        public DownloadConfig createFromParcel(Parcel source) {
            return new DownloadConfig(source);
        }

        @Override
        public DownloadConfig[] newArray(int size) {
            return new DownloadConfig[size];
        }
    };
}