package com.giga.library.bean;

/**
 * Created by Wxcily on 16/2/2.
 */
public class UpdateInfo {

    private String versionCode;
    private String versionName;
    private String updateContent;
    private String fileSize;
    private String downloadUrl;
    private String minVersionCode;

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getUpdateContent() {
        return updateContent;
    }

    public void setUpdateContent(String updateContent) {
        this.updateContent = updateContent;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getMinVersionCode() {
        return minVersionCode;
    }

    public void setMinVersionCode(String minVersionCode) {
        this.minVersionCode = minVersionCode;
    }
}
