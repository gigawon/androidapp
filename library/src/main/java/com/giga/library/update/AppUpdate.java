package com.giga.library.update;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Vibrator;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.zhy.http.okhttp.OkHttpUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import com.giga.library.R;
import com.giga.library.base.IBaseActivity;
import com.giga.library.bean.DownloadConfig;
import com.giga.library.bean.IEvent;
import com.giga.library.bean.UpdateInfo;
import com.giga.library.http.DownLoadService;
import com.giga.library.http.GenericsCallback;
import com.giga.library.util.AppUtils;
import com.giga.library.util.EmptyUtils;
import com.giga.library.util.EncryptUtils;
import com.giga.library.util.LogUtils;
import com.giga.library.util.NetworkUtils;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;
import okhttp3.Call;

/**
 * Created by Wxcily on 16/2/2.
 */
public class AppUpdate {

    private static AppUpdate instance;
    private Context context;
    private String serverUrl;
    private String downloadPath;
    private boolean mustWifi;
    private boolean isUpdateing = false;

    public final static String ACTION = "com.giga.library.appUpdate";

    public interface Key {
        String UPDATE_IGNORE_VERSION = "UPDATE_IGNORE_VERSION";
        String UPDATE_HAVE_DOWNLOAD_VERSION = "UPDATE_HAVE_DOWNLOAD_VERSION";
        String UPDATE_HAVE_DOWNLOAD_PATH = "UPDATE_HAVE_DOWNLOAD_PATH";
        String UPDATE_HAVE_DOWNLOAD_FILE_MD5 = "UPDATE_HAVE_DOWNLOAD_FILE_MD5";
    }

    public static AppUpdate getInstance() {
        if (instance == null) {
            synchronized (AppUpdate.class) {
                if (instance == null) {
                    instance = new AppUpdate();
                }
            }
        }
        return instance;
    }

    public void init(Context context, String serverUrl, String downloadPath, boolean mustWifi) {
        this.context = context;
        this.serverUrl = serverUrl;
        this.downloadPath = downloadPath;
        this.mustWifi = mustWifi;
    }

    public void setIsUpdateing(boolean isUpdateing) {
        this.isUpdateing = isUpdateing;
    }

    public boolean isUpdateing() {
        return isUpdateing;
    }

    /**
     * 检查更新
     *
     * @param activity
     */
    public void checkUpdate(IBaseActivity activity) {
        checkUpdate(activity, false);
    }

    /**
     * 检查更新
     *
     * @param activity
     * @param active   是否即使忽略此版本也提示更新
     */
    public void checkUpdate(IBaseActivity activity, boolean active) {
        if (context == null || EmptyUtils.isEmpty(serverUrl)) {
            LogUtils.e("AppUpdate:sdk未初始化");
            EventBus.getDefault().post(new IEvent(ACTION));
            return;
        }
        synchronized (AppUpdate.class) {
            if (isUpdateing) {
                ToastUtils.showLongToast("正在更新请稍候...");
                EventBus.getDefault().post(new IEvent(ACTION));
                return;
            } else {
                isUpdateing = true;
            }
        }
        if (NetworkUtils.isConnected()) {
            if (active) {
                startUpdate(activity, active);
            } else {
                if (mustWifi) {
                    if (NetworkUtils.getNetworkType() == NetworkUtils.NetworkType.NETWORK_WIFI) {
                        startUpdate(activity, active);
                    } else {
                        isUpdateing = false;
                        LogUtils.v("AppUpdate:非WIFI网络环境,忽略更新!");
                        EventBus.getDefault().post(new IEvent(ACTION));
                    }
                } else {
                    startUpdate(activity, active);
                }
            }
        } else {
            if (active) {
                NetworkUtils.openWirelessSettings();
            }
            isUpdateing = false;
            EventBus.getDefault().post(new IEvent(ACTION));
        }
    }

    private void startUpdate(final IBaseActivity activity, final boolean active) {
        OkHttpUtils.getInstance()
                .get()
                .url(serverUrl)
                .build()
                .execute(new GenericsCallback<UpdateInfo>() {

                    @Override
                    public void onAfter(int id) {
                        super.onAfter(id);
                        EventBus.getDefault().post(new IEvent(ACTION));
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        isUpdateing = false;
                        LogUtils.e("AppUpdate:网络异常数据获取失败");
                        ToastUtils.showLongToast("更新数据获取失败,请检查网络~");
                    }

                    @Override
                    public void onResponse(UpdateInfo response, int id) {
                        if (response == null) {
                            isUpdateing = false;
                            LogUtils.e("AppUpdate:服务器数据异常");
                            return;
                        }
                        try {
                            int nowCode = AppUtils.getAppVersionCode();
                            int minVersion;
                            if (EmptyUtils.isNotEmpty(response.getMinVersionCode())) {
                                minVersion = Integer.parseInt(response.getMinVersionCode());
                            } else {
                                minVersion = 0;
                            }
                            if (nowCode < minVersion) {
                                LogUtils.i("AppUpdate:版本低于最低版本限制,强制更新!");
                                update(response);
                                return;
                            }
                            int serCode = Integer.parseInt(response.getVersionCode());
                            if (serCode > nowCode) {
                                if (!active) {
                                    int IgnoreCode = Integer.parseInt(SPUtils.getInstance().getString(
                                            Key.UPDATE_IGNORE_VERSION, "0"));
                                    if (IgnoreCode == serCode) {
                                        isUpdateing = false;
                                        LogUtils.v("AppUpdate:已忽略当前版本");
                                        return;
                                    }
                                }
                                showUpdateDialog(activity, response);
                            } else {
                                isUpdateing = false;
                                LogUtils.v("AppUpdate:已经是最新版本!");
                                if (active) {
                                    ToastUtils.showLongToast("已是最新版本");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            isUpdateing = false;
                            LogUtils.e("AppUpdate:服务器数据异常");
                        }
                    }
                });
    }

    // 显示更新对话框
    private void showUpdateDialog(IBaseActivity activity, final UpdateInfo response) {
        String content = "新版本 ver." + response.getVersionName() + "\n安装包大小:" + response.getFileSize() + "\n更新内容:" + response.getUpdateContent();
        new MaterialDialog.Builder(activity)
                .title("应用更新")
                .content(content)
                .positiveText("更新")
                .neutralText("忽略")
                .negativeText("取消")
                .cancelable(true)
                .canceledOnTouchOutside(true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        update(response);
                    }
                })
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        isUpdateing = false;
                        SPUtils.getInstance().put(
                                Key.UPDATE_IGNORE_VERSION,
                                response.getVersionCode());
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        isUpdateing = false;
                    }
                }).cancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                isUpdateing = false;
            }
        }).show();
    }

    // 执行更新操作
    private void update(UpdateInfo response) {
        int nowCode = Integer.parseInt(response.getVersionCode());
        int downCode = Integer.parseInt(SPUtils.getInstance().getString(
                Key.UPDATE_HAVE_DOWNLOAD_VERSION, "0"));
        // 如果已经下载该版本APK则直接更新
        if (nowCode == downCode) {
            String path = SPUtils.getInstance()
                    .getString(Key.UPDATE_HAVE_DOWNLOAD_PATH, "");
            File file = new File(path);
            if (file != null && file.exists()) {
                String md5 = EncryptUtils.encryptMD5File2String(file);
                String md5_v = SPUtils.getInstance().getString(Key.UPDATE_HAVE_DOWNLOAD_FILE_MD5);
                if (md5.equals(md5_v)) {
                    isUpdateing = false;
                    LogUtils.v("AppUpdate:当前版本已下载,可直接安装");
                    //震动并弹出安装界面
                    long[] pattern = {0, 100, 200, 300};
                    Vibrator vibrator = (Vibrator) context
                            .getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(pattern, -1);
                    AppUtils.installApp(context, path);
                } else {
                    file.delete();
                    download(response);
                }
            } else {
                download(response);
            }
        } else {
            download(response);
        }
    }

    private void download(final UpdateInfo response) {
        ToastUtils.showLongToast("开始下载更新包...");
        DownloadConfig config = new DownloadConfig();
        config.setUrl(response.getDownloadUrl());
        config.setPath(downloadPath);
        config.setFileName(System.currentTimeMillis() + ".apk");
        config.setTicker("开始更新...");
        config.setTitle("正在更新 ver." + response.getVersionName());
        config.setIconResourceId(R.drawable.framework_download_image);
        config.setAction(ACTION);
        config.setData(response.getVersionCode());
        config.setPackageName(context.getPackageName());
        Intent intent = new Intent(context, DownLoadService.class);
        intent.putExtra(DownLoadService.KEY, config);
        context.startService(intent);
    }
}
