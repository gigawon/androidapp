package com.giga.library.update;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

import java.io.File;

import com.giga.library.bean.DownloadResult;
import com.giga.library.http.DownLoadService;
import com.giga.library.util.AppUtils;
import com.giga.library.util.EncryptUtils;
import com.giga.library.util.LogUtils;
import com.giga.library.util.SPUtils;
import com.giga.library.util.ToastUtils;


/**
 * Created by LaMitad on 2016/12/12.
 */

public class AppUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        DownloadResult result = intent.getExtras().getParcelable(DownLoadService.KEY);
        if (result.isSuccess() && context.getPackageName().equals(result.getPackageName())) {
            File file = new File(result.getPath());
            if (file != null && file.exists()) {
                LogUtils.v("更新文件下载完成,弹出安装页面");
                // 更新配置文件
                SPUtils.getInstance().put(
                        AppUpdate.Key.UPDATE_HAVE_DOWNLOAD_VERSION, result.getData());
                SPUtils.getInstance().put(
                        AppUpdate.Key.UPDATE_HAVE_DOWNLOAD_PATH, result.getPath());
                SPUtils.getInstance().put(
                        AppUpdate.Key.UPDATE_HAVE_DOWNLOAD_FILE_MD5, EncryptUtils.encryptMD5File2String(file));
                // 发出震动提示
                long[] pattern = {0, 100, 200, 300};
                Vibrator vibrator = (Vibrator) context
                        .getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(pattern, -1);
                // 安装应用
                AppUtils.installApp(context, result.getPath());
            } else {
                ToastUtils.showLongToast("更新失败,未找到更新文件...");
            }
        }
        AppUpdate.getInstance().setIsUpdateing(false);
    }

}
