package com.giga.library.http;

import java.io.IOException;

import com.giga.library.util.LogUtils;
import com.giga.library.util.StringUtils;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by LaMitad on 2016/12/15.
 */
public class ILogInterceptor implements Interceptor {
    public static final String TAG = "OkHttp";
    private String tag;
    private boolean showResponse;

    public ILogInterceptor(String tag, boolean showResponse) {
        if (StringUtils.isEmpty(tag)) {
            tag = TAG;
        }
        this.showResponse = showResponse;
        this.tag = tag;
    }

    public ILogInterceptor(String tag) {
        this(tag, false);
    }

    public ILogInterceptor() {
        this(TAG, false);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        logForRequest(request);
        Response response = chain.proceed(request);
        return logForResponse(response);
    }

    private void logForRequest(Request request) {
        try {
            String url = request.url().toString();
            Headers headers = request.headers();
            LogUtils.i(tag, "*Request method: " + request.method() + " url:" + url);
            if (headers != null && headers.size() > 0) {
                LogUtils.v(tag, "*Request headers: " + headers.toString());
            }
            RequestBody requestBody = request.body();
            if (requestBody != null) {
                LogUtils.v(tag, "*Request body: " + bodyToString(request));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Response logForResponse(Response response) {
        try {
            Response.Builder builder = response.newBuilder();
            Response clone = builder.build();
            LogUtils.d(tag, "*Response code: " + clone.code() + " url: " + clone.request().url());
            if (showResponse) {
                ResponseBody body = clone.body();
                if (body != null) {
                    MediaType mediaType = body.contentType();
                    if (mediaType != null) {
                        LogUtils.v(tag, "*Response contentType: " + mediaType.toString());
                        if (isText(mediaType)) {
                            String resp = body.string();
                            if (mediaType.subtype().equals("json")) {
                                LogUtils.json(tag, "*Response json: " + resp);
                            } else if (mediaType.subtype().equals("xml")) {
                                LogUtils.xml(tag, "*Response xml: " + resp);
                            } else {
                                LogUtils.v(tag, "*Response content: " + resp);
                            }
                            body = ResponseBody.create(mediaType, resp);
                            return response.newBuilder().body(body).build();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private boolean isText(MediaType mediaType) {
        if (mediaType.type() != null && mediaType.type().equals("text")) {
            return true;
        }
        if (mediaType.subtype() != null) {
            if (mediaType.subtype().equals("json") ||
                    mediaType.subtype().equals("xml") ||
                    mediaType.subtype().equals("html") ||
                    mediaType.subtype().equals("webviewhtml")
                    )
                return true;
        }
        return false;
    }

    private String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "something error when show requestBody.";
        }
    }
}

