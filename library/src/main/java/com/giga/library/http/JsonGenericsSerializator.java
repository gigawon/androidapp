package com.giga.library.http;

import com.google.gson.Gson;

import com.giga.library.util.LogUtils;

public class JsonGenericsSerializator implements IGenericsSerializator {
    Gson mGson = new Gson();

    @Override
    public <T> T transform(String response, Class<T> classOfT) {
        T bean;
        try {
            bean = mGson.fromJson(response, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.e("Json数据解析失败: response:" + response);
            LogUtils.e("Exception:" + e.toString());
            bean = null;
        }
        return bean;
    }

    @Override
    public String toJsonString(Object object) {
        return mGson.toJson(object);
    }
}
