package com.giga.library.http;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
//import android.support.v7.app.NotificationCompat;

import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.giga.library.R;
import com.giga.library.bean.DownloadConfig;
import com.giga.library.bean.DownloadResult;
import com.giga.library.util.EmptyUtils;
import okhttp3.Call;


/**
 * Created by LaMitad on 2016/11/29.
 * 下载服务(提供下载队列)
 */

public class DownLoadService extends Service {

    public final static String KEY = "DownloadConfig";
    public final static String ACTION = "com.giga.library.download.complete";

    private boolean isDownLoading = false;
    private NotificationCompat.Builder builder;
    private List<DownloadConfig> downloadList = new ArrayList<>();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        DownloadConfig config = null;
        if (intent != null) {
            config = intent.getExtras().getParcelable(KEY);
            if (config == null) {
                stopSelf();
            } else {
                super.onStartCommand(intent, flags, startId);
            }
        } else {
            if (!isDownLoading) {
                stopSelf();
            } else {
                super.onStartCommand(intent, flags, startId);
            }
        }
        if (EmptyUtils.isEmpty(config.getUrl()) && EmptyUtils.isEmpty(config.getPath())) {
            if (!isDownLoading) {
                stopSelf();
            }
        }
        downloadList.add(config);
        if (!isDownLoading) {
            startDownload();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initNotify();
    }

    private void initNotify() {
        builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(false);
        builder.setProgress(100, 0, false);
        builder.setOngoing(true);
    }

    private void setNotify(DownloadConfig config) {
        if (EmptyUtils.isEmpty(config.getTicker())) {
            builder.setTicker("开始下载...");
        } else {
            builder.setTicker(config.getTicker());
        }
        if (EmptyUtils.isEmpty(config.getTitle())) {
            builder.setContentTitle("正在下载请稍候...");
        } else {
            builder.setContentTitle(config.getTitle());
        }
        if (config.getIconResourceId() == 0) {
            builder.setSmallIcon(R.drawable.framework_download_image);
        } else {
            builder.setSmallIcon(config.getIconResourceId());
        }
        builder.setContentText("0/100");
        builder.setWhen(System.currentTimeMillis());
        startForeground(DownLoadService.class.hashCode(), builder.build());
    }

    /**
     * 设置下载进度
     */
    public void setProgress(int progress) {
        builder.setProgress(100, progress, false);
        builder.setContentText(progress + "/100");
        startForeground(DownLoadService.class.hashCode(), builder.build());
    }

    private long time = 0;

    private void startDownload() {
        if (downloadList.size() == 0) {
            stopSelf();
        } else {
            isDownLoading = true;
            final DownloadConfig config = downloadList.get(0);
            downloadList.remove(0);
            setNotify(config);
            OkHttpUtils
                    .get()
                    .url(config.getUrl())
                    .build()
                    .execute(new FileCallBack(config.getPath(), config.getFileName()) {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            DownloadResult result = new DownloadResult();
                            result.setSuccess(false);
                            result.setUrl(config.getUrl());
                            Intent intent = new Intent();
                            if (EmptyUtils.isEmpty(config.getAction())) {
                                intent.setAction(ACTION);
                            } else {
                                intent.setAction(config.getAction());
                            }
                            intent.putExtra(KEY, result);
                            sendBroadcast(intent);
                            startDownload();
                        }

                        @Override
                        public void onResponse(File response, int id) {
                            DownloadResult result = new DownloadResult();
                            result.setSuccess(true);
                            result.setPath(response.getAbsolutePath());
                            result.setUrl(config.getUrl());
                            result.setData(config.getData());
                            result.setPackageName(config.getPackageName());
                            Intent intent = new Intent();
                            if (EmptyUtils.isEmpty(config.getAction())) {
                                intent.setAction(ACTION);
                            } else {
                                intent.setAction(config.getAction());
                            }
                            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                            intent.putExtra(KEY, result);
                            sendBroadcast(intent);
                            startDownload();
                        }

                        @Override
                        public void inProgress(float progress, long total, int id) {
                            if ((System.currentTimeMillis() - time) > 1000) {
                                time = System.currentTimeMillis();
                                setProgress((int) (100 * progress));
                            }
                        }

                    });
        }
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        super.onDestroy();
    }
}
