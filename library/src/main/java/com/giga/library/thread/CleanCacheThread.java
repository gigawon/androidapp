package com.giga.library.thread;


import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import com.giga.library.base.BaseApplication;
import com.giga.library.bean.IEvent;
import com.giga.library.util.LogUtils;

import static com.giga.library.util.FileUtils.deleteDir;

/**
 * Created by Wxcily on 15/11/5.
 * 用于清理临时文件
 */
public class CleanCacheThread extends Thread {

    public final static String ACTION = "com.giga.library.clean.complete";

    private BaseApplication application;

    public CleanCacheThread(BaseApplication application) {
        this.application = application;
    }

    @Override
    public void run() {
        super.run();
        //清除图片缓存
        Glide.get(application.getApplicationContext()).clearDiskCache();
        //清除临时文件
        File temp_dir = new File(application.getTempPath());
        File[] temp_files = temp_dir.listFiles();
        for (int i = 0; i < temp_files.length; i++) {
            File file = temp_files[i];
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                file.delete();
            }
        }
        //清除下载文件
        File download_dir = new File(application.getDownloadPath());
        File[] download_dirs = download_dir.listFiles();
        for (int i = 0; i < download_dirs.length; i++) {
            File file = download_dirs[i];
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                file.delete();
            }
        }
        EventBus.getDefault().post(new IEvent(ACTION));
        LogUtils.i("缓存清理完成");
    }

}
