package com.giga.library.thread;


import java.io.File;

import com.giga.library.base.BaseApplication;
import com.giga.library.util.LogUtils;

import static com.giga.library.util.FileUtils.deleteDir;

/**
 * Created by Wxcily on 15/11/5.
 * 用于清理临时文件
 */
public class CleanTempThread extends Thread {

    private BaseApplication application;

    public CleanTempThread(BaseApplication application) {
        this.application = application;
    }

    @Override
    public void run() {
        super.run();
        //清除临时文件
        File temp_dir = new File(application.getTempPath());
        File[] files = temp_dir.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.isDirectory()) {
                    deleteDir(file);
                } else {
                    file.delete();
                }
            }
        }
        LogUtils.v("临时文件清理完成");
    }

}
