package com.giga.library.util;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DeviceUtil {
	public static String formatStr(String str) {
		return isNull(str) ? "" : str;
	}
	/**
	 * 判断给定字符串是否空白串。
	 *
	 * @param input
	 * @return boolean
	 */
	public static boolean isNull(String input) {
		//return str == null || "".equals(str) || str.trim().equals("null") ? true : false;
		if (input == null || "".equals(input))
			return true;

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
				return false;
			}
		}
		return true;
	}
	public static boolean isNumeric(String s) {
		
		return s.replaceAll("[+-]?\\d+", "").equals("");
		
	}
	
	public interface GeocodesByAddressListener {
		
		void onComplete(String json);
		
		void onError();
		
		void onError(String error);
		
	}
	


	
	/**
	 * 获取deviceID
	 * @param context
	 * @return
	 */
    public static String getDeviceId(Context context) {
            String deviceId = "";
            TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = tm.getDeviceId();   
            
            if (deviceId == null || "".equals(deviceId)) {  
                try {  
                    deviceId = getAndroidId(context);  
                } catch (Exception e) {
                    e.printStackTrace();  
                }  
            }  
            return deviceId;  
        }  
	
        // Android Id  
        private static String getAndroidId(Context context) {
            String androidId = Settings.Secure.getString(
                    context.getContentResolver(), Settings.Secure.ANDROID_ID);
            return androidId;  
        }  
      
        /**
         * 保存生成的deviceid
         * @param context
         * @param str
         */
        public static void saveDeviceID(Context context , String str) {
            try {
            	FileOutputStream fos = 	context.openFileOutput("file_out.txt",
                        Context.MODE_PRIVATE);
            	fos.write(str.getBytes());  
            	fos.close();  
            } catch (IOException e) {
                e.printStackTrace();  
            }  
        }  
      
        /**
         * 读取存储的deviceid
         * @param context
         * @return
         */
        public static String readDeviceID(Context context) {
        	 try {  
        	 FileInputStream fis= context.openFileInput("file_out.txt");
        	    byte[] buffer = new byte[1024];  
        	    fis.read(buffer);  
        	    // 对读取的数据进行编码以防止乱码  
        	    String fileContent = new String(buffer, "UTF-8");
                return fileContent;  
            } catch (IOException e) {
                e.printStackTrace();  
                return null;  
            }  
        }

        public static void Debug_Toast_Message(Context context, String msg)
		{
			//Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
		}

		public static void Real_Service_Toast_Message(Context context, String msg)
		{
			//Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
		}
	
}
