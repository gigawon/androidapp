package com.giga.library.util;

import android.util.Log;

public class L {

	public static final String TAG = "giga_log";

	public static boolean ACTIVE = true;
	
	public static void d(String log) {

		if (ACTIVE) Log.d(TAG, log);
		
	}
	
	public static void e(Exception e) {

		if (ACTIVE) {
			Log.e(TAG, e.getClass().toString());
			Log.e(TAG, "Exception: " + Log.getStackTraceString(e));
			e.printStackTrace();
		}
	}
	
}
