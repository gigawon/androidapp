package com.giga.library.util;

public class C {

    //기본 url
    public static String BASE_URL = "";

    public static  String APP_NAME = "GIGA";
    // 需要和manifest的provider名称一致
    public static  String FILE_CONTENT_FILEPROVIDER = "com.giga.shopping.fileprovider";

    public static final String KEY_JSON_FM_STATUS = "status";

    public static final String VALUE_RESPONSE_SUCCESS = "success";

    public static final String KEY_RESPONSE_FLAG = "flag";

    public static final String KEY_JSON_PROFILE_IMG = "ProfileImg";

}
