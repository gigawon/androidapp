package com.giga.library.util;

import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.giga.library.util.FileUtils.createOrExistsFile;

/**
 * <pre>
 *     author: Blankj
 *     blog  : http://blankj.com
 *     time  : 2016/9/27
 *     desc  : 崩溃相关工具类
 * </pre>
 */
public class CrashUtils implements UncaughtExceptionHandler {

    private volatile static CrashUtils mInstance;

    private UncaughtExceptionHandler mHandler;
    private boolean mInitialized;
    private String crashDir;
    private String packageName;
    private String versionName;
    private int versionCode;
    private boolean isDebug;

    private CrashUtils() {
    }

    /**
     * 获取单例
     * <p>在Application中初始化{@code CrashUtils.getInstance().init(this);}</p>
     *
     * @return 单例
     */
    public static CrashUtils getInstance() {
        if (mInstance == null) {
            synchronized (CrashUtils.class) {
                if (mInstance == null) {
                    mInstance = new CrashUtils();
                }
            }
        }
        return mInstance;
    }

    /**
     * 初始化
     *
     * @return {@code true}: 成功<br>{@code false}: 失败
     */
    public boolean init(boolean isDebug, String crashDir) {
        if (mInitialized)
            return true;
        this.isDebug = isDebug;
        this.crashDir = crashDir + File.separator + "crash";
        File dir = new File(crashDir);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
        packageName = Utils.getContext().getPackageName();
        versionName = AppUtils.getAppVersionName();
        versionCode = AppUtils.getAppVersionCode();
        mHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        return mInitialized = true;
    }

    @Override
    public void uncaughtException(Thread thread, final Throwable throwable) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isDebug) {
                    Date now = new Date();
                    String date = new SimpleDateFormat("MM-dd", Locale.getDefault()).format(now);
                    final String fullPath = crashDir + File.separator + date + ".crash";
                    if (!createOrExistsFile(fullPath)) {
                        return;
                    }
                    String time = new SimpleDateFormat("MM-dd HH:mm:ss.SSS ", Locale.getDefault()).format(now);
                    String crashInfo = getCrashHead(time) + getErrorInfo(throwable);
                    LogUtils.e(crashInfo);
                    BufferedWriter bw = null;
                    try {
                        bw = new BufferedWriter(new FileWriter(fullPath, true));
                        bw.write(crashInfo);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (bw != null) {
                                bw.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
        showToast(Utils.getContext(), "程序异常,我们会尽快修复!");
        try {
            thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (mHandler != null) {
            mHandler.uncaughtException(thread, throwable);
        }
    }

    private void showToast(final Context context, final String msg) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        }).start();
    }

    /**
     * 获取崩溃头
     *
     * @return 崩溃头
     */
    private String getCrashHead(String time) {
        return "\n************* Crash Time " + time + " ****************" +
                "\nDevice Manufacturer: " + Build.MANUFACTURER +// 设备厂商
                "\nDevice Model       : " + Build.MODEL +// 设备型号
                "\nAndroid Version    : " + Build.VERSION.RELEASE +// 系统版本
                "\nAndroid SDK        : " + Build.VERSION.SDK_INT +// SDK版本
                "\nApp PackageName    : " + packageName +
                "\nApp VersionName    : " + versionName +
                "\nApp VersionCode    : " + versionCode +
                "\n\n";
    }

    private static String getErrorInfo(Throwable arg1) {
        Writer writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        arg1.printStackTrace(pw);
        pw.close();
        String error = writer.toString();
        return error;
    }
}
